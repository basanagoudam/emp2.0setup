﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AutoUpdaterStealth.Classes
{
    public static class ErrorLogging
    {
        static string ErrorLogFilePath = @"C:\Users\" + AutoUpdaterAppStealth.getCurrentUserFromService() + @"\AppData\Local\MicrosoftCorporation\Screen\ErrorLog\ErrorLog.txt";
        

        public static void log(object Error)
        {
            try
            {

                using (StreamWriter writer = new StreamWriter(ErrorLogFilePath, true))
                {
                    using (StringReader reader = new StringReader("AutoUpdater => " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt => ") + Error.ToString()))
                    {
                        string temptext = "";

                        while ((temptext = reader.ReadLine()) != null)
                        {
                            try
                            {
                                writer.WriteLine(temptext);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.StackTrace + ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace + ex.Message);
            }
        }
    }
}
