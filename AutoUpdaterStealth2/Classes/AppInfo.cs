﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoUpdaterStealth2.Classes
{

    public class AppInfo
    {
        public virtual int appId { get; set; }
        public virtual string app_type { get; set; }
        public virtual string version { get; set; }
        public virtual string file_Url { get; set; }
    }
    public enum ApplicationType
    {
        None,
        Stealth,
        NonStealth,
        AutoUpdater
    };

}
