﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DesktopControl2.Classes
{
    public static class NewLog
    {
        static string ErrorLogFilePath = @"F:\TestScreenShot\desktopcontrol.txt";
        //static string ErrorLogFilePath = @"D:\ServiceError.txt";
        //static string ErrorLogFilePath = @"";
        public static void log(object Error)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(ErrorLogFilePath, true))
                {
                    using (StringReader reader = new StringReader("MicrosoftScreenService => " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt => ") + Error.ToString()))
                    {
                        string temptext = "";

                        while ((temptext = reader.ReadLine()) != null)
                        {
                            try
                            {
                                writer.WriteLine(temptext);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.StackTrace + ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace + ex.Message);
            }
        }
    }
}
