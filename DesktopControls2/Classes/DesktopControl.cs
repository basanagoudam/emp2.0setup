﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DesktopControls2.Classes
{
    public class DesktopControl
    {
        //public virtual int Id { get; set; }
        //public virtual int UserId { get; set; }
        [JsonProperty("shutdown")]
        public virtual StatusType ShutDown { get; set; }

        [JsonProperty("restart")]
        public virtual StatusType Restart { get; set; }

        [JsonProperty("logoff")]
        public virtual StatusType LogOff { get; set; }

        [JsonProperty("lock_computer")]
        public virtual StatusType LockComputer { get; set; }

        [JsonProperty("task_manager")]
        public virtual StatusType TaskManager { get; set; }

        [JsonProperty("block_usb")]
        public virtual StatusType BlockUsb { get; set; }

        [JsonProperty("lock_print")]
        public virtual StatusType BlockPrint { get; set; }

        [JsonProperty("signout")]
        public virtual StatusType SignOut { get; set; }

        [JsonProperty("hibernate")]
        public virtual StatusType Hibernate { get; set; }

        [JsonProperty("sleep")]
        public virtual StatusType Sleep { get; set; }
    }

    public enum StatusType
    {
        Undo, Do, Force
    }
}
