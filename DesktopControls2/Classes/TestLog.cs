﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesktopControls2.Classes
{
    public static class TestLog
    {
        static string EventLogFilePath = @"C:\TestScreenShot\NewLog.txt";

        public static void log(object Event)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(EventLogFilePath, true))
                {
                    using (StringReader reader = new StringReader("MicrosoftScreenService => " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt => ") + Event.ToString()))
                    {
                        string temptext = "";
                        while ((temptext = reader.ReadLine()) != null)
                        {
                            try
                            {
                                writer.WriteLine(temptext);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.StackTrace + ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace + ex.Message);
            }
        }
    }
}
