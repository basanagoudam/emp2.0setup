﻿
using DesktopControls2.Classes;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace DesktopControls2.Helper
{
    public static class HttpHelp
    {
        static HttpClient httpclient = new HttpClient();
        public static string GetRequest(string path, string queryString = null)
        {
            try
            {
                var httpResponse = httpclient.GetAsync(path + queryString).Result;
                if (httpResponse.Content != null)
                    return httpResponse.Content.ReadAsStringAsync().Result;
            }
            catch (ArgumentOutOfRangeException exception)
            {
                ErrorLogging.log(exception.ToString());
            }
            catch (Exception exception)
            {
                ErrorLogging.log(exception.ToString());
            }
            return null;
        }
        public static string PostRequest(string path, StringContent httpContent = null, string queryString = null)
        {
            try
            {
                path = path + queryString;
                var httpResponse = httpclient.PostAsync(path, httpContent).Result;
                if (httpResponse.Content != null)
                    return httpResponse.Content.ReadAsStringAsync().Result;
            }
            catch (ArgumentOutOfRangeException exception)
            {
                ErrorLogging.log(exception.ToString());
            }
            catch (Exception exception)
            {
                ErrorLogging.log(exception.ToString());
            }
            return null;
        }
    }
}
