﻿using DesktopControls2.Classes;
using DesktopControls2.Helper;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace DesktopControls2
{
    class Program
    {
        [DllImport("user32")]
        public static extern bool BlockInput(bool fBlockIt);

        [DllImport("user32")]
        public static extern void LockWorkStation();[DllImport("user32")]
        public static extern bool ExitWindowsEx(uint uFlags, uint dwReason);

        [DllImport("PowrProf.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);
        static void Main(string[] args)
        {
            try
            {
                string EmailPath = @"C:\Users\" + getCurrentUserFromWMI() + @"\AppData\Local\Details\Screen\UserDetails\Email.txt";
                string userCreds = File.ReadAllText(EmailPath);
                string EmailId = userCreds.Split(new string[1] { "<:><:><:>" }, 2, StringSplitOptions.None)[0];
                string Password = userCreds.Split(new string[1] { "<:><:><:>" }, 2, StringSplitOptions.None)[1];
                UserCreds userDetails = new UserCreds() { Email = EmailId, Password = Password };
                var postDetails = JsonConvert.SerializeObject(userDetails);
                var httpContentdetails = new StringContent(postDetails, Encoding.UTF8, "application/json");
                string checkUserIsValid = HttpHelp.PostRequest(AppSettings.UserUrl, httpContentdetails, "login");
                JObject jObject = JObject.Parse(checkUserIsValid);
                var response = jObject["data"].ToString();

                DesktopControl desktopControl = new DesktopControl();

                if (response == "True")
                {
                    List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>(3);
                    while (true)
                    {
                        try
                        {
                            var desktopRespo = JObject.Parse(HttpHelp.GetRequest(AppSettings.UserUrl, "desktop-settings"));

                            if (desktopRespo["code"].ToString() == "200")
                            {
                                desktopControl = JsonConvert.DeserializeObject<DesktopControl>(desktopRespo["data"].ToString());
                            }

                            if (desktopControl.LogOff == StatusType.Do)
                            {
                                desktopControl.LogOff = 0;
                                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
                                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
                                var statusCodes = JObject.Parse(HttpHelp.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
                                if (statusCodes.Value<int>("code") == 202)
                                    LockWorkStation();
                            }
                            if (desktopControl.ShutDown == StatusType.Force)
                            {
                                desktopControl.ShutDown = 0;
                                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
                                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
                                var statusCodes = JObject.Parse(HttpHelp.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
                                if (statusCodes.Value<int>("code") == 202)
                                    Process.Start("Shutdown", "-s");
                            }
                            else if (desktopControl.ShutDown == StatusType.Do)
                            {
                                desktopControl.ShutDown = 0;
                                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
                                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
                                var statusCodes = JObject.Parse(HttpHelp.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
                                if (statusCodes.Value<int>("code") == 202)
                                    Process.Start("Shutdown", "-s -f");
                            }
                            if (desktopControl.Restart == StatusType.Force)
                            {
                                desktopControl.Restart = 0;
                                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
                                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
                                var statusCodes = JObject.Parse(HttpHelp.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
                                if (statusCodes.Value<int>("code") == 202)
                                    Process.Start("Shutdown", "-r");
                            }
                            else if (desktopControl.Restart == StatusType.Do)
                            {
                                desktopControl.Restart = 0;
                                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
                                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
                                var statusCodes = JObject.Parse(HttpHelp.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
                                if (statusCodes.Value<int>("code") == 202)
                                    Process.Start("Shutdown", "-r -f");
                            }



                            if (desktopControl.SignOut == StatusType.Do)
                            {
                                desktopControl.SignOut = 0;
                                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
                                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
                                var statusCodes = JObject.Parse(HttpHelp.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
                                if (statusCodes.Value<int>("code") == 202)
                                    ExitWindowsEx(0, 0);
                            }
                            if (desktopControl.Hibernate == StatusType.Do)
                            {
                                desktopControl.Hibernate = 0;
                                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
                                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
                                var statusCodes = JObject.Parse(HttpHelp.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
                                if (statusCodes.Value<int>("code") == 202)
                                    SetSuspendState(true, true, true);
                            }
                            if (desktopControl.Sleep == StatusType.Do)
                            {
                                desktopControl.Sleep = 0;
                                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
                                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
                                var statusCodes = JObject.Parse(HttpHelp.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
                                if (statusCodes.Value<int>("code") == 202)
                                    SetSuspendState(false, true, true);
                            }
                            if (desktopControl.BlockUsb == StatusType.Do)
                                Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\USBSTOR", "Start", 4, Microsoft.Win32.RegistryValueKind.DWord);
                            else
                                Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\USBSTOR", "Start", 3, Microsoft.Win32.RegistryValueKind.DWord);

                            if (desktopControl.BlockPrint == StatusType.Do)
                            {
                                using (var svc = new ServiceController("Spooler"))
                                {
                                    if (svc.Status == ServiceControllerStatus.Running)
                                    {
                                        ServiceHelper.ChangeStartMode(svc, ServiceStartMode.Disabled);
                                        svc.Stop();
                                        svc.WaitForStatus(ServiceControllerStatus.Stopped);
                                    }
                                }
                            }
                            else
                            {
                                using (var svc = new ServiceController("Spooler"))
                                {
                                    if (svc.Status == ServiceControllerStatus.Stopped)
                                    {
                                        ServiceHelper.ChangeStartMode(svc, ServiceStartMode.Automatic);
                                        svc.Start();
                                        svc.WaitForStatus(ServiceControllerStatus.Running);
                                    }
                                }
                            }

                            if (desktopControl.LockComputer == StatusType.Do)
                                BlockInput(true);
                            else
                                BlockInput(false);

                            if (desktopControl.TaskManager == StatusType.Do)
                            {
                                RegistryKey TaskManger = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System", true);
                                if (TaskManger == null)
                                {
                                    TaskManger = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System", RegistryKeyPermissionCheck.ReadWriteSubTree);
                                }
                                TaskManger.SetValue("DisableTaskMgr", 1, Microsoft.Win32.RegistryValueKind.DWord);
                            }
                            else
                            {
                                RegistryKey TaskManger = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System", true);
                                TaskManger?.DeleteValue("DisableTaskMgr");
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        Thread.Sleep(1 * 30 * 1000);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static string getCurrentUserFromWMI()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT UserName FROM Win32_ComputerSystem");
            ManagementObjectCollection collection = searcher.Get();
            string username = (string)collection.Cast<ManagementBaseObject>().First()["UserName"];
            if (username.Contains("\\"))
            {
                username = username.Split('\\')[1];
            }
            return username;
        }
    }
}
