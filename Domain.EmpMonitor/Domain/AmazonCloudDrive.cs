﻿namespace Domain.EmpMonitor.Domain
{
    public class AmazonCloudDrive
    {
        public virtual int amazonCloudDriveId { get; set; }
        public virtual int organizationId { get; set; }
        public virtual string accessToken { get; set; }
        public virtual string refreshToken { get; set; }
    }
}