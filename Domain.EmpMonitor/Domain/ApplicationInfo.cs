using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class ApplicationInfo {
        public virtual int appId { get; set; }
        public virtual string app_type { get; set; }
        public virtual string version { get; set; }
        public virtual string file_Url { get; set; }
    }
    public enum ApplicationType
    {
        None,
        Stealth,
        NonStealth,
        AutoUpdater
    };
}
