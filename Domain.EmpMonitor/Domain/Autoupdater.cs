using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Autoupdater {
        public virtual int Id { get; set; }
        public virtual string Url { get; set; }
        public virtual string AppVersion { get; set; }
        public virtual string AccessToken { get; set; }
    }
}
