﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.DesktopApp
{
    public class BrowserHistory
    {
        public int browserHistoryId { get; set; }
        public string timeStamp { get; set; }
        public string browser { get; set; }
        public string url { get; set; }
        public string logsheetId { get; set; }
    }
}
