using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Companies {
        public virtual int Companies_Id { get; set; }
        public virtual int NumberOfLicenses { get; set; }
        public virtual string OrganizationName { get; set; }
        public virtual string RegisteredEmail { get; set; }
        public virtual int? ActivationStatus { get; set; }
        public virtual string StorageType { get; set; }
        public virtual string TransactionId { get; set; }
        public virtual int BlockedStatus { get; set; }
        public virtual int BlockingType { get; set; }
    }
}
