using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Department {
        public virtual string departmentId { get; set; }
        public virtual string department { get; set; }
        public virtual string locationId { get; set; }
    }
}
