namespace Domain.EmpMonitor.Domain
{
    public class Dropbox
    {
        public virtual int dropboxId { get; set; }
        public virtual int organizationId { get; set; }
        public virtual string desktopAccessToken { get; set; }
        public virtual string webAccessToken { get; set; }
        public virtual string reportAccessToken { get; set; }
    }
}