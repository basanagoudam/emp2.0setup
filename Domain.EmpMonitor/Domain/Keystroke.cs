﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Domain
{
    public class Keystroke
    {
        public int keystrokeId { get; set; }
        public string keystrokeData { get; set; }
        public string logsheetId { get; set; }
    }
}
