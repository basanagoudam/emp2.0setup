using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Location {
        public virtual string locationId { get; set; }
        public virtual string location { get; set; }
        public virtual int organizationId { get; set; }
    }
}
