using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {

    //public class ProductionStat
    //{
    //    public int id { get; set; }
    //    public string log_sheet_id { get; set; }
    //    public string day { get; set; }
    //    public string login_time { get; set; }
    //    public string logout_time { get; set; }
    //    public int user_id { get; set; }
    //    public string working_hours { get; set; }
    //    public string non_working_hours { get; set; }
    //    public string total_hours { get; set; }
    //    public int is_report_generated { get; set; }
    //    public string created_at { get; set; }
    //    public string updated_at { get; set; }
    //}

    public class Logsheet
    {
        public virtual string logsheetId { get; set; }
        public virtual string dateOfEntry { get; set; }
        public virtual string loginTime { get; set; }
        public virtual string logoutTime { get; set; }
        //public virtual string loginTimeUtc { get; set; }
        //public virtual string logoutTimeUtc { get; set; }
        public virtual string workingHours { get; set; }
        public virtual string nonWorkingHours { get; set; }
        public virtual string totalHours { get; set; }
        //public virtual int userId { get; set; }
        //public virtual string lastUpdateTime { get; set; }
        public virtual bool isReportGenerated { get; set; }
    }
}

