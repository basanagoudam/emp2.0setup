﻿namespace Domain.EmpMonitor.Domain
{
    public class MediaFire
    {
        public virtual int mediaFireId { get; set; }
        public virtual int organizationId { get; set; }
        public virtual string username { get; set; }
        public virtual string password { get; set; }
        public virtual string applicationId { get; set; }
        public virtual string apiKey { get; set; }
        
    }
}
