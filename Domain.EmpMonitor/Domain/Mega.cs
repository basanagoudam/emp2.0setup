﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Domain
{
    public class Mega
    {
        public virtual int MegaId { get; set; }
        public virtual int organizationId { get; set; }
        public virtual string username { get; set; }
        public virtual string password { get; set; }
    }
}
