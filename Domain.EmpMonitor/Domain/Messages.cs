using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Messages {
        public virtual int Messages_Id { get; set; }
        public virtual string Message { get; set; }
        public virtual int UsersId { get; set; }
        public virtual int MessageSeen { get; set; }
        public virtual string DateOfEntry { get; set; }
        public virtual string Subject { get; set; }
      
    }
}
