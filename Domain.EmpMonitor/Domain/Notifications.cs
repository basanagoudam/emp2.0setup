using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Notifications {
        public virtual int Notifications_Id { get; set; }
        public virtual string AdminsEmailId { get; set; }
        public virtual string NotificationMessage { get; set; }
        public virtual int? NotificationType { get; set; }
        public virtual int? ActivationStatus { get; set; }
        public virtual int? Users_Id { get; set; }
        public virtual int? EmailSent { get; set; }
        public virtual int NoOfLicenses { get; set; }
    }
}
