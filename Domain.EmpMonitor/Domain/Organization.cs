﻿using Domain.EmpMonitor.Models.DesktopApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Domain
{
    public class Organization
    {
        public virtual int organizationId { get; set; }
        public virtual string organizationName { get; set; }
        public virtual string registeredEmailId { get; set; }
        public virtual string transactionId { get; set; }
        public virtual int licenseCount { get; set; }
        public virtual bool isActivated { get; set; }
        public virtual bool isEmailVerified { get; set; }
        public virtual int selectedStorageType { get; set; }
        public virtual int screenshotCaptureInterval { get; set; }
        public virtual string verificationToken { get; set; }
    }
}
