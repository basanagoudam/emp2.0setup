using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Privileges {
        public virtual int Privileges_Id { get; set; }
        public virtual int AminsId { get; set; }
        public virtual int AddEmployeePrivilege { get; set; }
        public virtual int DeleteEmployeePrivilege { get; set; }
        public virtual int EditEmployeePrivilege { get; set; }
        public virtual int EditDropBoxPrivilege { get; set; }
        public virtual int AddLocationsPrivilege { get; set; }
        public virtual int EditLocationsPrivilege { get; set; }
    }
}
