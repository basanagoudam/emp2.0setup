using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Productivehrs {
        public virtual int ProductiveHrs_Id { get; set; }
        public virtual string ProductiveHoursSingleDay { get; set; }
        public virtual string NonProductiveHoursSingleDay { get; set; }
        public virtual string ProductiveHoursWeekly { get; set; }
        public virtual string NonProductiveHoursWeekly { get; set; }
        public virtual string DateOfEntry { get; set; }
        public virtual int CompanyId { get; set; }
    }
}
