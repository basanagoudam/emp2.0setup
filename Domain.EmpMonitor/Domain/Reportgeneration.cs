using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Reportgeneration {
        public virtual int ReportGeneration_Id { get; set; }
        public virtual string FromTime { get; set; }
        public virtual string ToTime { get; set; }
        public virtual int CompanyId { get; set; }
    }
}
