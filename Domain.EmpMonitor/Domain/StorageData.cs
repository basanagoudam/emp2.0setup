﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Domain
{
    public class StorageData
    {
        public virtual int id { get; set; }
        public virtual int companyId { get; set; }
        public virtual int storageType { get; set; }
        public virtual string adminEmail { get; set; }
        public virtual string clinetId { get; set; }
        public virtual string clientSecret { get; set; }
        public virtual string Token { get; set; }
        public virtual string apiKey { get; set; }
    }
}
