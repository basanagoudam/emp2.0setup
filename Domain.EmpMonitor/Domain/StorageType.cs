﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Domain
{
    public class StorageType
    {
        public virtual int storageTypeId { get; set; }
        public virtual string storageType { get; set; }
    }
}
