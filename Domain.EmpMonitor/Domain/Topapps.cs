using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Topapps {
        public virtual int TopApps_Id { get; set; }
        public virtual string Apps { get; set; }
        public virtual int UsersId { get; set; }
        public virtual int Counts { get; set; }
        public virtual string DateOfEntry { get; set; }
    }
}
