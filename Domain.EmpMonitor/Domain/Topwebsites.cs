using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Topwebsites {
        public virtual int TopWebsites_Id { get; set; }
        public virtual string Website { get; set; }
        public virtual int UsersId { get; set; }
        public virtual int Counts { get; set; }
        public virtual string DateOfEntry { get; set; }
    }
}
