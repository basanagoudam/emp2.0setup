using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Unblockeduserswebsites {
        public virtual int UnblockedUsersWebsites_Id { get; set; }
        public virtual int UsersId { get; set; }
        public virtual int WebsitesId { get; set; }
        public virtual int CompanyId { get; set; }
    }
}
