﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Domain
{
   public class UsbBlock
    {
        public virtual int Id { get; set; }
        public virtual int userId { get; set; }
        public virtual string BlockStatus { get; set; }
    }
}
