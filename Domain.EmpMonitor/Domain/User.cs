using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;




namespace Domain.EmpMonitor.Domain
{
    //public class UserData
    //{
    //    public int id { get; set; }
    //    public string name { get; set; }
    //    public string email { get; set; }
    //    public string phone { get; set; }
    //    public string emp_code { get; set; }
    //    public string location_id { get; set; }
    //    public string department_id { get; set; }
    //    public string date_join { get; set; }
    //    public string photo_path { get; set; }
    //    public string address { get; set; }
    //    public int role_id { get; set; }

    //}

    public class User
    {
        public virtual int userId { get; set; }
        public virtual string emailId { get; set; }
        public virtual string accessToken { get; set; }
        public virtual int? autoRunApp { get; set; }
        public virtual int organizationId { get; set; }
        public virtual string departmentId { get; set; }
        public virtual string displayName { get; set; }
        public virtual string mobileNumber { get; set; }
        public virtual int? notification { get; set; }
        public virtual int? notificationTime { get; set; }
        public virtual string password { get; set; }
        public virtual string task { get; set; }
        public virtual int? rememberMe { get; set; }
        public virtual int roleId { get; set; }
        public virtual int? showBusyWhenFullScreenMode { get; set; }
        public virtual string profilePicUrl { get; set; }
        public virtual int accessPrivileges { get; set; }
        public virtual string dropboxAccessToken { get; set; }
        public virtual int managerId { get; set; }
        public virtual int isPasswordReset { get; set; }
        public virtual string passwordResetToken { get; set; }
        public virtual int islogedIn { get; set; }
        public virtual string clientId { get; set; }
        public virtual string clientSecret { get; set; }
        public virtual string refreshToken { get; set; }
        public virtual int loggedInStatus { get; set; }
        public virtual string loggedInIp { get; set; }
        public virtual string empCode { get; set; }
        public virtual string joiningDate { get; set; }
        public virtual string address { get; set; }
        public virtual int ActivationStatus { get; set; }
    }
    public class User1
    {
        public string pwd_for_check { get; set; }
    }
}
