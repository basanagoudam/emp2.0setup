﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Domain
{
    public class UserDetails
    {
        public virtual bool isOnline { get; set; }
        public virtual bool Ispresent { get; set; }
        public virtual int UsersId { get; set; }
        public virtual string EmailId { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual string MobileNumber { get; set; }
        public virtual string UsersPassword { get; set; }
        public virtual string Location { get; set; }
        public virtual string Department { get; set; }
        public virtual string empCode { get; set; }
        public virtual string joiningDate { get; set; }
        public virtual string address { get; set; }
        public virtual string profilePicUrl { get; set; }
    }


    public class ManagerDetls
    {
        public  static string manager_id;
    }


}
