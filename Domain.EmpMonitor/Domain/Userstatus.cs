using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Userstatus {
        public virtual int UserStatus_Id { get; set; }
        public virtual string LogSheets_Id { get; set; }
        public virtual string Message { get; set; }
        public virtual string StatusMessage { get; set; }
        public virtual string TimeStmp { get; set; }
    }
}
