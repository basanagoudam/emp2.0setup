using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Websites {
        public virtual int Websites_Id { get; set; }
        public virtual string Domain { get; set; }
        public virtual string TypeOfDomain { get; set; }
        public virtual int CompanyId { get; set; }
        public virtual int? IsAllComputersChecked { get; set; }
    }
    public class Website
    {
        public virtual int id { get; set; }
        public virtual string domain { get; set; }
        public virtual int type_id { get; set; }
    }
}
