using System;
using System.Text;
using System.Collections.Generic;


namespace Domain.EmpMonitor.Domain {
    
    public class Websitesallcomuterschecked {
        public virtual string Id { get; set; }
        public virtual int CompanyId { get; set; }
        public virtual int? AllComputersChecked { get; set; }
        public virtual int WebsitesId { get; set; }
    }
}
