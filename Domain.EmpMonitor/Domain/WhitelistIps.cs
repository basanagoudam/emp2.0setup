﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Domain
{
   public class WhitelistIps
    {
        public virtual Int32 Id { get; set; }
        public virtual string Ip { get; set; }
        public virtual string AdminEmail { get; set; }
    }
}
