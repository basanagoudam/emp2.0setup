﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models
{
    public class AccessToken
    {
        public string accessToken { get; set; }
        public string emailId { get; set; }
        public string timeStamp { get; set; }
    }
}