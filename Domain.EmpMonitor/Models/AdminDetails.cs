﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models
{
    public class AdminDetails
    {
        public string EmailId { get; set; }
        public string AccessToken { get; set; }
        public string ComapnyId { get; set; }
    }
}
