﻿using Domain.EmpMonitor.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models
{
    public class AdminLogin
    {
        public string methodName { get; set; }
        public bool success { get; set; }
        public string message { get; set; }
        public User userDetails { get; set; }
        public Privileges privileges { get; set; }
        public List<Notifications> notifications { get; set; }
    }
}
