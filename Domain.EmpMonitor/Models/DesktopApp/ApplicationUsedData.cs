﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.DesktopApp
{
    public class ApplicationUsedData
    {
        public int applicationUsedId { get; set; }
        public string timeStamp { get; set; }
        public string applicationName { get; set; }
    }
}
