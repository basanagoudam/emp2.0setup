﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.DesktopApp
{
    public class BrowserHistoryData
    {
        public int browserHistoryId { get; set; }
        public virtual string timeStamp { get; set; }
        public string browser { get; set; }
        public string url { get; set; }
    }

    public class KeystrokeHistoryData
    {
        public string keystroke_data { get; set; }

        public virtual string date { get; set; }
     
    }

}
