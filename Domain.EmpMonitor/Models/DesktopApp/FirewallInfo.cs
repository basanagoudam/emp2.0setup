﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.DesktopApp
{
    public class FirewallInfo
    {
        public string ipToIgnore { get; set; }
        public bool isToBlock { get; set; }
        public List<string> sites { get; set; }
    }
}
