﻿using Domain.EmpMonitor.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.DesktopApp
{
    public class LogDetails
    {
        public Logsheet objLogsheet { get; set; }
        public List<Logsheet> lstLogsheet { get; set; }
    }
}
