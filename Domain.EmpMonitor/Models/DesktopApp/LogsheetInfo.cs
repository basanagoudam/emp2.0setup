﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.DesktopApp
{
    public class LogsheetInfo
    {
        public string logsheetId { get; set; }
        public string loginTime { get; set; }
        public string logoutTime { get; set; }
        public string totalHours { get; set; }
        public string workingHours { get; set; }
        public string nonWorkingHours { get; set; }
        public string lastRetrievedTime { get; set; }
    }
}
