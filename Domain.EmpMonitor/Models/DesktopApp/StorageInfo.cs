﻿using Domain.EmpMonitor.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Domain.EmpMonitor.Models.DesktopApp
{
    //public class StorageInfo
    //{
    //    public int id { get; set; }
    //    public int transaction_id { get; set; }
    //    public int name { get; set; }
    //    public int registered_email { get; set; }
    //    public int is_activated { get; set; }
    //    public int is_email_verified { get; set; }
    //    public int license_count { get; set; }
    //    public int storage_data_id { get; set; }
    //    public int screenshot_capture_interval { get; set; }
    //    public int verification_token { get; set; }
    //    public int expiredate { get; set; }
    //    public int storage_type_id { get; set; }
    //    public int admin_email { get; set; }
    //    public int client_id { get; set; }
    //    public int client_secret { get; set; }
    //    public int token { get; set; }
    //    public int api_key { get; set; }
    //}





    public class StorageInfo
    {
        public bool success { get; set; }
        public string message { get; set; }
        public int screenshotsCaptureInterval { get; set; }
        public StorageTypes selectedStorageType { get; set; }
        public MediaFire mediaFireInfo { get; set; }
        public Dropbox dropboxInfo { get; set; }
        public AmazonCloudDrive amazonCloudDriveInfo { get; set; }
        public Mega megaCloudInfo { get; set; }
        public StorageData gdriveinfo { get; set; }
    }

    public enum StorageTypes
    {
        None,
        Dropbox,
        MediaFire,
        AmazonCloudDrive,
        Mega,
        GoogleDrive
    }
}