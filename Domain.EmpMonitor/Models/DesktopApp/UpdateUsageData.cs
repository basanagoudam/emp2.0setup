﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.DesktopApp
{
    public class UpdateUsageData
    {
        public bool success { get; set; }
        public string message { get; set; }
        public bool keystrokesUpdated { get; set; }
        public bool browserhistoryUpdated { get; set; }
        public bool applicationsUpdated { get; set; }

        public bool voiletpolicy { get; set; }
    }
}
