﻿using Domain.EmpMonitor.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Domain.EmpMonitor.Models.DesktopApp
{
    public class UserLogin
    {
        //[JsonProperty("userData")]
        //public UserData UserData { get; set; }

        //[JsonProperty("storageInfo")]
        //public StorageInfo StorageInfo { get; set; }

        //[JsonProperty("productionStat")]
        //public ProductionStat ProductionStat { get; set; }



        public int status { get; set; }
        public bool success { get; set; }
        public string message { get; set; }
        public User data { get; set; }
        public StorageInfo storageInfo { get; set; }
        public Logsheet objLogsheet { get; set; }
    }
}