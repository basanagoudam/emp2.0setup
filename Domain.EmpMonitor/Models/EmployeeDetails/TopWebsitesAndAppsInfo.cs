﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.EmployeeDetails
{
    public class TopWebsitesAndAppsInfo
    {
        public List<TopAppsInfo> topApps { get; set; }
        public string graphStringApps { get; set; }
        public List<TopWebsitesInfo> topWebsites { get; set; }
        public string graphStringWebsites { get; set; }
    }
}
