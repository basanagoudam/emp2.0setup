﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.EmployeeDetails
{
    public class TopWebsitesInfo
    {
        public Int64 count { get; set; }
        public string url { get; set; }
    }
}
