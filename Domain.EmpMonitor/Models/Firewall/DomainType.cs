﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.Firewall
{
    public class DomainType
    {
        public virtual int typeId { get; set; }
        public virtual string type { get; set; }
    }
    public class DomainsInformation
    {
        public virtual DomainType domainType { get; set; }
        public virtual Int32 websiteCount { get; set; }
    }
}
