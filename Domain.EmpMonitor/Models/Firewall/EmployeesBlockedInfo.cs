﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.Firewall
{
    public class EmployeesFirewallInfo
    {
        public int employeeId { get; set; }
        public string displayName { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string mobileNumber { get; set; }
        public string location { get; set; }
        public string department { get; set; }
        public Int64 action { get; set; }
        public string usbStatus { get; set; }
        public EmployeesFirewallInfo()
        {
            usbStatus = "3";
        }
    }
    //public class EmployeesUsblInfo
    //{
    //    public int employeeId { get; set; }
    //    public string displayName { get; set; }
    //    public string email { get; set; }
    //    public string password { get; set; }
    //    public string mobileNumber { get; set; }
    //    public string location { get; set; }
    //    public string department { get; set; }
    //    public string action { get; set; }
    //}
}