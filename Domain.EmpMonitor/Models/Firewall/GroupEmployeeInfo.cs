﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.Firewall
{
    public class GroupEmployeeInfo
    {
        public bool isInGroup { get; set; }
        public int employeeId { get; set; }
        public string displayName { get; set; }
        public string email { get; set; }
        public string location { get; set; }
        public string department { get; set; }
    }
}
