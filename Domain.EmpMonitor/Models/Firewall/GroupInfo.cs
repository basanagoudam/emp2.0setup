﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.Firewall
{
    public class GroupInfo
    {
        public int groupId { get; set; }
        public string groupName { get; set; }
        public int isBlocking { get; set; }
        public Int64 userCount { get; set; }
    }
}
