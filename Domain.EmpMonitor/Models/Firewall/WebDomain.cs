﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.Firewall
{
    public class WebDomain
    {
        public int domainId { get; set; }
        public string domain { get; set; }
        public DomainType typeInfo { get; set; }
        public bool selected { get; set; }
    }
    public class WebDomainCategory
    {
        public string categoryName { get; set; }
        public int  websiteCount { get; set; }
        public DomainType domain { get; set; }
        public bool selected { get; set; }
    }

    public class BlockWebsite
    {
        public int blockedwebsite_id { get; set; }
        public int website_id { get; set; }
        public int user_id { get; set; }
    }


    public class WebDomain_new
    {
        public int domainId { get; set; }
        public string domain { get; set; }
        public int type_id { get; set; }
      
    }



}
