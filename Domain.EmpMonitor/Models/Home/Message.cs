﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.Home
{
    public class Message
    {
        public Int64 message_id { get; set; }
        public bool isRead { get; set; }
        public string message { get; set; }
        public string message_subject { get; set; }
        public string date { get; set; }
    }
}
