﻿using Domain.EmpMonitor.Domain;
using System.Collections.Generic;
namespace Domain.EmpMonitor.Models.Home
{
    public class WorkingNonWorkingHours
    {
        public string workingHoursSingleDay = "00";
        public string nonWorkingHoursSingleDay = "00";
        public string workingHoursWeekly = "00";
        public string nonWorkingHoursWeekly = "00";
        public string workingHoursWeeklyAverage = "00";
        public string nonWorkingHoursWeeklyAverage = "00";
        public string graphStringProductivity { get; set; }
        public List<OrganizationProductivity> lstOrganizationProductivity { get; set; }
    }
    public class OrganizationProductivity
    {
        public string date { get; set; }
        public string workingHours { get; set; }
        public string nonWorkingHours { get; set; }
    }

    public class dashboard_data
    {
        public int total_employee { get; set; }
        public int tolat_online { get; set; }
        public int total_offline { get; set; }
        public int total_absent { get; set; }
        public List<UserDetails> objEmployeesoffline{get;set;}
        public List<UserDetails> objEmployeesabsent { get; set; }
    }
   

}