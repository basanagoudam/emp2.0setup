﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.EmpMonitor.Models.Reports
{
    public class GeneratedReportInfo
    {
        public bool isLogReportGenerated { get; set; }
        public string logDetailsReportPath { get; set; }
        public bool isKeystrokeGenerated { get; set; }
        public string keystrokesReportPath { get; set; }
        public bool isBrowserHistoryReportGenerated { get; set; }
        public string browserHistoryReportPath { get; set; }
        public bool isApplicationUsedReportGenerated { get; set; }
        public string applicationsUsedReportPath { get; set; }
        public bool isScreenshotReportGenerated { get; set; }
        public string screenshotsReportPath { get; set; }
        public bool isScreenshotVideoReportGenerated { get; set; }
        public string screenshotsVideoReportPath { get; set; }
        public bool isMergedReportGenerated { get; set; }
        public string mergedReportPath { get; set; }
        public bool isPatchGenerated { get; set; }
        public string patchPath { get; set; }
    }
}