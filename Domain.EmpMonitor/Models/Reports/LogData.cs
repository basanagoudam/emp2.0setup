﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.EmpMonitor.Models.Reports
{
    public class LogData
    {
        public string logsheetId { get; set; }
        public string loginTime { get; set; }
        public string logoutTime { get; set; }
        public string workingHours { get; set; }
        public string nonWorkingHours { get; set; }
        public string totalTime { get; set; }
    }
}

