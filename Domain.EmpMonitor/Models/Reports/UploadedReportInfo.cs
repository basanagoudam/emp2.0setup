﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.Reports
{
    public class UploadedReportInfo
    {
        public bool isLogReportUploaded { get; set; }
        public bool isKeystrokeReportUploaded { get; set; }
        public bool isBrowserHistoryReportUploaded { get; set; }
        public bool isApplicationUsedReportUploaded { get; set; }
        public bool isScreenshotReportUploaded { get; set; }
        public bool isScreenshotVideoReportUploaded { get; set; }
        public bool isMergedReportUploaded { get; set; }
    }
}
