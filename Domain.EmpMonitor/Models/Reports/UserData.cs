﻿using Domain.EmpMonitor.Models.DesktopApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.EmpMonitor.Models.Reports
{
    public class UserData
    {
        public int companyId { get; set; }
        public string email { get; set; }
        public string dateOfEntry { get; set; }
        public string displayName { get; set; }
        public string location { get; set; }
        public string department { get; set; }
        public LogData logInfo { get; set; }
    }
}
