﻿using Newtonsoft.Json;

namespace EmpMonitorStealth.Models
{
    class ServerTime
    {
        [JsonProperty("dateTime")]
        public string DateTime { get; set; }

        [JsonProperty("timeStamp")]
        public string TimeStamp { get; set; }

        [JsonProperty("ztime")]
        public string Ztime { get; set; }
    }
}
