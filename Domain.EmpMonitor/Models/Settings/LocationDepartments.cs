﻿using Domain.EmpMonitor.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.Settings
{
    public class LocationDepartments
    {
        public Location location { get; set; }
        public List<Department> departments { get; set; }
    }
}
