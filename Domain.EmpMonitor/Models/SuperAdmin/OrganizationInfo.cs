﻿using Domain.EmpMonitor.Models.DesktopApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.EmpMonitor.Models.SuperAdmin
{
    public class OrganizationInfo
    {
        //public int organization_id { get; set; }
        //public string organization_name { get; set; }
        //public string registered_email { get; set; }
        //public string transaction_id { get; set; }
        //public int license_count { get; set; }
        //public int is_activated { get; set; }
        //public int is_email_verified { get; set; }
        //public StorageTypes storage_type { get; set; }
        //public int screenshot_capture_interval { get; set; }
        //public int verification_token { get; set; }
        public virtual int organizationId { get; set; }
        public virtual string transactionId { get; set; }
        public virtual string organizationName { get; set; }
        public virtual string registeredEmailId { get; set; }
        
        public virtual int licenseCount { get; set; }
        public virtual bool isActivated { get; set; }
        public virtual bool isEmailVerified { get; set; }
        public virtual int storageTypeId { get; set; }
        public virtual string storageType { get; set; }
        public virtual string verificationToken { get; set; }
        public virtual int screenshotCaptureInterval { get; set; }
        
    }
}
