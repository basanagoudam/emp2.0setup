﻿using Newtonsoft.Json;

namespace EmpMonitorStealth.Models
{
    class UserCreds
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
