﻿using System;
using System.IO;

namespace EmpMonitorStealth.Classes
{
    public static class ErrorLogging
    {
        static string ErrorLogFilePath = Globals.ErrorLogFolderPath + @"\ErrorLog.txt";

        public static void log(object Error)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(ErrorLogFilePath, true))
                {
                    using (StringReader reader = new StringReader("ClientApp => " + InternalClock.Now.ToString("dd-MMM-yyyy hh:mm:ss tt => ") + Error.ToString()))
                    {
                        string temptext = "";
                        while ((temptext = reader.ReadLine()) != null)
                        {
                            try
                            {
                                writer.WriteLine(temptext);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.StackTrace + ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace + ex.Message);
            }
        }
    }
}
