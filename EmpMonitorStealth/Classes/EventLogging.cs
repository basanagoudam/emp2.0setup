﻿using System;
using System.IO;

namespace EmpMonitorStealth.Classes
{
    public static class EventLogging
    {
        static string EventLogFilePath = Globals.EventLogFolderPath + @"\EventLog.txt";
        //static string EventLogFilePath = @"D:\EventLog.txt";

        public static void log(object Event)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(EventLogFilePath, true))
                {
                    using (StringReader reader = new StringReader("ClientApp => " + InternalClock.Now.ToString("dd-MMM-yyyy hh:mm:ss tt => ") + Event.ToString()))
                    {
                        string temptext = "";
                        while ((temptext = reader.ReadLine()) != null)
                        {
                            try
                            {
                                writer.WriteLine(temptext);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.StackTrace + ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace + ex.Message);
            }
        }
    }
}
