﻿using Domain.EmpMonitor.Models.DesktopApp;
using System;
using System.Collections.Generic;

namespace EmpMonitorStealth.Classes
{
    class Globals
    {
        public static string LocalAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        public static string LockedFolderPath = LocalAppDataPath + @"\Details";
        public static string MainFolderPath = LockedFolderPath + @"\Screen";
        public static string UserDetailsFolderPath = MainFolderPath + @"\UserDetails";
        public static string UserDetailsFilePath = UserDetailsFolderPath + @"\Email.txt";
        public static string ErrorLogFolderPath = MainFolderPath + @"\ErrorLog";
        public static string EventLogFolderPath = MainFolderPath + @"\EventLog";
        public static string UserFolderPath = string.Empty;
        public static string CurrentDateFolderPath = string.Empty;
        public static string SessionFolderPath = string.Empty;
        public static string SessionFilePath = string.Empty;
        public static string KeystrokesFolderPath = string.Empty;
        public static string KeystrokesFilePath = string.Empty;
        public static string BrowserHistoriesFolderPath = string.Empty;
        public static string BrowserHistoriesFilePath = string.Empty;
        public static string ApplicationsUsedFolderPath = string.Empty;
        public static string ApplicationsUsedFilePath = string.Empty;
        public static string ScreenshotsFolderPath = string.Empty;
        public static string ApplicationTotalTimePath = string.Empty;
        public static string BrowserTotalTimepath = string.Empty;



        public static int UserId = 0;
        public static string Email = string.Empty;
        public static string Password = string.Empty;
        public static string AccessToken = string.Empty;
        public static string DropboxAccessToken = string.Empty;
        public static int ImageCaptureInterval = 15;
        public static string SessionID = string.Empty;
        public static string KeystrokesSessionID = string.Empty;
        public static string CurrentSessionDate = string.Empty;
        public static bool isSystemTimeAccurate = true;
        public static TimeSpan localServerTimeDiff = new TimeSpan();
        public static TimeSpan inactiveTime = new TimeSpan();
        public static bool isSignOut = false;
        public static bool AutoUpdate = false;
        public static bool internalClose = false;
        public static string CurrentAppVersion = string.Empty;
        public static string CurrentAutoUpdaterAppVersion = string.Empty;
        public static TimeSpan WorkingHours = new TimeSpan();
        public static TimeSpan NonWorkingHours = new TimeSpan();
        public static IntPtr ApplicationHandle = IntPtr.Zero;
        public static StorageTypes selectedStorageType;
        //public static MediaFire objMediaFire;
        //public static AmazonCloudDrive objAmazonCloudDrive;
        //public static Mega objMegaDrive;
        public static Dictionary<string, string> NotWorkingInfo = new Dictionary<string, string>()
        {
            { "keystroke","" } ,
            { "website","" },
            { "application","" },
            { "screenshot","" }
        };
        public static string ClientId { get; set; }
        public static string ClientSecret { get; set; }
        public static string RefreshToken { get; set; }
        /// <summary>
        /// Generated the Unix Timestamp
        /// </summary>
        /// <returns>Unix timestamp</returns>
        public static string generateUnixTimeStamp(DateTime dateTime)
        {
            return Convert.ToDouble(dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds).ToString();
        }
    }
}
