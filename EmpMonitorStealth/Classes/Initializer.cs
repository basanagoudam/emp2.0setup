﻿using Domain.EmpMonitor.Classes;
using Domain.EmpMonitor.Domain;
using EmpMonitorStealth.Helper;
using EmpMonitorStealth.Models;
//using EmpMonitorStealth.Api.DesktopApp;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
//using System.Web.Script.Serialization;

namespace EmpMonitorStealth.Classes
{
    public class Initializer
    {
        public static void start()
        {
            try
            {
                string userCreds = checkUserFileInfo();
                if (!string.IsNullOrEmpty(userCreds))
                {
                    string username = Regex.Split(userCreds, "<:><:><:>")[0];
                    string password = Regex.Split(userCreds, "<:><:><:>")[1];
                    if (Utility.isEmail(username))
                    {
                        Globals.Email = username;
                        Globals.UserFolderPath = Globals.MainFolderPath + @"\" + username;
                        if (!Directory.Exists(Globals.UserFolderPath))
                        {
                            Directory.CreateDirectory(Globals.UserFolderPath);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public static string checkUserFileInfo()
        {
            EventLogging.log("Checking for user creds");
            try
            {
                if (!Directory.Exists(Globals.LockedFolderPath))
                {
                    EventLogging.log("Inside locked folder path");
                    createFolders();
                    return null;
                }
                if (!Directory.Exists(Globals.MainFolderPath))
                {
                    EventLogging.log("Inside Main folder path");
                    createFolders();
                    return null;
                }
                if (!Directory.Exists(Globals.UserDetailsFolderPath))
                {
                    EventLogging.log("Inside userdetails folder path");
                    createFolders();
                    return null;
                }
                if (File.Exists(Globals.UserDetailsFilePath))
                {
                    EventLogging.log("Inside Userdetails path");
                    string userCreds = File.ReadAllText(Globals.UserDetailsFilePath);
                    EventLogging.log("Got user Creds====="+userCreds);
                    string[] userCredsArr = Regex.Split(userCreds, "<:><:><:>");

                    if (userCredsArr.Length == 2)
                    {
                        UserCreds userDetails = new UserCreds() { Email = userCredsArr[0], Password = userCredsArr[1] };
                        EventLogging.log("Psoting the data");
                        var postDetails = JsonConvert.SerializeObject(userDetails);
                        EventLogging.log("Serialized data=====" +postDetails);
                        var httpContent = new StringContent(postDetails, Encoding.UTF8, "application/json");
                        string checkUserIsValid = HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "login");
                        EventLogging.log("Api Response=====" + checkUserIsValid);
                        JObject jObject = JObject.Parse(checkUserIsValid);
                        var response = jObject["data"].ToString();
                        EventLogging.log("this is response"+response);
                        if (response != "True")
                            return null;
                    }
                    if (Utility.isEmail(userCredsArr[0]))
                    {
                        EventLogging.log("Inside utility.IsEmail");
                        Globals.UserFolderPath = Globals.MainFolderPath + @"\" + userCredsArr[0];
                        Globals.Email = userCredsArr[0];
                        EventLogging.log("returning  "+Globals.Email);
                        return userCreds;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public static void checkServerTime()
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
        }
        public static bool createFolders()
        {
            try
            {
                if (!Directory.Exists(Globals.LockedFolderPath))
                {
                    Directory.CreateDirectory(Globals.LockedFolderPath);
                    //FolderLock.LockFolder(Globals.LockedFolderPath, true);
                }
                if (!Directory.Exists(Globals.MainFolderPath))
                {
                    //FolderLock.LockFolder(Globals.LockedFolderPath, false);
                    Directory.CreateDirectory(Globals.MainFolderPath);
                    //FolderLock.LockFolder(Globals.LockedFolderPath, true);
                }
                if (!Directory.Exists(Globals.UserDetailsFolderPath))
                {
                    Directory.CreateDirectory(Globals.UserDetailsFolderPath);
                }
                if (!Directory.Exists(Globals.ErrorLogFolderPath))
                {
                    Directory.CreateDirectory(Globals.ErrorLogFolderPath);
                }
                if (!Directory.Exists(Globals.EventLogFolderPath))
                {
                    Directory.CreateDirectory(Globals.EventLogFolderPath);
                }
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        public static bool createSessionFolders(string date)
        {
            try
            {
                Globals.CurrentDateFolderPath = Globals.UserFolderPath + @"\" + Globals.CurrentSessionDate;
                if (!Directory.Exists(Globals.CurrentDateFolderPath))
                {
                    Directory.CreateDirectory(Globals.CurrentDateFolderPath);
                }
                Globals.SessionFolderPath = Globals.CurrentDateFolderPath + @"\Sessions";
                if (!Directory.Exists(Globals.SessionFolderPath))
                {
                    Directory.CreateDirectory(Globals.SessionFolderPath);
                }
                Globals.SessionFilePath = Globals.SessionFolderPath + @"\Session.txt";
                Globals.KeystrokesFolderPath = Globals.CurrentDateFolderPath + @"\Keystrokes";
                if (!Directory.Exists(Globals.KeystrokesFolderPath))
                {
                    Directory.CreateDirectory(Globals.KeystrokesFolderPath);
                }
                Globals.KeystrokesFilePath = Globals.KeystrokesFolderPath + @"\Keystrokes.txt";
                Globals.BrowserHistoriesFolderPath = Globals.CurrentDateFolderPath + @"\BrowserHistories";
                if (!Directory.Exists(Globals.BrowserHistoriesFolderPath))
                {
                    Directory.CreateDirectory(Globals.BrowserHistoriesFolderPath);
                }
                Globals.BrowserHistoriesFilePath = Globals.BrowserHistoriesFolderPath + @"\BrowserHistories.txt";
                Globals.ApplicationsUsedFolderPath = Globals.CurrentDateFolderPath + @"\ApplicationsUsed";
                if (!Directory.Exists(Globals.ApplicationsUsedFolderPath))
                {
                    Directory.CreateDirectory(Globals.ApplicationsUsedFolderPath);
                }
                Globals.ApplicationsUsedFilePath = Globals.ApplicationsUsedFolderPath + @"\ApplicationsUsed.txt";
                Globals.ScreenshotsFolderPath = Globals.CurrentDateFolderPath + @"\Screenshots";
                if (!Directory.Exists(Globals.ScreenshotsFolderPath))
                {
                    Directory.CreateDirectory(Globals.ScreenshotsFolderPath);
                }

                Globals.ApplicationsUsedFolderPath = Globals.CurrentDateFolderPath + @"\TimeSpentOnApplication";
                if (!Directory.Exists(Globals.ApplicationsUsedFolderPath))
                {
                    Directory.CreateDirectory(Globals.ApplicationsUsedFolderPath);
                }
                Globals.ApplicationsUsedFilePath = Globals.ApplicationsUsedFolderPath + @"\TimeSpentOnApplication.txt";

                Globals.ApplicationsUsedFolderPath = Globals.CurrentDateFolderPath + @"\TimeSpentOnBrowserTab";
                if (!Directory.Exists(Globals.ApplicationsUsedFolderPath))
                {
                    Directory.CreateDirectory(Globals.ApplicationsUsedFolderPath);
                }
                Globals.ApplicationsUsedFilePath = Globals.ApplicationsUsedFolderPath + @"\TimeSpentOnBrowserTab.txt";
                
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
        
        public static Logsheet checkForExistingSession()
        {
            try
            {
                if (!Directory.Exists(Globals.UserFolderPath))
                {
                    Directory.CreateDirectory(Globals.UserFolderPath);
                }

                string date = InternalClock.Now.ToString("yyyy-MM-dd");
                if (!Directory.Exists(Globals.UserFolderPath + @"\" + date))
                {
                    //date = InternalClock.Now.ToString("dd-MMM-yyyy");
                    date = InternalClock.Now.AddDays(-1).ToString("yyyy-MM-dd");
                    if (!Directory.Exists(Globals.UserFolderPath + @"\" + date))
                    {
                        return null;
                    }
                }

                Logsheet objLogsheet = null;
                TimeSpan presentLoginLastLogoutDiff = new TimeSpan();
                TimeSpan lastLoginLastLogoutDiff = new TimeSpan();

                if (!File.Exists(Globals.UserFolderPath + @"\" + date + @"\Sessions\Session.txt"))
                {
                    try
                    {
                        //do it
                        //objLogsheet = new JavaScriptSerializer().Deserialize<Logsheet>(new DesktopApp().GetLatestSession(Utility.generateAccessToken("", ""), Globals.Email));
                        presentLoginLastLogoutDiff = HomePage.LoginTime.Subtract(DateTime.Parse(objLogsheet.logoutTime));
                        lastLoginLastLogoutDiff = DateTime.Parse(objLogsheet.logoutTime).Subtract(DateTime.Parse(objLogsheet.loginTime));
                        if (objLogsheet != null && presentLoginLastLogoutDiff <= new TimeSpan(2, 0, 0) && lastLoginLastLogoutDiff <= new TimeSpan(20, 0, 0))
                        {
                            Globals.inactiveTime = presentLoginLastLogoutDiff;
                            HomePage.NonWorkingHours = HomePage.NonWorkingHours.Add(Globals.inactiveTime);
                            return objLogsheet;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    return null;
                }
                string sessionInfo = File.ReadAllText(Globals.UserFolderPath + @"\" + date + @"\Sessions\Session.txt");
                if (string.IsNullOrEmpty(sessionInfo))
                {
                    return null;
                }
                objLogsheet = new JavaScriptSerializer().Deserialize<Logsheet>(sessionInfo);
                //if (objLogsheet == null || objLogsheet.lastUpdateTime == null)
                //{
                //    return null;
                //}
                //DateTime lastUpdateTime;
                //if (!DateTime.TryParse(objLogsheet.lastUpdateTime, out lastUpdateTime))
                //{
                //    return null;
                //}
                DateTime loginTime;
                if (!DateTime.TryParse(objLogsheet.loginTime, out loginTime))
                {

                    return null;
                }
                TimeSpan totalHours;
                if (!TimeSpan.TryParse(objLogsheet.totalHours, out totalHours))
                {
                    return null;
                }
                TimeSpan te = new TimeSpan(365, 23, 59, 59);
                te = te.Add(new TimeSpan(0, 0, 1));
                presentLoginLastLogoutDiff = HomePage.LoginTime.Subtract(DateTime.Parse(objLogsheet.logoutTime));
                lastLoginLastLogoutDiff = DateTime.Parse(objLogsheet.logoutTime).Subtract(DateTime.Parse(objLogsheet.loginTime));
                if (presentLoginLastLogoutDiff <= new TimeSpan(2, 0, 0) && lastLoginLastLogoutDiff <= new TimeSpan(20, 0, 0))
                {
                    Globals.inactiveTime = presentLoginLastLogoutDiff;
                    HomePage.NonWorkingHours = HomePage.NonWorkingHours.Add(Globals.inactiveTime);
                    return objLogsheet;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
            return null;
        }
        public static ProductionStat checkForExistingSession2()
        {
            try
            {
                if (!Directory.Exists(Globals.UserFolderPath))
                {
                    Directory.CreateDirectory(Globals.UserFolderPath);
                }

                string date = InternalClock.Now.ToString("yyyy-MM-dd");
                if (!Directory.Exists(Globals.UserFolderPath + @"\" + date))
                {
                    //date = InternalClock.Now.ToString("dd-MMM-yyyy");
                    date = InternalClock.Now.AddDays(-1).ToString("yyyy-MM-dd");
                    if (!Directory.Exists(Globals.UserFolderPath + @"\" + date))
                    {
                        return null;
                    }
                }

                ProductionStat objLogsheet = null;
                TimeSpan presentLoginLastLogoutDiff = new TimeSpan();
                TimeSpan lastLoginLastLogoutDiff = new TimeSpan();

                if (!File.Exists(Globals.UserFolderPath + @"\" + date + @"\Sessions\Session.txt"))
                {
                    try
                    {
                        //do it
                        //objLogsheet = new JavaScriptSerializer().Deserialize<Logsheet>(new DesktopApp().GetLatestSession(Utility.generateAccessToken("", ""), Globals.Email));
                        presentLoginLastLogoutDiff = HomePage.LoginTime.Subtract(DateTime.Parse(objLogsheet.logout_time));
                        lastLoginLastLogoutDiff = DateTime.Parse(objLogsheet.logout_time).Subtract(DateTime.Parse(objLogsheet.login_time));
                        if (objLogsheet != null && presentLoginLastLogoutDiff <= new TimeSpan(2, 0, 0) && lastLoginLastLogoutDiff <= new TimeSpan(20, 0, 0))
                        {
                            Globals.inactiveTime = presentLoginLastLogoutDiff;
                            HomePage.NonWorkingHours = HomePage.NonWorkingHours.Add(Globals.inactiveTime);
                            return objLogsheet;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    return null;
                }
                string sessionInfo = File.ReadAllText(Globals.UserFolderPath + @"\" + date + @"\Sessions\Session.txt");
                if (string.IsNullOrEmpty(sessionInfo))
                {
                    return null;
                }
                objLogsheet = new JavaScriptSerializer().Deserialize<ProductionStat>(sessionInfo);
                //if (objLogsheet == null || objLogsheet.lastUpdateTime == null)
                //{
                //    return null;
                //}
                //DateTime lastUpdateTime;
                //if (!DateTime.TryParse(objLogsheet.lastUpdateTime, out lastUpdateTime))
                //{
                //    return null;
                //}
                DateTime loginTime;
                if (!DateTime.TryParse(objLogsheet.login_time, out loginTime))
                {

                    return null;
                }
                TimeSpan totalHours;
                if (!TimeSpan.TryParse(objLogsheet.total_hours, out totalHours))
                {
                    return null;
                }
                TimeSpan te = new TimeSpan(365, 23, 59, 59);
                te = te.Add(new TimeSpan(0, 0, 1));
                presentLoginLastLogoutDiff = HomePage.LoginTime.Subtract(DateTime.Parse(objLogsheet.logout_time));
                lastLoginLastLogoutDiff = DateTime.Parse(objLogsheet.logout_time).Subtract(DateTime.Parse(objLogsheet.login_time));
                if (presentLoginLastLogoutDiff <= new TimeSpan(2, 0, 0) && lastLoginLastLogoutDiff <= new TimeSpan(20, 0, 0))
                {
                    Globals.inactiveTime = presentLoginLastLogoutDiff;
                    HomePage.NonWorkingHours = HomePage.NonWorkingHours.Add(Globals.inactiveTime);
                    return objLogsheet;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
            return null;
        }
    }
}