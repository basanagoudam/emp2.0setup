﻿using System;

namespace EmpMonitorStealth.Classes
{
    public class InternalClock
    {
        private static TimeSpan _diff = new TimeSpan();
        public static DateTime Now { get { return DateTime.Now.Subtract(_diff); } }
        public static void setTimeDifference(TimeSpan diff)
        {
            try
            {
                _diff = diff;
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex);
            }
        }
    }
}
