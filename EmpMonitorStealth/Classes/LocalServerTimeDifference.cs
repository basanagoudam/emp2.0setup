﻿using System;

namespace EmpMonitorStealth.Classes
{
    public class LocalServerTimeDifference
    {
        public TimeSpan localServerTimeDifference { get; set; }
        public DateTime lastFetchedTime { get; set; }
    }
}