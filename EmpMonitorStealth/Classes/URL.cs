﻿using System;

namespace EmpMonitorStealth.Classes
{
    public class URL
    {
        public string URl = string.Empty;
        public string Title = string.Empty;
        public string BrowserName = string.Empty;
        public DateTime LastVisited;

        public URL(string url, string title, string browser, DateTime lastVisited)
        {
            URl = url;
            Title = title;
            BrowserName = browser;
            LastVisited = lastVisited;
        }
    }
}
