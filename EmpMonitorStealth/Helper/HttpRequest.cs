﻿using Domain.EmpMonitor.Domain;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace EmpMonitorStealth.Helper
{
    class HttpRequest
    {
        static HttpClient httpClientObj = null;
        public static CookieCollection CookieCollection = new CookieCollection();
        static HttpRequest()
        {
            CookieContainer cookies = new CookieContainer();
            cookies.Add(CookieCollection);
            HttpClientHandler handler = new HttpClientHandler
            {
                CookieContainer = cookies
            };
            httpClientObj = new HttpClient(handler);
        }
        string basePath = null;
        internal HttpRequest(string controllerPath)
        {
            basePath = controllerPath;
        }
        private static HttpClient httpClient { get; set; }
        public HttpResponseMessage PostRequest(string action, List<KeyValuePair<string, string>> Parameters)
        {
            httpClient = httpClient ?? new HttpClient();
            var content = new FormUrlEncodedContent(Parameters);
            HttpResponseMessage response = httpClient.PostAsync(basePath + action, content).Result;
            return response;
        }
        public static HttpResponseMessage GetRequest(string action, string services)
        {
            httpClient = httpClient ?? new HttpClient();
            //var content = new FormUrlEncodedContent(Parameters);
            HttpResponseMessage response = httpClient.GetAsync(action + services).Result;
            var customerJsonString = response.Content.ReadAsStringAsync().Result;
            return response;
        }

        static internal DesktopControl GetStatus(string emailId)
        {
            string status = httpClientObj.GetStringAsync("http://api.empmonitor.com/api/Desktop/DesktopControlSetting?accessToken=%22%22&EmailId=" + emailId).Result;
            if (!string.IsNullOrEmpty(status))
                return JsonConvert.DeserializeObject<DesktopControl>(status);
            return new DesktopControl();
        }
    }
}
