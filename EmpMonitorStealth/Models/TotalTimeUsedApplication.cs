﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmpMonitorStealth.Models
{
    public class TotalTimeUsedApplication
    {
        [JsonProperty("timestamp")]
        public string timeStamp { get; set; }

        [JsonProperty("name")]
        public string applicationName { get; set; }

        [JsonProperty("time")]
        public string totaltimeused { get; set; }
    }
}
