﻿namespace EmpMonitorStealth.Models
{
    class UserInfo
    {
        public int code { get; set; }
        public string error { get; set; }
        public string message { get; set; }
        public UserFullData userFullData { get; set; }
    }
    public class UserFullData
    {
        public UserData UserData { get; set; }
        public StorageInfo2 StorageInfo { get; set; }
        public ProductionStat ProductionStat { get; set; }
    }
    public class ProductionStat
    {
        //public int id { get; set; }
        public string log_sheet_id { get; set; }
        public string day { get; set; }
        public string login_time { get; set; }
        public string logout_time { get; set; }
        //public int user_id { get; set; }
        public string working_hours { get; set; }
        public string non_working_hours { get; set; }
        public string total_hours { get; set; }
        public bool is_report_generated { get; set; }
        //public string created_at { get; set; }
        //public string updated_at { get; set; }
    }

    public class StorageInfo2
    {
        public int id { get; set; }
        public string transaction_id { get; set; }
        public string name { get; set; }
        public string registered_email { get; set; }
        public int? is_activated { get; set; }
        public int? is_email_verified { get; set; }
        public int? license_count { get; set; }
        public StorageTypes1? storage_data_id { get; set; }
        public int screenshot_capture_interval { get; set; }
        public string verification_token { get; set; }
        public string expiredate { get; set; }
        public StorageTypes1 storage_type_id { get; set; }
        public string admin_email { get; set; }
        public string client_id { get; set; }
        public string client_secret { get; set; }
        public string refresh_token { get; set; }
        public string api_key { get; set; }
        public string token { get; set; }
    }
    public enum StorageTypes1
    {
        None,
        GoogleDrive,
        Dropbox,
        MediaFire,
        AmazonCloudDrive,
        Mega
    }

    public class UserData
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string emp_code { get; set; }
        public string location_id { get; set; }
        public string department_id { get; set; }
        public string date_join { get; set; }
        public string photo_path { get; set; }
        public string address { get; set; }
        public int role_id { get; set; }
    }
}
