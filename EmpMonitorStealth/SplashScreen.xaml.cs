﻿using Domain.EmpMonitor.Classes;
using Domain.EmpMonitor.Domain;
using Domain.EmpMonitor.Models.DesktopApp;
using EmpMonitorStealth.Classes;
using EmpMonitorStealth.Helper;
using EmpMonitorStealth.Models;
using Microsoft.Win32;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;

namespace EmpMonitorStealth
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        private const int WM_QUERYENDSESSION = 0x0011;
        private const int WM_ENDSESSION = 0x0016;
        private const int ENDSESSION_CLOSEAPP = 0x1;
        private const int ENDSESSION_CRITICAL = 0x40000000;
        private const int WM_DESTROY = 0x0002;

        StealthRawStuff.InputDevice id;
        IntPtr handle = IntPtr.Zero;
        System.Windows.Forms.Message message = new System.Windows.Forms.Message();
        public SplashScreen()
        {
            //TestEventLog.log("Inside the displayhost2");
            setInternalClock();
            //TestEventLog.log("setInternalClock completed");
            InitializeComponent();
            //TestEventLog.log("Initialize Component completed");
            EventLogging.log("Constructor Called");
            HomePage.LoginTime = InternalClock.Now;
            HomePage.lastInputTime = Environment.TickCount;
            Globals.CurrentSessionDate = InternalClock.Now.ToString("yyyy-MM-dd");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
         {
            EventLogging.log("Window Loaded");
            try
            {
                //TestEventLog.log("Start Wnd Proc Handler");
                StartWndProcHandler();
                //TestEventLog.log("WindowLoad Thread starts");
                Thread threadLoadWindow = new Thread(WindowLoad);
                threadLoadWindow.IsBackground = true;
                threadLoadWindow.Start();
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }

        private void WindowLoad()
        {
            try
            {
                EventLogging.log("i am inside Window Load");
                string userCreds = Initializer.checkUserFileInfo();
                EventLogging.log("<=====got user creds==>>"+userCreds);
                if (string.IsNullOrEmpty(userCreds))
                {
                    EventLogging.log("showing window==>>");
                    showWindow();
                    return;
                }
                else
                {
                    EventLogging.log("Already logged in");
                    hideApp();
                    EventLogging.log("App hidden");
                    if (Utility.CheckForInternetConnection("https://www.google.com"))
                    {
                        AppType appType = new AppType() { app_type = "Test" };
                        var serializeApptype = JsonConvert.SerializeObject(appType);
                        var httpContent = new StringContent(serializeApptype, Encoding.UTF8, "application/json");
                        var updateUsageDataResponse = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "application-info"));
                        List<ApplicationInfo> objApplicationInfo = JsonConvert.DeserializeObject<List<ApplicationInfo>>(updateUsageDataResponse["data"].ToString());
                        string version = Utility.getCallingAssemblyVersion();
                        if (objApplicationInfo != null && !objApplicationInfo[0].version.Equals(version))
                        {
                            EventLogging.log("Application version different. Latest application version" + objApplicationInfo[0].version + ". Current application version " + version);
                            Globals.internalClose = true;
                            closeApp();
                            return;
                        }
                    }
                    if (!HomePage.Start(Regex.Split(userCreds, "<:><:><:>")[0], Regex.Split(userCreds, "<:><:><:>")[1]))
                    {
                        Globals.internalClose = true;
                        closeApp();
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogging.log("Exception " + ex.Message);
                ErrorLogging.log(ex.ToString());
                Globals.internalClose = true;
                closeApp();
            }
        }

        void StartWndProcHandler()
        {
            EventLogging.log("Hooking keyboard and mouse");
            Window myWin = null;
            myWin = System.Windows.Application.Current.MainWindow;
            try
            {
                Globals.ApplicationHandle = handle = new WindowInteropHelper(myWin).Handle;
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }

            //Get the Hwnd source   
            HwndSource source = HwndSource.FromHwnd(handle);
            //Win32 queue sink
            source.AddHook(new HwndSourceHook(WndProc));

            id = new StealthRawStuff.InputDevice(source.Handle);
            EventLogging.log("Enumerated devices : " + id.EnumerateDevices());
            id.KeyPress += new System.Windows.Forms.KeyPressEventHandler(HomePage.MyKeyPress);
            EventLogging.log("Events hooked to methods");
        }


        private void txtEmail_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Login();
            }
        }


        private void txtPassword_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Login();
            }
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void showWindow()
        {
            this.Dispatcher.Invoke(new Action(delegate
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.WindowState = WindowState.Normal;
                this.ShowInTaskbar = true;
                this.Opacity = 1;
            }));
        }

        private void hideApp()
        {
            this.Dispatcher.Invoke(new Action(delegate
            {
                this.Hide();
            }));
        }

        private void closeApp()
        {
            this.Dispatcher.Invoke(new Action(delegate
            {
                this.Close();
            }));
        }

        private void Login()
        {
            try
            {
                string Email = txtEmail.Text;
                if (!string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(txtPassword.Password))
                {
                    string Result = string.Empty;
                    if (!Utility.isEmail(Email))
                    {
                        System.Windows.Forms.MessageBox.Show("Please enter a valid email address");
                        return;
                    }
                    //if (!Utility.CheckForInternetConnection("https://www.google.com"))
                    //{
                    //    System.Windows.Forms.MessageBox.Show(@"No Internet Connection", "EmpMonitor " + Globals.CurrentAppVersion);
                    //    return;
                    //}
                    string objUserLogin = HomePage.FirstTimeLogin(Email, Utility.Encrypt(txtPassword.Password));
                    if (objUserLogin != null && objUserLogin == "Successfully Logged in.")
                    {
                        File.WriteAllText(Globals.UserDetailsFilePath, Email + "<:><:><:>" + Utility.Encrypt(txtPassword.Password));
                        EventLogging.log("User Details saved in txt file");
                        System.Windows.Forms.MessageBox.Show("Login successful"); this.Close();
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show(objUserLogin, "EmpMonitor " + Globals.CurrentAppVersion);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Can not leave the Email and Password field empty", "EmpMonitor " + Globals.CurrentAppVersion);
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
                System.Windows.Forms.MessageBox.Show(ex.ToString());
                System.Windows.Forms.MessageBox.Show("Unknown error", "EmpMonitor " + Globals.CurrentAppVersion);
            }
        }

        public IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WM_QUERYENDSESSION)
            {
                if (!Globals.isSignOut && !Globals.AutoUpdate && !Globals.internalClose)
                {
                    HomePage.signOut();
                    return IntPtr.Zero;
                }
            }
            else if (msg == WM_ENDSESSION)
            {
                if (!Globals.isSignOut && !Globals.AutoUpdate && !Globals.internalClose)
                {
                    HomePage.signOut();
                }
            }
            if (id != null)
            {
                message.HWnd = hwnd;
                message.Msg = msg;
                message.LParam = lParam;
                message.WParam = wParam;

                id.ProcessMessage(message);
            }
            return IntPtr.Zero;
        }

        #region SetInternalClock
        private static bool isTimeChangedEventSubscribed = false;
        private static void setInternalClock()
        {
            TestEventLog.log("Inside the setInternalClock");
            EventLogging.log("setInternalClock() Called");
            if (Utility.CheckForInternetConnection("https://www.google.com"))
            {
                System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
                DateTime fecthTime = DateTime.Now;
                watch.Start();
                var servertime = HttpHelper.GetRequest(AppSettings.UserUrl, "server-time");
                JObject jObject = JObject.Parse(servertime);
                var te = JsonConvert.DeserializeObject<ServerTime>(jObject["data"].ToString()).DateTime;
                watch.Stop();
                TimeSpan elapsedTime = TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds / 2);
                if (te != null)
                {
                    DateTime serverTime = DateTime.ParseExact(te, "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    DateTime convertedLocalTime = serverTime;
                    //DateTime convertedLocalTime = serverTime.ToLocalTime();
                    //InternalClock.startClock(convertedLocalTime);
                    TimeSpan diff = fecthTime.Subtract(convertedLocalTime);
                    InternalClock.setTimeDifference(diff);
                    LocalServerTimeDifference objLocalServerTimeDifference = new LocalServerTimeDifference { lastFetchedTime = convertedLocalTime, localServerTimeDifference = diff };
                    if (Directory.Exists(Globals.MainFolderPath))
                    {
                        if (!Directory.Exists(Globals.MainFolderPath + @"\Time"))
                        {
                            Directory.CreateDirectory(Globals.MainFolderPath + @"\Time");
                        }
                        File.WriteAllText(Globals.MainFolderPath + @"\Time\LocalServerTimeDifference.txt", new JavaScriptSerializer().Serialize(objLocalServerTimeDifference));
                    }
                    Globals.localServerTimeDiff = diff;
                    if (!isTimeChangedEventSubscribed)
                    {
                        SystemEvents.TimeChanged += SystemEvents_TimeChanged;
                        isTimeChangedEventSubscribed = true;
                    }
                }
                if (Globals.localServerTimeDiff > new TimeSpan(0, 0, 1, 0) || Globals.localServerTimeDiff < new TimeSpan(0, 0, -1, 0))
                {
                    EventLogging.log("Server Local Time Diff : " + Globals.localServerTimeDiff.ToString());
                    Globals.isSystemTimeAccurate = false;
                }
            }
            else
            {
                EventLogging.log("Internet connection not available to check server time.");
                if (File.Exists(Globals.MainFolderPath + @"\Time\LocalServerTimeDifference.txt"))
                {
                    string fileContent = File.ReadAllText(Globals.MainFolderPath + @"\Time\LocalServerTimeDifference.txt");
                    if (!string.IsNullOrEmpty(fileContent))
                    {
                        try
                        {
                            LocalServerTimeDifference objLocalServerTimeDifference = new JavaScriptSerializer().Deserialize<LocalServerTimeDifference>(fileContent);
                            if (objLocalServerTimeDifference.localServerTimeDifference != null)
                            {
                                InternalClock.setTimeDifference(objLocalServerTimeDifference.localServerTimeDifference);
                                Globals.localServerTimeDiff = objLocalServerTimeDifference.localServerTimeDifference;
                                if (!isTimeChangedEventSubscribed)
                                {
                                    SystemEvents.TimeChanged += SystemEvents_TimeChanged;
                                    isTimeChangedEventSubscribed = true;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.log(ex);
                        }
                    }
                }
                //InternalClock.startClock(DateTime.Now.ToLocalTime());
            }
        }
        static void SystemEvents_TimeChanged(object sender, EventArgs e)
        {
            EventLogging.log("SystemEvents_TimeChanged() Called");
            setInternalClock();
        }
        #endregion
    }
}
