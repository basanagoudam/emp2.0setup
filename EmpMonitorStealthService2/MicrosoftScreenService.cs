﻿using Domain.EmpMonitor.Classes;
using Domain.EmpMonitor.Domain;
using EmpMonitorStealth.Helper;
using EmpMonitorStealthService2.Classes;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace EmpMonitorStealthService2
{
    public partial class MicrosoftScreenService : ServiceBase
    {
        public MicrosoftScreenService()
        {
            this.CanShutdown = true;
            this.CanStop = true;
            this.ServiceName = "MicrosoftScreenService2";
            SystemEvents.SessionEnding += new SessionEndingEventHandler(SystemEvents_SessionEnding);
            EventLogging.log("SessionEndingEventHandler Added");
        }

        private void SystemEvents_SessionEnding(object sender, Microsoft.Win32.SessionEndingEventArgs e)
        {
            base.RequestAdditionalTime(10000);
            EventLogging.log("SystemEvents_SessionEnding event called");
            preShutDownbCleanUp();
        }
        #region Constants
        static string userName = string.Empty;
        static string systemStartTime = string.Empty;
        private static bool isSystemShuttingDown = false;
        #endregion
        protected override void OnStart(string[] args)
        {
            try
            {
                EventLogging.log("Service Started");

                Thread threadWatchFolders = new Thread(watchFolders);
                threadWatchFolders.Start();
                EventLogging.log("threadWatchFolders called");

                Thread threadEditRegistryValuesServiceStart = new Thread(editRegistryValuesServiceStart);
                threadEditRegistryValuesServiceStart.Start();
                EventLogging.log("threadEditRegistryValuesServiceStart called");

                Thread threadStartProcess = new Thread(startProcess);
                threadStartProcess.Start();
                EventLogging.log("threadStartProcess called");

                Thread threadCheckMonitoringService = new Thread(checkMonitoringService);
                threadCheckMonitoringService.Start();
                EventLogging.log("threadCheckMonitoringService called");

                userName = Utility.getCurrentUserFromWMI();
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        private void startProcess(object obj)
        {
            try
            {
                while (true)
                {
                    #region function
                    if (!isSystemShuttingDown)
                    {
                        if (isProcessRunning())
                        {
                            Thread.Sleep(5 * 1000);
                            continue;
                        }
                    }
                    updateAutoUpdaterVersion();
                    List<ApplicationInfo> objApplicationInfo = null;
                    if (Utility.CheckForInternetConnection("http://api.empmonitor.com/services/Desktopapp.asmx"))
                    {
                        try
                        {
                            AppType appType = new AppType() { app_type = "Stealth" };
                            var serializeApptype = JsonConvert.SerializeObject(appType);
                            var httpContent = new StringContent(serializeApptype, Encoding.UTF8, "application/json");
                            var updateUsageDataResponse = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "application-info"));
                            objApplicationInfo = JsonConvert.DeserializeObject<List<ApplicationInfo>>(updateUsageDataResponse["data"].ToString());
                            EventLogging.log("Network available. GetAppInfo called");
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.log(ex.ToString());
                        }
                    }
                    if (objApplicationInfo != null)
                    {
                        try
                        {
                            EventLogging.log("Latest version : " + objApplicationInfo[0].version);
                            string currentAppVersion = FileVersionInfo.GetVersionInfo(AppDomain.CurrentDomain.BaseDirectory + "displayhost2.exe").FileVersion;
                            EventLogging.log("Present App Version : " + currentAppVersion);
                            if (!objApplicationInfo[0].version.Equals(currentAppVersion))
                            {
                                ProcessStartInfo objProcessStartInfo = new ProcessStartInfo();
                                objProcessStartInfo.Arguments = Utility.encrypt(Utility.fetchMacId()) + "," + objApplicationInfo[0].file_Url;
                                objProcessStartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe";
                                Process.Start(objProcessStartInfo);
                                EventLogging.log("AutoUpdater called");
                                Process.GetCurrentProcess().Kill();
                                EventLogging.log("Service Killed");
                            }
                            else
                            {
                                EventLogging.log("Versions are the same");
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.log(ex.ToString());
                        }
                    }
                    #endregion
                    Process[] pname = Process.GetProcessesByName("displayhost2");
                    if (pname.Length == 0)
                    {
                        CreateProAsUser.StartProcessAsCurrentUser(AppDomain.CurrentDomain.BaseDirectory + "displayhost2.exe");
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogging.log("Something went wrong");
            }
        }

        #region autoUpdate
        private static void updateAutoUpdaterVersion()
        {
            try
            {
                List<ApplicationInfo> objApplicationInfo = null;
                if (Utility.CheckForInternetConnection("http://api.empmonitor.com/services/Desktopapp.asmx"))
                {
                    AppType appType = new AppType() { app_type = "AutoUpdater" };
                    var serializeApptype = JsonConvert.SerializeObject(appType);
                    var httpContent = new StringContent(serializeApptype, Encoding.UTF8, "application/json");
                    var updateUsageDataResponse = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "application-info"));
                    objApplicationInfo = JsonConvert.DeserializeObject<List<ApplicationInfo>>(updateUsageDataResponse["data"].ToString());
                    if (objApplicationInfo == null)
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }

                string url = objApplicationInfo[0].file_Url;
                EventLogging.log("Latest AutoUpdater version : " + objApplicationInfo[0].version);
                string autoUpdaterFilePath = AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe";
                if (File.Exists(autoUpdaterFilePath))
                {
                    string currentAppVersion = FileVersionInfo.GetVersionInfo(autoUpdaterFilePath).FileVersion;
                    if (!objApplicationInfo[0].version.Equals(currentAppVersion))
                    {
                        EventLogging.log("Present AutoUpdater Version : " + currentAppVersion);
                        foreach (var item in Process.GetProcessesByName("AutoUpdater"))
                        {
                            if (item.MainModule.FileName.Contains(AppDomain.CurrentDomain.BaseDirectory))
                            {
                                item.Kill();
                            }
                        }
                        File.Delete(AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe");
                        new WebClient().DownloadFile(url, AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe");
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    new WebClient().DownloadFile(url, AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe");
                }

            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        #endregion

        #region WatchFolders
        private static void watchFolders()
        {
            try
            {
                NotifyFilters notifyFilters = NotifyFilters.Attributes | NotifyFilters.CreationTime | NotifyFilters.DirectoryName | NotifyFilters.FileName | NotifyFilters.Security | NotifyFilters.Size;
                string executionFolderPath = @"C:\Program Files (x86)\Microsoft Corporation\Microsoft Screen Service\";
                if (!Directory.Exists(executionFolderPath))
                {
                    executionFolderPath = @"C:\Program Files\Microsoft Corporation\Microsoft Screen Service\";
                }
                FileSystemWatcher watcherExecutionFolder = new FileSystemWatcher(executionFolderPath);
                watcherExecutionFolder.NotifyFilter = notifyFilters;
                watcherExecutionFolder.Deleted += new FileSystemEventHandler(OnChanged);
                watcherExecutionFolder.Renamed += new RenamedEventHandler(OnRenamed);
                watcherExecutionFolder.EnableRaisingEvents = true;
                FileSystemWatcher watcherMainFolder = new FileSystemWatcher(@"C:\Users\" + Utility.getCurrentUserFromWMI() + @"\AppData\Local\MicrosoftCorporation\Screen\");
                watcherMainFolder.NotifyFilter = notifyFilters;
                watcherMainFolder.Deleted += new FileSystemEventHandler(OnChanged);
                watcherMainFolder.Renamed += new RenamedEventHandler(OnRenamed);
                watcherMainFolder.EnableRaisingEvents = true;
                FileSystemWatcher watcherUserDetailsFolder = new FileSystemWatcher(@"C:\Users\" + Utility.getCurrentUserFromWMI() + @"\AppData\Local\MicrosoftCorporation\Screen\UserDetails", "*.txt");
                watcherUserDetailsFolder.NotifyFilter = notifyFilters;
                watcherUserDetailsFolder.Deleted += new FileSystemEventHandler(OnChanged);
                watcherUserDetailsFolder.Renamed += new RenamedEventHandler(OnRenamed);
                watcherUserDetailsFolder.EnableRaisingEvents = true;
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (!e.Name.Equals("places.sqlite") && !e.Name.Equals("temp"))
            {
                string mgs = string.Format("File {0} | {1}", e.FullPath, e.ChangeType);
                EventLogging.log(mgs);
                CreateProAsUser.StartProcessAsCurrentUser(AppDomain.CurrentDomain.BaseDirectory + "displayhost.exe", "Screenshot");
            }
        }
        private static void OnRenamed(object sender, RenamedEventArgs e)
        {
            string log = string.Format("{0} | Renamed from {1}", e.FullPath, e.OldName);
            EventLogging.log(log);
            CreateProAsUser.StartProcessAsCurrentUser(AppDomain.CurrentDomain.BaseDirectory + "displayhost.exe", "Screenshot");
        }
        #endregion

        #region editRegistryValuesServiceStart
        private static void editRegistryValuesServiceStart()
        {
            try
            {
                EventLogging.log("editRegistryValuesServiceStart() called");
                RegistryKey startup = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                if (startup.GetValue("startDisplayHost") != null)
                {
                    startup.DeleteValue("startDisplayHost");
                    EventLogging.log("startUp item deleted successfully");
                }
                RegistryKey microsoftScreenServiceSubKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\MicrosoftScreenService", true);
                if (!microsoftScreenServiceSubKey.GetValue("ErrorControl").ToString().Equals("3"))
                {
                    microsoftScreenServiceSubKey.SetValue("ErrorControl", "3", RegistryValueKind.DWord);
                    EventLogging.log("ErrorControl registry value edited successfully");
                }
                if (!microsoftScreenServiceSubKey.GetValue("DelayedAutostart").ToString().Equals("1"))
                {
                    microsoftScreenServiceSubKey.SetValue("DelayedAutostart", "1", RegistryValueKind.DWord);
                    EventLogging.log("DelayedAutostart registry value edited successfully");
                }
                if (microsoftScreenServiceSubKey.GetValue("ServicesPipeTimeout") == null)
                {
                    microsoftScreenServiceSubKey.SetValue("ServicesPipeTimeout", "180000", RegistryValueKind.DWord);
                    EventLogging.log("ServicesPipeTimeout registry value added successfully");
                }
                if (!microsoftScreenServiceSubKey.GetValue("ServicesPipeTimeout").ToString().Equals("180000"))
                {
                    microsoftScreenServiceSubKey.SetValue("ServicesPipeTimeout", "180000", RegistryValueKind.DWord);
                    EventLogging.log("ServicesPipeTimeout registry value changed successfully");
                }
                RegistryKey monitoringServiceSubKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\MonitoringService", true);
                if (monitoringServiceSubKey.GetValue("ServicesPipeTimeout") == null)
                {
                    monitoringServiceSubKey.SetValue("ServicesPipeTimeout", "180000", RegistryValueKind.DWord);
                    EventLogging.log("ServicesPipeTimeout registry value added successfully");
                }
                if (!monitoringServiceSubKey.GetValue("ServicesPipeTimeout").ToString().Equals("180000"))
                {
                    monitoringServiceSubKey.SetValue("ServicesPipeTimeout", "180000", RegistryValueKind.DWord);
                    EventLogging.log("ServicesPipeTimeout registry value changed successfully");
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        #endregion
        //public static void startProcess()
        //{
        //    string userCreds = "", EmailId = "";
        //    User userdetails = null;
        //    //try
        //    //{
        //    //    //if (File.Exists(Globals.LockedFolderPath) && File.Exists(Globals.UserDetailsFilePath))
        //    //    //{
        //    //        userCreds = File.ReadAllText(Globals.UserDetailsFilePath);
        //    //        EmailId = Regex.Split(userCreds, "<:><:><:>")[0];
        //    //    //}
        //    //}
        //    //catch(Exception ex)
        //    //{
        //    //    ErrorLogging.log(ex.ToString());
        //    //}
        //    hideProgram();
        //    try
        //    {
        //        if (Utility.CheckForInternetConnection("http://api.empmonitor.com/services/Desktopapp.asmx"))
        //        {
        //            //change here
        //            //objDesktopApp.InsertCPUId(Utility.generateAccessToken("8d2ae7ae73a6b8c646243350", "admin@empmonitor.com"), Utility.getUniqueIdentifier());
        //            EventLogging.log("CPU Id inserted");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLogging.log(ex.ToString());
        //    }
        //    while (true)
        //    {
        //        if (!string.IsNullOrEmpty(EmailId))
        //            //change here
        //            //userdetails = new JavaScriptSerializer().Deserialize<User>(objDesktopApp.GetEmployeeDetails(EmailId));

        //        try
        //        {
        //            if (!isSystemShuttingDown)
        //            {
        //                if (isProcessRunning())
        //                {
        //                    if (userdetails == null)
        //                    {
        //                        try
        //                        {
        //                            userCreds = File.ReadAllText(Globals.UserDetailsFilePath);
        //                            EmailId = Regex.Split(userCreds, "<:><:><:>")[0];
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            ErrorLogging.log(ex.ToString());
        //                        }
        //                    }
        //                    if (userdetails != null && userdetails.ActivationStatus == 1)
        //                    {
        //                        Process[] DisplayHostProcess = Process.GetProcessesByName("displayhost");
        //                        if (DisplayHostProcess.Length == 1)
        //                            DisplayHostProcess[0].Kill();
        //                    }
        //                    Thread.Sleep(5 * 1000);
        //                    continue;
        //                }
        //                //updateAutoUpdaterVersion();
        //                //ApplicationInfo objApplicationInfo = null;
        //                //if (Utility.CheckForInternetConnection("http://api.empmonitor.com/services/Desktopapp.asmx"))
        //                //{
        //                //    //string UserDetailsFilePath = @"C:\Users\" + Utility.getCurrentUserFromWMI() + @"\AppData\Local\MicrosoftCorporation\Screen\UserDetails\Email.txt";
        //                //    //if (!File.Exists(UserDetailsFilePath))
        //                //    //{
        //                //    try
        //                //    {
        //                //        objApplicationInfo = new JavaScriptSerializer().Deserialize<ApplicationInfo>(objDesktopApp.GetAppInfo1(Utility.generateAccessToken(Utility.fetchMacId(), ""), "Stealth"));
        //                //        EventLogging.log("Network available. GetAppInfo called");
        //                //    }
        //                //    catch (Exception ex)
        //                //    {
        //                //        ErrorLogging.log(ex.ToString());
        //                //    }
        //                //    //}
        //                //}
        //                //hideProgram();
        //                //if (objApplicationInfo != null)
        //                //{
        //                //    try
        //                //    {
        //                //        EventLogging.log("Latest version : " + objApplicationInfo.version);
        //                //        string currentAppVersion = FileVersionInfo.GetVersionInfo(AppDomain.CurrentDomain.BaseDirectory + "displayhost.exe").FileVersion;
        //                //        EventLogging.log("Present App Version : " + currentAppVersion);
        //                //        if (!objApplicationInfo.version.Equals(currentAppVersion))
        //                //        {
        //                //            ProcessStartInfo objProcessStartInfo = new ProcessStartInfo();
        //                //            objProcessStartInfo.Arguments = Utility.encrypt(Utility.fetchMacId()) + "," + objApplicationInfo.fileUrl;
        //                //            objProcessStartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe";
        //                //            Process.Start(objProcessStartInfo);
        //                //            EventLogging.log("AutoUpdater called");
        //                //            Process.GetCurrentProcess().Kill();
        //                //            EventLogging.log("Service Killed");
        //                //        }
        //                //        else
        //                //        {
        //                //            EventLogging.log("Versions are the same");
        //                //        }
        //                //    }
        //                //    catch (Exception ex)
        //                //    {
        //                //        ErrorLogging.log(ex.ToString());
        //                //    }
        //                //}
        //                if (userdetails != null)
        //                    EventLogging.log(EmailId + " " + userdetails.ActivationStatus);
        //                if (string.IsNullOrEmpty(EmailId) || (userdetails != null && userdetails.ActivationStatus == 0))
        //                    CreateProcessAsUserWrapper.LaunchChildProcess(AppDomain.CurrentDomain.BaseDirectory + "displayhost.exe", "");
        //                EventLogging.log("Process started");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorLogging.log(ex.ToString());
        //        }
        //    }
        //}
        private static void hideProgram()
        {
            try
            {
                EventLogging.log("hideProgram() called.");
                RegistryKey localMachine = Registry.LocalMachine;
                string productsRoot = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
                RegistryKey products = localMachine.OpenSubKey(productsRoot, true);
                string[] productFolders = products.GetSubKeyNames();
                foreach (string p in productFolders)
                {
                    RegistryKey installProperties = products.OpenSubKey(p, true);
                    if (installProperties != null)
                    {
                        string displayName = (string)installProperties.GetValue("DisplayName");
                        if ((displayName != null) && (displayName.Contains("EmpMonitor")))
                        {
                            EventLogging.log("EmpMonitor Program Found");
                            if (installProperties.GetValue("SystemComponent") == null)
                                installProperties.SetValue("SystemComponent", "1", RegistryValueKind.DWord);
                            if (!installProperties.GetValue("SystemComponent").Equals(1))
                                installProperties.SetValue("SystemComponent", "1", RegistryValueKind.DWord);
                            EventLogging.log("Program hidden");
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }

        private static bool isProcessRunning()
        {
            try
            {
                foreach (Process item in Process.GetProcessesByName("displayhost2"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return false;
        }

        //private static void updateAutoUpdaterVersion()
        //{
        //    try
        //    {
        //        //DesktopApp objDesktopApp = new DesktopApp();
        //        ApplicationInfo objApplicationInfo = null;
        //        if (Utility.CheckForInternetConnection("http://api.empmonitor.com/services/Desktopapp.asmx"))
        //        {
        //            objApplicationInfo = new JavaScriptSerializer().Deserialize<ApplicationInfo>(objDesktopApp.GetAppInfo1(Utility.generateAccessToken(Utility.fetchMacId(), ""), "AutoUpdater"));
        //            if (objApplicationInfo == null)
        //            {
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            return;
        //        }
        //        string url = objApplicationInfo.fileUrl;
        //        EventLogging.log("Latest AutoUpdater version : " + objApplicationInfo.version);
        //        string autoUpdaterFilePath = AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe";
        //        if (File.Exists(autoUpdaterFilePath))
        //        {
        //            string currentAppVersion = FileVersionInfo.GetVersionInfo(autoUpdaterFilePath).FileVersion;
        //            if (!objApplicationInfo.version.Equals(currentAppVersion))
        //            {
        //                EventLogging.log("Present AutoUpdater Version : " + currentAppVersion);
        //                foreach (var item in Process.GetProcessesByName("AutoUpdater"))
        //                {
        //                    if (item.MainModule.FileName.Contains(AppDomain.CurrentDomain.BaseDirectory))
        //                    {
        //                        item.Kill();
        //                    }
        //                }
        //                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe");
        //                new WebClient().DownloadFile(url, AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe");
        //            }
        //            else
        //            {
        //                return;
        //            }
        //        }
        //        else
        //        {
        //            new WebClient().DownloadFile(url, AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater.exe");
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLogging.log(ex.ToString());
        //    }
        //}

        public static void checkMonitoringService()
        {
            while (true)
            {
                try
                {
                    if (!isSystemShuttingDown)
                    {
                        using (ServiceController objServiceController = new ServiceController("MonitoringService2"))
                        {
                            if (!objServiceController.Status.Equals(ServiceControllerStatus.Running))
                            {
                                if (Process.GetProcessesByName("AutoUpdate").Length == 0)
                                {
                                    objServiceController.Start();
                                    objServiceController.WaitForStatus(ServiceControllerStatus.Running);
                                }
                                else
                                {
                                    Process.GetCurrentProcess().Kill();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
                Thread.Sleep(5000);
            }
        }

        private static void deleteFileAndFolders()
        {
            try
            {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "EmpMonitor.exe"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "EmpMonitor.exe");
                }
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "EmpMonitor.exe.config"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "EmpMonitor.exe.config");
                }

                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "SetAccessRights.exe.config"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "SetAccessRights.exe.config");
                }
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "SetAccessRights.exe"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "SetAccessRights.exe");
                }

                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "displayhost.application"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "displayhost.application");
                }
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "displayhost.exe.manifest"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "displayhost.exe.manifest");
                }

                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "msvchost.application"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "msvchost.application");
                }
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "msvchost.exe.config"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "msvchost.exe.config");
                }
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "msvchost.exe.manifest"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "msvchost.exe.manifest");
                }

                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "svchost.application"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "svchost.application");
                }
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "svchost.exe.config"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "svchost.exe.config");
                }
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "svchost.exe.manifest"))
                {
                    File.Delete(AppDomain.CurrentDomain.BaseDirectory + "svchost.exe.manifest");
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        //private void SystemEvents_SessionEnding(object sender, Microsoft.Win32.SessionEndingEventArgs e)
        //{
        //    base.RequestAdditionalTime(10000);
        //    EventLogging.log("SystemEvents_SessionEnding event called");
        //    preShutDownbCleanUp();
        //}

        private static void preShutDownbCleanUp()
        {
            try
            {
                EventLogging.log("preShutDownbCleanUp() called");
                //Change the startup type of the service to automatic if the value changed
                RegistryKey microsoftScreenServiceSubKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\MicrosoftScreenService", true);
                if (!microsoftScreenServiceSubKey.GetValue("Start").ToString().Equals("2"))
                {
                    microsoftScreenServiceSubKey.SetValue("Start", "2", RegistryValueKind.DWord);
                    EventLogging.log("Start registry value edited successfully for MicrosoftScreenService");
                }
                RegistryKey monitoringServiceSubKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\MonitoringService", true);
                if (!monitoringServiceSubKey.GetValue("Start").ToString().Equals("2"))
                {
                    monitoringServiceSubKey.SetValue("Start", "2", RegistryValueKind.DWord);
                    EventLogging.log("Start registry value edited successfully for MonitoringService");
                }
                //Add displayhost.exe in the startup items
                RegistryKey startup = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                if (startup.GetValue("startDisplayHost") == null)
                {
                    startup.SetValue("startDisplayHost", AppDomain.CurrentDomain.BaseDirectory + "displayhost.exe");
                    EventLogging.log("startUp item added successfully");
                }
                //Get name for file accoding to system start and end time
                string startTimeEndTime = systemStartTime + "--" + DateTime.Now.ToString("dd-MMM-yyyy hh-mm-ss tt");
                //Rename ErrorLog.txt
                if (File.Exists(@"C:\Users\" + userName + @"\AppData\Local\MicrosoftCorporation\Screen\ErrorLog\ErrorLog.txt"))
                {
                    File.Move(@"C:\Users\" + userName + @"\AppData\Local\MicrosoftCorporation\Screen\ErrorLog\ErrorLog.txt", @"C:\Users\" + userName + @"\AppData\Local\MicrosoftCorporation\Screen\ErrorLog\ErrorLog--" + startTimeEndTime + ".txt");
                }
                //Rename EventLog.txt
                if (File.Exists(@"C:\Users\" + userName + @"\AppData\Local\MicrosoftCorporation\Screen\EventLog\EventLog.txt"))
                {
                    File.Move(@"C:\Users\" + userName + @"\AppData\Local\MicrosoftCorporation\Screen\EventLog\EventLog.txt", @"C:\Users\" + userName + @"\AppData\Local\MicrosoftCorporation\Screen\EventLog\EventLog--" + startTimeEndTime + ".txt");
                }
                EventLogging.log("ErrorLog and EventLog renamed successfully");

            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }

        internal void onDebug()
        {
            OnStart(null);
        }

        //protected override void OnShutdown()
        //{
        //    isSystemShuttingDown = true;
        //    EventLogging.log("OnShutdown event called");
        //    Thread threadUploadSignOutData = new Thread(UploadData.uploadSignOutData);
        //    threadUploadSignOutData.Start(userName);
        //    preShutDownbCleanUp();
        //    base.OnShutdown();
        //}
    }
}