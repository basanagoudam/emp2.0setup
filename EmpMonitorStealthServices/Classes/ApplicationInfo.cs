﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmpMonitorStealthServices.Classes
{

    public class ApplicationInfo
    {
        public virtual int appId { get; set; }
        public virtual string app_type { get; set; }
        public virtual string version { get; set; }
        public virtual string file_Url { get; set; }
    }
    public enum ApplicationType
    {
        None,
        Stealth,
        NonStealth,
        AutoUpdater
    };

}
