﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace EmpMonitorStealthServices.Classes
{
    public class Utility
    {
        public static string getUniqueIdentifier()
        {
            return GetHash("CPU >> " + cpuId() + "\nBIOS >> " + biosId() + "\nBASE >> " + baseId() + "\nDISK >> " + diskId());
        }
        private static string GetHash(string s)
        {
            MD5 sec = new MD5CryptoServiceProvider();
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] bt = enc.GetBytes(s);
            return GetHexString(sec.ComputeHash(bt));
        }
        private static string GetHexString(byte[] bt)
        {
            string s = string.Empty;
            for (int i = 0; i < bt.Length; i++)
            {
                byte b = bt[i];
                int n, n1, n2;
                n = (int)b;
                n1 = n & 15;
                n2 = (n >> 4) & 15;
                if (n2 > 9)
                    s += ((char)(n2 - 10 + (int)'A')).ToString();
                else
                    s += n2.ToString();
                if (n1 > 9)
                    s += ((char)(n1 - 10 + (int)'A')).ToString();
                else
                    s += n1.ToString();
                if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
            }
            return s;
        }
        #region Original Device ID Getting Code
        //Return a hardware identifier
        private static string identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                if (mo[wmiMustBeTrue].ToString() == "True")
                {
                    //Only get the first one
                    if (result == "")
                    {
                        try
                        {
                            result = mo[wmiProperty].ToString();
                            break;
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return result;
        }
        //Return a hardware identifier
        private static string identifier(string wmiClass, string wmiProperty)
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }
        private static string cpuId()
        {
            //Uses first CPU identifier available in order of preference
            //Don't get all identifiers, as it is very time consuming
            string retVal = identifier("Win32_Processor", "UniqueId");
            if (retVal == "") //If no UniqueID, use ProcessorID
            {
                retVal = identifier("Win32_Processor", "ProcessorId");
                if (retVal == "") //If no ProcessorId, use Name
                {
                    retVal = identifier("Win32_Processor", "Name");
                    if (retVal == "") //If no Name, use Manufacturer
                    {
                        retVal = identifier("Win32_Processor", "Manufacturer");
                    }
                    //Add clock speed for extra security
                    retVal += identifier("Win32_Processor", "MaxClockSpeed");
                }
            }
            return retVal;
        }
        //BIOS Identifier
        private static string biosId()
        {
            return identifier("Win32_BIOS", "Manufacturer")
            + identifier("Win32_BIOS", "SMBIOSBIOSVersion")
            + identifier("Win32_BIOS", "IdentificationCode")
            + identifier("Win32_BIOS", "SerialNumber")
            + identifier("Win32_BIOS", "ReleaseDate")
            + identifier("Win32_BIOS", "Version");
        }
        //Main physical hard drive ID
        private static string diskId()
        {
            return identifier("Win32_DiskDrive", "Model")
            + identifier("Win32_DiskDrive", "Manufacturer")
            + identifier("Win32_DiskDrive", "Signature")
            + identifier("Win32_DiskDrive", "TotalHeads");
        }
        //Motherboard ID
        private static string baseId()
        {
            return identifier("Win32_BaseBoard", "Model")
            + identifier("Win32_BaseBoard", "Manufacturer")
            + identifier("Win32_BaseBoard", "Name")
            + identifier("Win32_BaseBoard", "SerialNumber");
        }
        //Primary video controller ID
        private static string videoId()
        {
            return identifier("Win32_VideoController", "DriverVersion")
            + identifier("Win32_VideoController", "Name");
        }
        //First enabled network card ID
        private static string macId()
        {
            return identifier("Win32_NetworkAdapterConfiguration",
                "MACAddress", "IPEnabled");
        }

        #endregion
        public static bool SplitFile(string SourceFile, int nNoofFiles, int sizeOfFile)
        {
            // bool Split = false;
            try
            {
                string splitFolderPath = Path.Combine(Path.GetDirectoryName(SourceFile) + "\\Split");
                if (!Directory.Exists(splitFolderPath))
                {
                    Directory.CreateDirectory(splitFolderPath);
                }
                else
                {
                    Directory.Delete(splitFolderPath, true);
                    Directory.CreateDirectory(splitFolderPath);
                }
                FileStream fs = new FileStream(SourceFile, FileMode.Open, FileAccess.Read);
                int SizeofEachFile = sizeOfFile;//(int)Math.Ceiling((double)fs.Length / nNoofFiles);
                for (int i = 0; i < nNoofFiles; i++)
                {
                    string baseFileName = Path.GetFileNameWithoutExtension(SourceFile);
                    string Extension = Path.GetExtension(SourceFile);

                    FileStream outputFile = new FileStream(splitFolderPath + "\\" + baseFileName + "." +
                        i.ToString().PadLeft(5, Convert.ToChar("0")) + Extension, FileMode.Create, FileAccess.Write);
                    //mergeFolder = Path.GetDirectoryName(SourceFile);
                    int bytesRead = 0;
                    byte[] buffer = new byte[SizeofEachFile];
                    if ((bytesRead = fs.Read(buffer, 0, SizeofEachFile)) > 0)
                    {
                        outputFile.Write(buffer, 0, bytesRead);
                        //outp.Write(buffer, 0, BytesRead);
                        string packet = baseFileName + "." + i.ToString().PadLeft(3, Convert.ToChar("0")) + Extension.ToString();
                        //Packets.Add(packet);
                    }
                    outputFile.Close();
                }
                fs.Close();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static string removeInvalidCharacters(string path)
        {
            try
            {
                string invalidChars = new string(Path.GetInvalidPathChars());
                foreach (char item in invalidChars)
                {
                    path = path.Replace(item.ToString(), "");
                }
                return path;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// Gets a list of DateTime instances which come in the week of the speficied Date
        /// </summary>
        /// <param name="Date">Date for which the days in a week needs to returned</param>
        /// <returns>List of DateTime instances in a Week</returns>
        public static List<DateTime> getDaysInAWeek(string Date)
        {
            DateTime dt = DateTime.Parse(Date);
            DayOfWeek dayOfWeek = dt.DayOfWeek;
            List<DateTime> daysOfWeelk = new List<DateTime>();
            daysOfWeelk.Add(dt);
            if (dayOfWeek.Equals(DayOfWeek.Monday))
            {
                daysOfWeelk.Add(dt.AddDays(1));
                daysOfWeelk.Add(dt.AddDays(2));
                daysOfWeelk.Add(dt.AddDays(3));
                daysOfWeelk.Add(dt.AddDays(4));
                daysOfWeelk.Add(dt.AddDays(5));
                daysOfWeelk.Add(dt.AddDays(6));
            }
            else if (dayOfWeek.Equals(DayOfWeek.Tuesday))
            {
                daysOfWeelk.Add(dt.Add(new TimeSpan(-1, 0, 0, 0)));
                daysOfWeelk.Add(dt.AddDays(1));
                daysOfWeelk.Add(dt.AddDays(2));
                daysOfWeelk.Add(dt.AddDays(3));
                daysOfWeelk.Add(dt.AddDays(4));
                daysOfWeelk.Add(dt.AddDays(5));
            }
            else if (dayOfWeek.Equals(DayOfWeek.Wednesday))
            {
                daysOfWeelk.Add(dt.Add(new TimeSpan(-2, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-1, 0, 0, 0)));
                daysOfWeelk.Add(dt.AddDays(1));
                daysOfWeelk.Add(dt.AddDays(2));
                daysOfWeelk.Add(dt.AddDays(3));
                daysOfWeelk.Add(dt.AddDays(4));
            }
            else if (dayOfWeek.Equals(DayOfWeek.Thursday))
            {
                daysOfWeelk.Add(dt.Add(new TimeSpan(-3, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-2, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-1, 0, 0, 0)));
                daysOfWeelk.Add(dt.AddDays(1));
                daysOfWeelk.Add(dt.AddDays(2));
                daysOfWeelk.Add(dt.AddDays(3));
            }
            else if (dayOfWeek.Equals(DayOfWeek.Friday))
            {
                daysOfWeelk.Add(dt.Add(new TimeSpan(-4, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-3, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-2, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-1, 0, 0, 0)));
                daysOfWeelk.Add(dt.AddDays(1));
                daysOfWeelk.Add(dt.AddDays(2));
            }
            else if (dayOfWeek.Equals(DayOfWeek.Saturday))
            {
                daysOfWeelk.Add(dt.Add(new TimeSpan(-5, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-4, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-3, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-2, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-1, 0, 0, 0)));
                daysOfWeelk.Add(dt.AddDays(1));
            }
            else if (dayOfWeek.Equals(DayOfWeek.Sunday))
            {
                daysOfWeelk.Add(dt.Add(new TimeSpan(-6, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-5, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-4, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-3, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-2, 0, 0, 0)));
                daysOfWeelk.Add(dt.Add(new TimeSpan(-1, 0, 0, 0)));
            }
            return daysOfWeelk;
        }
        /// <summary>
        /// Gets a list of DateTime instances which come in the week of the speficied Date
        /// </summary>
        /// <param name="Date">Date for which the days in a week needs to returned</param>
        /// <returns>List of DateTime instances in a Month</returns>
        public static List<DateTime> getDaysInMonth()
        {
            List<DateTime> monthsDate = new List<DateTime>();
            try
            {
                int month = DateTime.Today.Month;
                int year = DateTime.Today.Year;
                int daysInMonthMinusFridayAndSaturday = 0;
                for (int i = 1; i <= DateTime.DaysInMonth(year, month); i++)
                {
                    DateTime thisDay = new DateTime(year, month, i);
                    if (thisDay.DayOfWeek != DayOfWeek.Sunday)
                    {
                        monthsDate.Add(thisDay);
                        daysInMonthMinusFridayAndSaturday += 1;
                    }
                }

            }
            catch (Exception)
            {

            }
            return monthsDate;
        }

        /// <summary>
        /// Gets the processor ID using WMI query
        /// </summary>
        /// <returns>Processor ID</returns>
        public static string getProcessorId()
        {
            string sQuery = "SELECT * FROM Win32_Processor";
            ManagementObjectSearcher oManagementObjectSearcher = new ManagementObjectSearcher(sQuery);
            ManagementObjectCollection oCollection = oManagementObjectSearcher.Get();
            foreach (ManagementObject oManagementObject in oCollection)
            {
                return (string)oManagementObject["UniqueId"];
            }
            return string.Empty;
        }
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (new WebClient().OpenRead("http://api.empmonitor.com/services/Desktopapp.asmx"))
                {
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }
        public static bool CheckForInternetConnection(string url)
        {
            try
            {
                using (new WebClient().OpenRead(url))
                {
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        }
        public static byte[] readFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        public static bool isNumber(string number)
        {
            return Regex.IsMatch(number, @"^[0-9]*$");
        }
        public static bool isEmail(string email)
        {
            return Regex.IsMatch(email, @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", RegexOptions.IgnoreCase);
        }
        public static string generateRandomPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(generateRandomString(4, true));
            builder.Append(generateRandomNumber(1000, 9999));
            builder.Append(generateRandomString(2, false));
            return builder.ToString();
        }
        public static int generateRandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
        public static string generateRandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        public static string generateAccessTokenForUser()
        {
            try
            {
                return DateTime.Now.Ticks.ToString("x") + new Random().Next(100000000, 999999999);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gets the Mime type for the sepecified extension (As per media Fire API)
        /// </summary>
        /// <param name="extension">extension to be checked</param>
        /// <returns>Mime Type of a file</returns>
        public static string getFileType(string extension)
        {
            if (string.IsNullOrEmpty(extension))
            {
                return string.Empty;
            }
            if (!extension.Contains("."))
            {
                extension = extension.Remove(0);
            }
            if (extension.Equals("json"))
            {
                return "application/json";
            }
            else if (extension.Equals("doc"))
            {
                return "application/msword";
            }
            else if (extension.Equals("pdf"))
            {
                return "application/pdf";
            }
            else if (extension.Equals("rtf"))
            {
                return "application/rtf";
            }
            else if (extension.Equals("sdp"))
            {
                return "application/sdp";
            }
            else if (extension.Equals("cab"))
            {
                return "application/vnd.ms-cab-compressed";
            }
            else if (extension.Equals("xls"))
            {
                return "application/vnd.ms-excel";
            }
            else if (extension.Equals("ppt"))
            {
                return "application/vnd.ms-powerpoint";
            }
            else if (extension.Equals("odg"))
            {
                return "application/vnd.oasis.opendocument.graphics";
            }
            else if (extension.Equals("otg"))
            {
                return "application/vnd.oasis.opendocument.graphics-template";
            }
            else if (extension.Equals("odp"))
            {
                return "application/vnd.oasis.opendocument.presentation";
            }
            else if (extension.Equals("otp"))
            {
                return "application/vnd.oasis.opendocument.presentation-template";
            }
            else if (extension.Equals("ods"))
            {
                return "application/vnd.oasis.opendocument.spreadsheet";
            }
            else if (extension.Equals("ots"))
            {
                return "application/vnd.oasis.opendocument.spreadsheet-template";
            }
            else if (extension.Equals("odt"))
            {
                return "application/vnd.oasis.opendocument.text";
            }
            else if (extension.Equals("odm"))
            {
                return "application/vnd.oasis.opendocument.text-master";
            }
            else if (extension.Equals("ott"))
            {
                return "application/vnd.oasis.opendocument.text-template";
            }
            else if (extension.Equals("oth"))
            {
                return "application/vnd.oasis.opendocument.text-web";
            }
            else if (extension.Equals("wpd"))
            {
                return "application/vnd.wordperfect";
            }
            else if (extension.Equals("zip"))
            {
                return "application/zip";
            }
            else if (extension.Equals("ac3"))
            {
                return "audio/ac3";
            }
            else if (extension.Equals("gif"))
            {
                return "image/gif";
            }
            else if (extension.Equals("png"))
            {
                return "image/png";
            }
            else if (extension.Equals("tfx"))
            {
                return "image/tiff-fx";
            }
            else if (extension.Equals("psd"))
            {
                return "image/vnd.adobe.photoshop";
            }
            else if (extension.Equals("css"))
            {
                return "text/css";
            }
            else if (extension.Equals("csv"))
            {
                return "text/csv";
            }
            else if (extension.Equals("js"))
            {
                return "text/javascript";
            }
            else if (extension.Equals("tsv"))
            {
                return "text/tab-separated-values";
            }
            else if (extension.Equals("jtd"))
            {
                return "text/vnd.esmertec.theme-descriptor";
            }
            else if (extension.Equals("ogv"))
            {
                return "video/ogg";
            }
            else if (extension.Equals("s14"))
            {
                return "video/vnd.sealed.mpeg4";
            }
            else if (extension.Equals("xlsb"))
            {
                return "application/vnd.ms-excel.sheet.binary.macroEnabled.12";
            }
            else if (extension.Equals("xlsm"))
            {
                return "application/vnd.ms-excel.sheet.macroEnabled.12";
            }
            else if (extension.Equals("xltm"))
            {
                return "application/vnd.ms-excel.template.macroEnabled.12";
            }
            else if (extension.Equals("pptm"))
            {
                return "application/vnd.ms-powerpoint.presentation.macroEnabled.12";
            }
            else if (extension.Equals("ppsm"))
            {
                return "application/vnd.ms-powerpoint.slideshow.macroEnabled.12";
            }
            else if (extension.Equals("potm"))
            {
                return "application/vnd.ms-powerpoint.template.macroEnabled.12";
            }
            else if (extension.Equals("docm"))
            {
                return "application/vnd.ms-word.document.macroEnabled.12";
            }
            else if (extension.Equals("dotm"))
            {
                return "application/vnd.ms-word.template.macroEnabled.12";
            }
            else if (extension.Equals("ppsx"))
            {
                return "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
            }
            else if (extension.Equals("potx"))
            {
                return "application/vnd.openxmlformats-officedocument.presentationml.template";
            }
            else if (extension.Equals("xlsx"))
            {
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            }
            else if (extension.Equals("xltx"))
            {
                return "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
            }
            else if (extension.Equals("docx"))
            {
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            }
            else if (extension.Equals("dotx"))
            {
                return "application/vnd.openxmlformats-officedocument.wordprocessingml.template";
            }
            else if (extension.Equals("sxc"))
            {
                return "application/vnd.sun.xml.calc";
            }
            else if (extension.Equals("stc"))
            {
                return "application/vnd.sun.xml.calc.template";
            }
            else if (extension.Equals("std"))
            {
                return "application/vnd.sun.xml.draw.template";
            }
            else if (extension.Equals("sxi"))
            {
                return "application/vnd.sun.xml.impress";
            }
            else if (extension.Equals("sti"))
            {
                return "application/vnd.sun.xml.impress.template";
            }
            else if (extension.Equals("sxw"))
            {
                return "application/vnd.sun.xml.writer";
            }
            else if (extension.Equals("sxg"))
            {
                return "application/vnd.sun.xml.writer.global";
            }
            else if (extension.Equals("bz2"))
            {
                return "application/x-bzip2";
            }
            else if (extension.Equals("jar"))
            {
                return "application/zip";
            }
            else if (extension.Equals("pl"))
            {
                return "application/x-perl";
            }
            else if (extension.Equals("sh"))
            {
                return "application/x-sh";
            }
            else if (extension.Equals("swf"))
            {
                return "application/x-shockwave-flash";
            }
            else if (extension.Equals("tar"))
            {
                return "application/x-tar";
            }
            else if (extension.Equals("tcl"))
            {
                return "application/x-tcl";
            }
            else if (extension.Equals("flac"))
            {
                return "audio/x-flac";
            }
            else if (extension.Equals("wma"))
            {
                return "audio/x-ms-wma";
            }
            else if (extension.Equals("wav"))
            {
                return "audio/x-wav";
            }
            else if (extension.Equals("bmp"))
            {
                return "image/bmp";
            }
            else if (extension.Equals("tga"))
            {
                return "image/x-targa";
            }
            else if (extension.Equals("webm"))
            {
                return "video/webm";
            }
            else if (extension.Equals("flv"))
            {
                return "video/x-flv";
            }
            else if (extension.Equals("asx"))
            {
                return "video/x-ms-asf";
            }
            else if (extension.Equals("wmv"))
            {
                return "video/x-ms-wmv";
            }
            else if (extension.Equals("avi"))
            {
                return "video/x-msvideo";
            }
            else if (extension.Equals("exe"))
            {
                return "application/x-dosexec";
            }
            else if (extension.Equals("bin"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("dll"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("img"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("iso"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("123"))
            {
                return "application/vnd.lotus-1-2-3";
            }
            else if (extension.Equals("wk1"))
            {
                return "application/vnd.lotus-1-2-3";
            }
            else if (extension.Equals("wks"))
            {
                return "application/vnd.ms-works";
            }
            else if (extension.Equals("wps"))
            {
                return "application/vnd.ms-works";
            }
            else if (extension.Equals("pdb"))
            {
                return "application/vnd.palm";
            }
            else if (extension.Equals("xhtml"))
            {
                return "application/xhtml+xml";
            }
            else if (extension.Equals("xhtm"))
            {
                return "application/xhtml+xml";
            }
            else if (extension.Equals("mp3"))
            {
                return "audio/mpeg";
            }
            else if (extension.Equals("mpga"))
            {
                return "audio/mpeg";
            }
            else if (extension.Equals("mp1"))
            {
                return "audio/mpeg";
            }
            else if (extension.Equals("mp2"))
            {
                return "audio/mpeg";
            }
            else if (extension.Equals("ogg"))
            {
                return "audio/ogg";
            }
            else if (extension.Equals("smp3"))
            {
                return "audio/vnd.sealedmedia.softseal.mpeg";
            }
            else if (extension.Equals("smp"))
            {
                return "audio/vnd.sealedmedia.softseal.mpeg";
            }
            else if (extension.Equals("s1m"))
            {
                return "audio/vnd.sealedmedia.softseal.mpeg";
            }
            else if (extension.Equals("jpg"))
            {
                return "image/jpeg";
            }
            else if (extension.Equals("jpeg"))
            {
                return "image/jpeg";
            }
            else if (extension.Equals("jpe"))
            {
                return "image/jpeg";
            }
            else if (extension.Equals("jfif"))
            {
                return "image/jpeg";
            }
            else if (extension.Equals("tiff"))
            {
                return "image/tiff";
            }
            else if (extension.Equals("tif"))
            {
                return "image/tiff";
            }
            else if (extension.Equals("html"))
            {
                return "text/html";
            }
            else if (extension.Equals("htm"))
            {
                return "text/html";
            }
            else if (extension.Equals("txt"))
            {
                return "text/plain";
            }
            else if (extension.Equals("asc"))
            {
                return "text/plain";
            }
            else if (extension.Equals("text"))
            {
                return "text/plain";
            }
            else if (extension.Equals("h"))
            {
                return "text/plain";
            }
            else if (extension.Equals("cc"))
            {
                return "text/plain";
            }
            else if (extension.Equals("cxx"))
            {
                return "text/plain";
            }
            else if (extension.Equals("dot"))
            {
                return "application/msword";
            }
            else if (extension.Equals("xml"))
            {
                return "text/xml";
            }
            else if (extension.Equals("3gp"))
            {
                return "video/3gpp";
            }
            else if (extension.Equals("3gpp"))
            {
                return "video/3gpp";
            }
            else if (extension.Equals("3g2"))
            {
                return "video/3gpp2";
            }
            else if (extension.Equals("3gpp2"))
            {
                return "video/3gpp2";
            }
            else if (extension.Equals("mp4"))
            {
                return "video/mp4";
            }
            else if (extension.Equals("mpg4"))
            {
                return "video/mp4";
            }
            else if (extension.Equals("mpeg"))
            {
                return "video/mpeg";
            }
            else if (extension.Equals("mpg"))
            {
                return "video/mpeg";
            }
            else if (extension.Equals("mpe"))
            {
                return "video/mpeg";
            }
            else if (extension.Equals("mov"))
            {
                return "video/quicktime";
            }
            else if (extension.Equals("qt"))
            {
                return "video/quicktime";
            }
            else if (extension.Equals("uvu"))
            {
                return "video/vnd.dece.mp4";
            }
            else if (extension.Equals("uvvu"))
            {
                return "video/vnd.dece.mp4";
            }
            else if (extension.Equals("mxu"))
            {
                return "video/vnd.mpegurl";
            }
            else if (extension.Equals("smpg"))
            {
                return "video/vnd.sealed.mpeg1";
            }
            else if (extension.Equals("s11"))
            {
                return "video/vnd.sealed.mpeg1";
            }
            else if (extension.Equals("gz"))
            {
                return "application/x-gzip";
            }
            else if (extension.Equals("tgz"))
            {
                return "application/x-gzip";
            }
            else if (extension.Equals("1"))
            {
                return "application/x-troff-man";
            }
            else if (extension.Equals("mid"))
            {
                return "audio/midi";
            }
            else if (extension.Equals("midi"))
            {
                return "audio/midi";
            }
            else if (extension.Equals("aif"))
            {
                return "audio/x-aiff";
            }
            else if (extension.Equals("aiff"))
            {
                return "audio/x-aiff";
            }
            else if (extension.Equals("aifc"))
            {
                return "audio/x-aiff";
            }
            else if (extension.Equals("rm"))
            {
                return "audio/x-pn-realaudio";
            }
            else if (extension.Equals("zzz"))
            {
                return "zzz";
            }
            else if (extension.Equals("rar"))
            {
                return "application/rar";
            }
            else if (extension.Equals("pptx"))
            {
                return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            }
            else if (extension.Equals("pps"))
            {
                return "application/vnd.ms-powerpoint";
            }
            else if (extension.Equals("apk"))
            {
                return "application/vnd.android.package-archive";
            }
            else if (extension.Equals("dmg"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("rtf"))
            {
                return "text/plain";
            }
            else if (extension.Equals("cpp"))
            {
                return "text/x-c";
            }
            else if (extension.Equals("rtf"))
            {
                return "text/rtf";
            }
            else if (extension.Equals("deb"))
            {
                return "application/x-debian-package";
            }
            else if (extension.Equals("rpm"))
            {
                return "application/x-rpm";
            }
            else if (extension.Equals("gz"))
            {
                return "application/x-gzip";
            }
            else if (extension.Equals("iso"))
            {
                return "application/x-iso9660-image";
            }
            else if (extension.Equals("rar"))
            {
                return "application/x-rar";
            }
            else if (extension.Equals("mkv"))
            {
                return "video/x-matroska";
            }
            else if (extension.Equals("doc"))
            {
                return "application/vnd.ms-office";
            }
            else if (extension.Equals("mod"))
            {
                return "video/mpeg";
            }
            else if (extension.Equals("php"))
            {
                return "text/x-php";
            }
            else if (extension.Equals("dgl"))
            {
                return "application/dgl";
            }
            else if (extension.Equals("c"))
            {
                return "text/x-c";
            }
            else if (extension.Equals("xlt"))
            {
                return "application/vnd.ms-excel";
            }
            else if (extension.Equals("php3"))
            {
                return "text/x-php";
            }
            else if (extension.Equals("php4"))
            {
                return "text/x-php";
            }
            else if (extension.Equals("php5"))
            {
                return "text/x-php";
            }
            else if (extension.Equals("phps"))
            {
                return "text/x-php";
            }
            else if (extension.Equals("rmvb"))
            {
                return "application/vnd.rn-realmedia";
            }
            else if (extension.Equals("h264"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("7z"))
            {
                return "application/x-7z-compressed";
            }
            else if (extension.Equals("com"))
            {
                return "application/x-dosexec";
            }
            else if (extension.Equals("ini"))
            {
                return "text/plain";
            }
            else if (extension.Equals("rmv"))
            {
                return "application/vnd.rn-realmedia";
            }
            else if (extension.Equals("ipa"))
            {
                return "application/zip";
            }
            else if (extension.Equals("bat"))
            {
                return "text/plain";
            }
            else if (extension.Equals("bsh"))
            {
                return "application/x-sh";
            }
            else if (extension.Equals("cob"))
            {
                return "text/x-cobol";
            }
            else if (extension.Equals("hpp"))
            {
                return "text/plain";
            }
            else if (extension.Equals("java"))
            {
                return "text/plain";
            }
            else if (extension.Equals("jsp"))
            {
                return "text/plain";
            }
            else if (extension.Equals("mak"))
            {
                return "text/plain";
            }
            else if (extension.Equals("phtml"))
            {
                return "application/x-web-iPerl";
            }
            else if (extension.Equals("phtml"))
            {
                return "application/x-httpd-eperl";
            }
            else if (extension.Equals("phtml"))
            {
                return "text/html";
            }
            else if (extension.Equals("phtml"))
            {
                return "application/x-httpd-php";
            }
            else if (extension.Equals("py"))
            {
                return "application/x-python";
            }
            else if (extension.Equals("shtml"))
            {
                return "wwwserver/html-ssi";
            }
            else if (extension.Equals("shtml"))
            {
                return "text/html";
            }
            else if (extension.Equals("sql"))
            {
                return "text/plain";
            }
            else if (extension.Equals("vb"))
            {
                return "text/plain";
            }
            else if (extension.Equals("vbs"))
            {
                return "text/vbs";
            }
            else if (extension.Equals("vbs"))
            {
                return "text/vbscript";
            }
            else if (extension.Equals("vbs"))
            {
                return "application/x-vbs";
            }
            else if (extension.Equals("conf"))
            {
                return "text/plain";
            }
            else if (extension.Equals("inf"))
            {
                return "text/plain";
            }
            else if (extension.Equals("m4v"))
            {
                return "video/mp4";
            }
            else if (extension.Equals("msi"))
            {
                return "application/x-msi";
            }
            else if (extension.Equals("csv"))
            {
                return "text/plain";
            }
            else if (extension.Equals("vob"))
            {
                return "video/mpeg";
            }
            else if (extension.Equals("m4v"))
            {
                return "video/x-m4v";
            }
            else if (extension.Equals("abw"))
            {
                return "application/xml";
            }
            else if (extension.Equals("ogg"))
            {
                return "application/ogg";
            }
            else if (extension.Equals("aflv"))
            {
                return "video/mp4";
            }
            else if (extension.Equals("mp3"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("ctz"))
            {
                return "application/zip";
            }
            else if (extension.Equals("wma"))
            {
                return "video/x-ms-asf";
            }
            else if (extension.Equals("jar"))
            {
                return "application/x-java-archive";
            }
            else if (extension.Equals("oga"))
            {
                return "application/ogg";
            }
            else if (extension.Equals("wpd"))
            {
                return "application/wordperf";
            }
            else if (extension.Equals("wpd"))
            {
                return "application/wordperfect";
            }
            else if (extension.Equals("wpd"))
            {
                return "application/wpd";
            }
            else if (extension.Equals("qpw"))
            {
                return "application/x-quattro-pro";
            }
            else if (extension.Equals("mp4"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("mkv"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("ogv"))
            {
                return "application/ogg";
            }
            else if (extension.Equals("wpd"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("srt"))
            {
                return "text/plain";
            }
            else if (extension.Equals("wma"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("m4a"))
            {
                return "audio/mp4";
            }
            else if (extension.Equals("m4a"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("ogg"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("flac"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("avi"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("wmv"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("mov"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("qt"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("mpg"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("ogv"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("flv"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("3gp"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("vob"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("rar"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("zip"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("xlsb"))
            {
                return "application/vnd.ms-excel";
            }
            else if (extension.Equals("wmv"))
            {
                return "video/x-ms-asf";
            }
            else if (extension.Equals("aac"))
            {
                return "audio/x-hx-aac-adts";
            }
            else if (extension.Equals("ac3"))
            {
                return "application/octet-stream";
            }
            else if (extension.Equals("xcf"))
            {
                return "image/x-xcf";
            }
            return string.Empty;
        }
        /// <summary>
        /// Encrypts a string
        /// </summary>
        /// <param name="clearText">String to Encrypt</param>
        /// <returns>Encrypted string</returns>
        public static string encrypt(string clearText)
        {
            string EncryptionKey = "Globus7879";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        /// <summary>
        /// Decrypyts the encoded string
        /// </summary>
        /// <param name="encryptedString">String to Decrypt</param>
        /// <returns>Decrypted string</returns>
        public static string decrypt(string encryptedString)
        {
            try
            {
                string EncryptionKey = "Globus7879";
                byte[] cipherBytes = Convert.FromBase64String(encryptedString);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        encryptedString = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return encryptedString;
        }



        #region new Encryption Decryption
        //public static string Encrypt(string clearText)
        //{
        //    string EncryptionKey = "YFpoGQ@$VrUMf64tZ9eg^RiaQSZ^Pw%*";
        //    AesCryptoServiceProvider aesCryptoProvider = new AesCryptoServiceProvider();

        //    byte[] byteBuff;

        //    try
        //    {
        //        aesCryptoProvider.Key = Encoding.UTF8.GetBytes(EncryptionKey);

        //        aesCryptoProvider.GenerateIV();
        //        aesCryptoProvider.IV = aesCryptoProvider.IV;
        //        byteBuff = Encoding.UTF8.GetBytes(clearText);

        //        byte[] encoded = aesCryptoProvider.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length);

        //        string ivHexString = ToHexString(aesCryptoProvider.IV);
        //        string encodedHexString = ToHexString(encoded);


        //        return ivHexString + ':' + encodedHexString;

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //        return null;
        //    }
        //}
        //public static string Decrypt(string encryptedString)
        //{
        //    string EncryptionKey = "YFpoGQ@$VrUMf64tZ9eg^RiaQSZ^Pw%*";
        //    AesCryptoServiceProvider aesCryptoProvider = new AesCryptoServiceProvider();

        //    byte[] byteBuff;

        //    try
        //    {
        //        aesCryptoProvider.Key = Encoding.UTF8.GetBytes(EncryptionKey);


        //        string[] textParts = encryptedString.Split(':');
        //        byte[] iv = FromHexString(textParts[0]);
        //        aesCryptoProvider.IV = iv;
        //        byteBuff = FromHexString(textParts[1]);

        //        string plaintext = Encoding.UTF8.GetString(aesCryptoProvider.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));

        //        return plaintext;

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //        return null;
        //    }
        //}
        //public static string ToHexString(byte[] str)
        //{
        //    var sb = new StringBuilder();

        //    var bytes = str;
        //    foreach (var t in bytes)
        //    {
        //        sb.Append(t.ToString("X2"));
        //    }

        //    return sb.ToString();
        //}
        //public static byte[] FromHexString(string hexString)
        //{
        //    var bytes = new byte[hexString.Length / 2];
        //    for (var i = 0; i < bytes.Length; i++)
        //    {
        //        bytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
        //    }

        //    return bytes;
        //} 
        #endregion





        /// <summary>
        /// Gets the assembly version
        /// </summary>
        /// <param name="assemblyType">Type of the assembly</param>
        /// <returns>Version of the passed type of assembly</returns>
        public static string getAssemblyVersion(Type assemblyType)
        {
            return AssemblyName.GetAssemblyName(Assembly.GetAssembly(assemblyType).Location).Version.ToString();
        }
        /// <summary>
        /// Gets the assembly version of the calling assembly
        /// </summary>
        /// <returns>Version of the calling assembly</returns>
        public static string getCallingAssemblyVersion()
        {
            return AssemblyName.GetAssemblyName(Assembly.GetCallingAssembly().Location).Version.ToString();
        }
        /// <summary>
        /// Gets the assembly version of the entry assembly
        /// </summary>
        /// <returns>Version of the entry assembly</returns>
        public static string getEntryAssemblyVersion()
        {
            return AssemblyName.GetAssemblyName(Assembly.GetEntryAssembly().Location).Version.ToString();
        }
        /// <summary>
        /// Gets the assembly version of the executing assembly
        /// </summary>
        /// <returns>Version of the executing assembly</returns>
        public static string getExecutingAssemblyVersion()
        {
            return AssemblyName.GetAssemblyName(Assembly.GetExecutingAssembly().Location).Version.ToString();
        }
        /// <summary>
        /// Gets the Mac Id of the system
        /// </summary>
        /// <returns>Mac Id of the system</returns>
        public static string fetchMacId()
        {
            string macAddresses = "";
            try
            {
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == OperationalStatus.Up)
                    {
                        if (!string.IsNullOrEmpty(macAddresses))
                        {
                            break;
                        }
                        macAddresses += nic.GetPhysicalAddress().ToString();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return macAddresses;
        }
        /// <summary>
        /// Get a DateTime instance of when the system started
        /// </summary>
        /// <returns>DateTime instance of when the system started</returns>
        public static DateTime getSystemStartTime()
        {
            // create a new management object searcher and pass it
            // the select query
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT LastBootUpTime FROM Win32_OperatingSystem   WHERE Primary='true'");
            // get the datetime value and set the local boot
            // time variable to contain that value
            foreach (ManagementObject mo in searcher.Get())
            {
                DateTime dtBootTime = ManagementDateTimeConverter.ToDateTime(mo.Properties["LastBootUpTime"].Value.ToString());
                return dtBootTime;
                // display the start time and date
            }
            return new DateTime();
        }
        /// <summary>
        /// Gets the currently logged on user from a windows service
        /// </summary>
        /// <returns>Username</returns>
        public static string getCurrentUserFromWMI()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT UserName FROM Win32_ComputerSystem");
            ManagementObjectCollection collection = searcher.Get();
            string username = (string)collection.Cast<ManagementBaseObject>().First()["UserName"];
            if (username.Contains("\\"))
            {
                username = username.Split('\\')[1];
            }
            return username;
        }
      
        public static string generateUnixTimeStamp()
        {
            return Convert.ToDouble(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds).ToString();
        }
        /// <summary>
        /// Gets the Human DateTime
        /// </summary>
        /// <param name="timeStamp">Time Stamp to be coverted</param>
        /// <returns>DateTime instance</returns>
        public static DateTime generateHumanTimeFromUnixTimeStamp(string timeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Convert.ToDouble(timeStamp));
        }
        /// <summary>
        /// Decode the access token
        /// </summary>
        /// <param name="accessToken">Access token to be decoded</param>
        /// <returns>Instance of AccessToken</returns>


        public static string Encrypt(string str)
        {
            AesCryptoServiceProvider aesCryptoProvider = new AesCryptoServiceProvider();
            string key = "d%_eQHBl]{E(/TYe>/h#tKe.#_Ah^Q1h";
            byte[] byteBuff;

            try
            {
                aesCryptoProvider.Key = Encoding.UTF8.GetBytes(key);

                aesCryptoProvider.GenerateIV();
                aesCryptoProvider.IV = aesCryptoProvider.IV;
                byteBuff = Encoding.UTF8.GetBytes(str);

                byte[] encoded = aesCryptoProvider.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length);

                string ivHexString = ToHexString(aesCryptoProvider.IV);
                string encodedHexString = ToHexString(encoded);


                return ivHexString + ':' + encodedHexString;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string Decrypt(string encodedStr, string key)
        {
            AesCryptoServiceProvider aesCryptoProvider = new AesCryptoServiceProvider();

            byte[] byteBuff;

            try
            {
                aesCryptoProvider.Key = Encoding.UTF8.GetBytes(key);


                string[] textParts = encodedStr.Split(':');
                byte[] iv = FromHexString(textParts[0]);
                aesCryptoProvider.IV = iv;
                byteBuff = FromHexString(textParts[1]);

                string plaintext = Encoding.UTF8.GetString(aesCryptoProvider.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));

                return plaintext;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public static string ToHexString(byte[] str)
        {
            var sb = new StringBuilder();

            var bytes = str;
            foreach (var t in bytes)
            {
                sb.Append(t.ToString("X2"));
            }

            return sb.ToString();
        }

        public static byte[] FromHexString(string hexString)
        {
            var bytes = new byte[hexString.Length / 2];
            for (var i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }

            return bytes;
        }

    }
}
