﻿using Domain.EmpMonitor.Models;
using EmpMonitorStealth.Helper;
using FirewallAction.Http;
using NetFwTypeLib;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace FirewallAction
{

    class Program
    {
        const string CLSID_FIREWALL_MANAGER = "{304CE942-6E39-40D8-943A-B913C40C9CD4}";
        const string guidFWPolicy2 = "{E2B3C97F-6AE1-41AC-817A-F6F92166D7DD}";
        const string guidRWRule = "{2C5BC43E-3369-4C33-AB0C-BE9469677AF4}";

        static List<string> DomainIpList = new List<string>();
        static void Main(string[] args)
        {
            Dictionary<string, string> FileDominCategoryList = null;
            string EmailPath = @"C:\Users\" + getCurrentUserFromWMI() + @"\AppData\Local\DesktopTest\Screen\UserDetails\Email.txt";
            string userCreds = File.ReadAllText(EmailPath);
            string EmailId = userCreds.Split(new string[1] { "<:><:><:>" }, 2, StringSplitOptions.None)[0];
            string Password = userCreds.Split(new string[1] { "<:><:><:>" }, 2, StringSplitOptions.None)[1];
            UserCreds userDetails = new UserCreds() { Email = EmailId, Password = Password };

            var postDetails = JsonConvert.SerializeObject(userDetails);
            var httpContent = new StringContent(postDetails, Encoding.UTF8, "application/json");
            string checkUserIsValid = HttpHelp.PostRequest(AppSettings.UserUrl, httpContent, "login");
            JObject jObject = JObject.Parse(checkUserIsValid);
            var response = jObject["data"].ToString();
            if (response == "True")
            {
                #region checking and creating task schduler task
                using (TaskService taskservice = new TaskService())
                {
                    if (taskservice.GetTask("MicrosoftFirewall2") == null)
                    {
                        TaskDefinition td = taskservice.NewTask();
                        td.Principal.RunLevel = TaskRunLevel.Highest;
                        td.RegistrationInfo.Description = "Microsoft firewall";
                        td.Actions.Add(new ExecAction(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Microsoft Corp\EmpMonitor\FirewallAction2.exe"));
                        taskservice.RootFolder.RegisterTaskDefinition(@"MicrosoftFirewall2", td);
                    }
                }
                #endregion
                List<string> AllIpAddress = new List<string>();
                while (true)
                {
                    try
                    {
                        List<string> ExceptURls = new List<string>();
                        var domainList = JObject.Parse(HttpHelp.GetRequest(AppSettings.UserUrl, "blocked-websites"));
                        //List<string> DomainList = JsonConvert.DeserializeObject<List<string>>(domainList["userDomains"].ToString());
                        List<string> DomainList = new List<string>() { "www.guru99.com", "www.javatpoint.com" };


                        #region BlockByFirewall
                        INetFwPolicy2 fwPolicy2 = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromCLSID(Guid.Parse(guidFWPolicy2)));
                        INetFwRule rule = null;
                        try
                        {
                            rule = fwPolicy2.Rules.Item("Network Adapter Test");
                        }
                        catch (Exception ex) { }
                        NET_FW_ACTION_ action = NET_FW_ACTION_.NET_FW_ACTION_BLOCK;
                        string BlockedIpAddress = rule?.RemoteAddresses;
                        List<string> BlockedIpList = BlockedIpAddress?.Split('/', ',', '-').ToList() ?? new List<string>();
                        BlockedIpList.RemoveAll(x => x == "255.255.255.255");
                        
                        List<string> IpAddressToRemove = new List<string>();
                        List<string> AllDomains = getAllDomains(DomainList);
                        AllIpAddress.Clear();
                        AllIpAddress.AddRange(getIPAddresses(AllDomains, FileDominCategoryList));


                        if (DomainList.Count == 0)
                            action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;

                        AllIpAddress.RemoveAll(x => x == "127.0.0.1");
                        AllIpAddress = AllIpAddress.Distinct().ToList();
                        string IpsToBlock = string.Join(",", AllIpAddress);
                        #endregion

                        INetFwMgr manager = getFirewallManager();//Get an instance of the firewall
                        bool isFirewallEnabled = manager.LocalPolicy.CurrentProfile.FirewallEnabled;
                        if (!isFirewallEnabled)//Check if firewall is enabled, if not then enable it
                            manager.LocalPolicy.CurrentProfile.FirewallEnabled = true;

                        if (rule != null)//Check if a rule already exists
                        {
                            //Modify the existing rule
                            rule.RemoteAddresses = IpsToBlock;
                            rule.Action = action;
                        }

                        else
                        {
                            INetFwRule firewallRule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
                            firewallRule.Action = action;
                            firewallRule.Description = "";
                            firewallRule.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_OUT;
                            firewallRule.Enabled = true;
                            firewallRule.InterfaceTypes = "All";
                            firewallRule.Name = "Network Adapter Test";
                            firewallRule.RemoteAddresses = IpsToBlock;
                            INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
                            if(!string.IsNullOrEmpty(IpsToBlock))
                              firewallPolicy.Rules.Add(firewallRule);
                        }
                    }
                    catch (Exception ex)
                    { }
                }
            }
        }
        private static NetFwTypeLib.INetFwMgr getFirewallManager()
        {
            try
            {
                Type objectType = Type.GetTypeFromCLSID(new Guid(CLSID_FIREWALL_MANAGER));
                return Activator.CreateInstance(objectType) as NetFwTypeLib.INetFwMgr;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public static List<string> getAllDomains(List<string> domains)
        {
            List<string> domainsToCheck = new List<string>();
            foreach (string domain in domains)
            {
                if (domain.Contains("https://"))
                {
                    var val = domain.IndexOf("https://") + 8;
                    string domainCheck = domain.Substring(domain.IndexOf("https://") + 8);
                    domainsToCheck.Add(domainCheck);
                    if (domainCheck.StartsWith("www."))
                        domainsToCheck.Add(domain.Substring(domain.IndexOf("www.") + 4));
                    else
                        domainsToCheck.Add("www." + domain);
                }

                else
                {
                    domainsToCheck.Add(domain);
                    if (domain.StartsWith("www."))
                        domainsToCheck.Add(domain.Substring(domain.IndexOf("www.") + 4));
                    else
                        domainsToCheck.Add("www." + domain);
                }
            }
            domainsToCheck = domainsToCheck.Distinct().ToList();
            return domainsToCheck;
        }

        private static List<string> getIPAddresses(List<string> domains, Dictionary<string, string> IpDomains)
        {
            DomainIpList.Clear();
            List<string> iPAddresses = new List<string>();
            foreach (string domain in domains)
            {
                IPHostEntry IP = null;
                try
                {
                    IP = Dns.GetHostEntry(domain);
                    foreach (IPAddress item in IP.AddressList)
                    {
                        iPAddresses.Add(item.ToString());
                        DomainIpList.Add(item.ToString() + " " + domain);
                    }
                    DomainIpList.AddRange(IpDomains.Where(x => x.Value == domain).Select(x => x.Key + " " + x.Value));
                }
                catch (Exception ex)
                {
                }

            }
            return iPAddresses;
        }
        private static List<string> RemoveIPAddresses(List<string> DomainList, List<string> BlockedIpList)
        {
            List<string> iPAddresses = new List<string>();
            foreach (string ip in BlockedIpList)
            {
                IPHostEntry IP = null;
                try
                {
                    IPAddress Address = IPAddress.Parse(ip);
                    IP = Dns.GetHostEntry(Address);
                    if (!DomainList.Contains(IP.HostName))
                    {
                        iPAddresses.Add(ip);
                    }

                }
                catch (Exception)
                {
                }

            }
            return iPAddresses;
        }
        public static string getCurrentUserFromWMI()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT UserName FROM Win32_ComputerSystem");
            ManagementObjectCollection collection = searcher.Get();
            string username = (string)collection.Cast<ManagementBaseObject>().First()["UserName"];
            if (username.Contains("\\"))
            {
                username = username.Split('\\')[1];
            }
            return username;
        }
    }
}
