﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirewallAction
{
    class UserCreds
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
