﻿using Google.Apis.Download;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace GoogledriveApi.Drivemethods
{
    public class GoogledriveMethods
    {
        private static List<Google.Apis.Drive.v3.Data.File> AllFiles = new List<Google.Apis.Drive.v3.Data.File>();
        static readonly object lockemail = new object();


        public static Google.Apis.Auth.OAuth2.UserCredential getUserAuthentication(string ClientId, string ClientSecret, string RefreshToken)
        {
            try
            {
                var Y_oAuthToken = new Google.Apis.Auth.OAuth2.Responses.TokenResponse { RefreshToken = RefreshToken };
                var Y_oAuthCredentials = new Google.Apis.Auth.OAuth2.UserCredential(new Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow(
                    new Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer
                    {
                        ClientSecrets = new Google.Apis.Auth.OAuth2.ClientSecrets
                        {
                            ClientId = ClientId,
                            ClientSecret = ClientSecret
                        },
                    }), "user", Y_oAuthToken);

                return Y_oAuthCredentials;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string folderCreation(string name, string folderId, string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
                var driveService = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                if (string.IsNullOrEmpty(folderId))
                {
                    var fileMetadata = new Google.Apis.Drive.v3.Data.File()
                    {
                        Name = name,
                        MimeType = "application/vnd.google-apps.folder"
                    };
                    var request = driveService.Files.Create(fileMetadata);
                    request.Fields = "id,name,parents";
                    var file = request.Execute();
                    Google.Apis.Drive.v3.Data.Permission newPermission = new Google.Apis.Drive.v3.Data.Permission();
                    newPermission.Type = "anyone";
                    newPermission.Role = "reader";
                    driveService.Permissions.Create(newPermission, file.Id).Execute();
                    AllFiles.Add(file);
                    return file.Id;
                }
                else
                {
                    var fileMetadata = new Google.Apis.Drive.v3.Data.File()
                    {
                        Name = name,
                        MimeType = "application/vnd.google-apps.folder",
                        Parents = new List<string>
                                {
                                    folderId
                                }
                    };
                    var request = driveService.Files.Create(fileMetadata);
                    request.Fields = "id,name,parents";
                    var file = request.Execute();
                    Google.Apis.Drive.v3.Data.Permission newPermission = new Google.Apis.Drive.v3.Data.Permission();
                    newPermission.Type = "anyone";
                    newPermission.Role = "reader";
                    driveService.Permissions.Create(newPermission, file.Id).Execute();
                    AllFiles.Add(file);
                    return file.Id;
                }
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }

        }

        public static string fileUploading(string folderId, string fileName, string filePath, string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
                var driveService = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                var fileMetaData = new Google.Apis.Drive.v3.Data.File()
                {
                    Name = fileName,
                    Parents = new List<string>
                    {
                        folderId
                    }
                };
                Google.Apis.Drive.v3.FilesResource.CreateMediaUpload fileUploadrequest;
                using (var stream = new System.IO.FileStream(filePath,
                    System.IO.FileMode.Open))
                {
                    fileUploadrequest = driveService.Files.Create(
                        fileMetaData, stream, "image/jpeg");
                    fileUploadrequest.Fields = "id";
                    fileUploadrequest.Upload();
                }
                var files = fileUploadrequest.ResponseBody;
                return files.Id;
            }
            catch (Exception ex)
            {
                //ErrorLogging.log(ex.StackTrace);
                return null;
            }
        }
        public static List<Google.Apis.Drive.v3.Data.File> GetFolderdetails(string[] ids, string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                List<Google.Apis.Drive.v3.Data.File> filess = new List<Google.Apis.Drive.v3.Data.File>();
                var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
                var driveService = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                Google.Apis.Drive.v3.FilesResource.ListRequest listRequest = driveService.Files.List();
                listRequest.PageSize = 1000;
                listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink)";
                var result = listRequest.Execute();
                IList<Google.Apis.Drive.v3.Data.File> filesss = result.Files;
                try
                {
                    filess.AddRange(filesss.ToList());
                }
                catch (Exception)
                {

                }
                while (filesss != null && filesss.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(result.NextPageToken))
                    {
                        try
                        {
                            listRequest = driveService.Files.List();
                            listRequest.PageToken = result.NextPageToken;
                            listRequest.PageSize = 1000;
                            listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink,parents)";
                            result = listRequest.Execute();
                            filesss = result.Files;
                            filess.AddRange(filesss.ToList());

                        }
                        catch (Exception)
                        {
                            Thread.Sleep(5000);
                            listRequest = driveService.Files.List();
                            listRequest.PageToken = result.NextPageToken;
                            listRequest.PageSize = 1000;
                            listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink,parents)";
                            result = listRequest.Execute();
                            filesss = result.Files;
                            filess.AddRange(filesss.ToList());
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                var checks = filess.ToList().Where(x => x.Trashed == false && (ids.Contains(x.Id))).ToList();
                return checks;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<Google.Apis.Drive.v3.Data.File> GetFolderdetails(string[] ids, string[] names, string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                List<Google.Apis.Drive.v3.Data.File> filess = new List<Google.Apis.Drive.v3.Data.File>();
                var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
                var driveService = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                Google.Apis.Drive.v3.FilesResource.ListRequest listRequest = driveService.Files.List();
                listRequest.PageSize = 1000;
                listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink)";
                var result = listRequest.Execute();
                IList<Google.Apis.Drive.v3.Data.File> filesss = result.Files;
                try
                {
                    filess.AddRange(filesss.ToList());
                }
                catch (Exception)
                {

                }
                while (filesss != null && filesss.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(result.NextPageToken))
                    {
                        try
                        {
                            listRequest = driveService.Files.List();
                            listRequest.PageToken = result.NextPageToken;
                            listRequest.PageSize = 1000;
                            listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink,parents)";
                            result = listRequest.Execute();
                            filesss = result.Files;
                            filess.AddRange(filesss.ToList());

                        }
                        catch (Exception)
                        {
                            Thread.Sleep(5000);
                            listRequest = driveService.Files.List();
                            listRequest.PageToken = result.NextPageToken;
                            listRequest.PageSize = 1000;
                            listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink,parents)";
                            result = listRequest.Execute();
                            filesss = result.Files;
                            filess.AddRange(filesss.ToList());
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                var checks = filess.ToList().Where(x => x.Trashed == false && (ids.Contains(x.Id)) && (!names.Contains(x.Name))).ToList();
                return checks;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<Google.Apis.Drive.v3.Data.File> GetFolderdetails(string folderName, string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                List<Google.Apis.Drive.v3.Data.File> filess = new List<Google.Apis.Drive.v3.Data.File>();
                var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
                var driveService = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                Google.Apis.Drive.v3.FilesResource.ListRequest listRequest = driveService.Files.List();
                listRequest.PageSize = 1000;
                listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink)";
                var result = listRequest.Execute();
                IList<Google.Apis.Drive.v3.Data.File> filesss = result.Files;
                try
                {
                    filess.AddRange(filesss.ToList());
                }
                catch (Exception)
                {

                }
                while (filesss != null && filesss.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(result.NextPageToken))
                    {
                        try
                        {
                            listRequest = driveService.Files.List();
                            listRequest.PageToken = result.NextPageToken;
                            listRequest.PageSize = 1000;
                            listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink)";
                            Thread.Sleep(2000);
                            result = listRequest.Execute();
                            filesss = result.Files;
                            filess.AddRange(filesss.ToList());
                        }
                        catch (Exception)
                        {
                            Thread.Sleep(5000);
                            listRequest = driveService.Files.List();
                            listRequest.PageToken = result.NextPageToken;
                            listRequest.PageSize = 1000;
                            listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink)";
                            Thread.Sleep(2000);
                            result = listRequest.Execute();
                            filesss = result.Files;
                            filess.AddRange(filesss.ToList());
                        }

                    }
                    else
                    {
                        break;
                    }
                }
                var checks = filess.ToList().Where(x => x.Name.Equals(folderName) && x.Trashed == false).ToList();
                return checks;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// will get all the files from drive
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="ids"></param>
        /// <param name="cli_id"></param>
        /// <param name="cli_sec"></param>
        /// <param name="ref_token"></param>
        /// <returns></returns>
        public static List<Google.Apis.Drive.v3.Data.File> GetFolderdetails(string folderName, string[] ids, string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                List<Google.Apis.Drive.v3.Data.File> filess = new List<Google.Apis.Drive.v3.Data.File>();
                var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
                var driveService = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                Google.Apis.Drive.v3.FilesResource.ListRequest listRequest = driveService.Files.List();
                listRequest.PageSize = 1000;
                listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink)";
                var result = listRequest.Execute();
                IList<Google.Apis.Drive.v3.Data.File> filesss = result.Files;
                try
                {
                    filess.AddRange(filesss.ToList());
                }
                catch (Exception)
                {

                }
                while (filesss != null && filesss.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(result.NextPageToken))
                    {
                        try
                        {
                            listRequest = driveService.Files.List();
                            listRequest.PageToken = result.NextPageToken;
                            listRequest.PageSize = 1000;
                            listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink,parents)";
                            result = listRequest.Execute();
                            filesss = result.Files;
                            filess.AddRange(filesss.ToList());

                        }
                        catch (Exception)
                        {
                            Thread.Sleep(5000);
                            listRequest = driveService.Files.List();
                            listRequest.PageToken = result.NextPageToken;
                            listRequest.PageSize = 1000;
                            listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink,parents)";
                            result = listRequest.Execute();
                            filesss = result.Files;
                            filess.AddRange(filesss.ToList());
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                var checks = filess.ToList().Where(x => x.Name.Equals(folderName) && x.Trashed == false && (ids.Contains(x.Id))).ToList();
                return checks;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<Google.Apis.Drive.v3.Data.File> GetFolderdetails(string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                List<Google.Apis.Drive.v3.Data.File> filess = new List<Google.Apis.Drive.v3.Data.File>();
                var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
                var driveService = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                Google.Apis.Drive.v3.FilesResource.ListRequest listRequest = driveService.Files.List();
                listRequest.Q = "mimeType = 'application/vnd.google-apps.folder' and trashed = false";
                listRequest.PageSize = 1000;
                listRequest.Fields = "nextPageToken, files(name,id,parents)";
                var result = listRequest.Execute();
                IList<Google.Apis.Drive.v3.Data.File> filesss = result.Files;
                filess.AddRange(filesss);
                while (filesss != null && filesss.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(result.NextPageToken))
                    {
                        result = listRequest.Execute();
                        filess.AddRange(result.Files);
                    }
                    else
                    {
                        break;
                    }
                }
                // var checks = filess.ToList().Where(x => x.Trashed == false).ToList();
                return filess;
            }
            catch (Exception ex)
            {
                Thread.Sleep(5000);
                //ErrorLogging.log(ex.ToString());
                throw;
            }
        }


        public static Google.Apis.Drive.v2.Data.ChildList getChildren(string folderId, string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
                var driveService = new Google.Apis.Drive.v2.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                Google.Apis.Drive.v2.ChildrenResource.ListRequest requestss = driveService.Children.List(folderId);
                Google.Apis.Drive.v2.Data.ChildList children = requestss.Execute();
                return children;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Google.Apis.Drive.v3.Data.File> getALLfolderfiles(string folderId, string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                List<Google.Apis.Drive.v3.Data.File> lstfiles = new List<Google.Apis.Drive.v3.Data.File>();
                var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
                var driveService = new Google.Apis.Drive.v2.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                var service = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = Y_oAuthCredentials,
                    ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
                });
                Google.Apis.Drive.v2.ChildrenResource.ListRequest requestss = driveService.Children.List(folderId);
                Google.Apis.Drive.v2.Data.ChildList children = requestss.Execute();
                foreach (Google.Apis.Drive.v2.Data.ChildReference child in children.Items)
                {
                    var req = driveService.Files.Get(child.Id);
                    Google.Apis.Drive.v3.FilesResource.GetRequest getres = service.Files.Get(child.Id);
                    getres.Fields = "webViewLink, name,id,trashed";
                    Google.Apis.Drive.v3.Data.File getfile = getres.Execute();
                    lstfiles.Add(getfile);
                }
                return lstfiles;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static void deleteFoler(string folderId, string cli_id, string cli_sec, string ref_token)
        {
            List<Google.Apis.Drive.v3.Data.File> lstfiles = new List<Google.Apis.Drive.v3.Data.File>();
            var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
            var driveService = new Google.Apis.Drive.v2.DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = Y_oAuthCredentials,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });
            driveService.Files.Delete(folderId).Execute();
        }


        //public static string uploadFilesOld(string filePath, string EmailID, string Date, string Hour, string cli_id, string cli_sec, string ref_token)
        //{
        //    string SimpleUpload = "failure";
        //    try
        //    {
        //        string fileId = string.Empty;
        //        // ref_token = "1/D4lworJTfo_4liOOM1ffr88MjOxm7boYIXhalxWLjXk";
        //        //cli_sec = "UKu5F3NF6L79iFE4XRIuu94G";
        //        // cli_id = "1042383277802-c2khno03l410tbrpjigls5b84j73f5te.apps.googleusercontent.com";
        //        string fileName = new FileInfo(filePath).Name;
        //        Google.Apis.Drive.v2.Data.ChildList childList = new Google.Apis.Drive.v2.Data.ChildList();
        //        List<Google.Apis.Drive.v3.Data.File> folderdetails = null;

        //        //EmailID = "demodrive@drive.com";
        //        // List<Google.Apis.Drive.v3.Data.File> allfiles = GetFolderdetails(cli_id, cli_sec, ref_token);

        //        if (AllFiles == null)
        //        {
        //            AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //        }

        //        try
        //        {
        //            folderdetails = AllFiles.Where(x => x.Name.Equals("EmpMonitor") && x.Trashed == false).ToList();//getFolderdetails("EmpMonitor", cli_id, cli_sec, ref_token);
        //        }
        //        catch (Exception ex)
        //        {
        //            // ignored
        //        }

        //        if (folderdetails.Count < 1)
        //        {
        //            string foldeId = folderCreation("EmpMonitor", "", cli_id, cli_sec, ref_token);
        //            childList = getChildren(foldeId, cli_id, cli_sec, ref_token);
        //            if (childList.Items.Count < 1)
        //            {
        //                foldeId = folderCreation(EmailID, foldeId, cli_id, cli_sec, ref_token);
        //                childList = getChildren(foldeId, cli_id, cli_sec, ref_token);
        //                if (childList.Items.Count < 1)
        //                {
        //                    foldeId = folderCreation(Date, foldeId, cli_id, cli_sec, ref_token);
        //                    childList = getChildren(foldeId, cli_id, cli_sec, ref_token);
        //                    if (childList.Items.Count < 1)
        //                    {
        //                        foldeId = folderCreation(Hour, foldeId, cli_id, cli_sec, ref_token);
        //                        fileId = fileUploading(foldeId, fileName, filePath, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(fileId))
        //                        {
        //                            SimpleUpload = "Success";
        //                            AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {

        //            }
        //        }
        //        else
        //        {
        //            //Google.Apis.Drive.v2.Data.ChildList childList = getChildren(folderdetails[0].Id, cli_id, cli_sec, ref_token);
        //            //var res = childList.Items.Select(x => x.Id).ToArray();
        //            List<Google.Apis.Drive.v3.Data.File> getFolderdetailssEmailID = AllFiles.ToList().Where(x => x.Name.Equals(EmailID) && x.Trashed == false && x.Parents[0].Equals(folderdetails[0].Id)).ToList(); //getFolderdetails(EmailID, res, cli_id, cli_sec, ref_token);
        //            if (getFolderdetailssEmailID.Count > 0)
        //            {
        //                //childList = getChildren(getFolderdetailssEmailID[0].Id, cli_id, cli_sec, ref_token);
        //                //res = childList.Items.Select(x => x.Id).ToArray();
        //                List<Google.Apis.Drive.v3.Data.File> getFolderdetailssDate = AllFiles.ToList().Where(x => x.Name.Equals(Date) && x.Trashed == false && x.Parents[0].Equals(getFolderdetailssEmailID[0].Id)).ToList();//getFolderdetails(Date, res, cli_id, cli_sec, ref_token);
        //                if (getFolderdetailssDate.Count > 0)
        //                {
        //                    //childList = getChildren(getFolderdetailssDate[0].Id, cli_id, cli_sec, ref_token);
        //                    //res = childList.Items.Select(x => x.Id).ToArray();
        //                    List<Google.Apis.Drive.v3.Data.File> getFolderdetailssHour = AllFiles.ToList().Where(x => x.Name.Equals(Hour) && x.Trashed == false && x.Parents[0].Equals(getFolderdetailssDate[0].Id)).ToList();//getFolderdetails(Hour, res, cli_id, cli_sec, ref_token);
        //                    if (getFolderdetailssHour.Count > 0)
        //                    {
        //                        fileId = fileUploading(getFolderdetailssHour[0].Id, fileName, filePath, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(fileId))
        //                        {
        //                            SimpleUpload = "Success";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string folderId = folderCreation(Hour, getFolderdetailssDate[0].Id, cli_id, cli_sec, ref_token);
        //                        fileId = fileUploading(folderId, fileName, filePath, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(fileId))
        //                        {
        //                            SimpleUpload = "Success";
        //                            AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    string folderId = folderCreation(Date, getFolderdetailssEmailID[0].Id, cli_id, cli_sec, ref_token);
        //                    string folderIdHrs = folderCreation(Hour, folderId, cli_id, cli_sec, ref_token);
        //                    fileId = fileUploading(folderIdHrs, fileName, filePath, cli_id, cli_sec, ref_token);
        //                    if (!string.IsNullOrEmpty(fileId))
        //                    {
        //                        SimpleUpload = "Success";
        //                        AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                string folderId = folderCreation(EmailID, folderdetails[0].Id, cli_id, cli_sec, ref_token);
        //                if (!String.IsNullOrEmpty(folderId))
        //                {
        //                    string folderIdDate = folderCreation(Date, folderId, cli_id, cli_sec, ref_token);
        //                    if (!String.IsNullOrEmpty(folderId))
        //                    {
        //                        string folderIdHour = folderCreation(Hour, folderIdDate, cli_id, cli_sec, ref_token);
        //                        fileId = fileUploading(folderIdHour, fileName, filePath, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(fileId))
        //                        {
        //                            SimpleUpload = "Success";
        //                            AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    string folderIdDate = folderCreation(Date, folderId, cli_id, cli_sec, ref_token);
        //                    if (!String.IsNullOrEmpty(folderIdDate))
        //                    {
        //                        string folderIdHrs = folderCreation(Hour, folderIdDate, cli_id, cli_sec, ref_token);
        //                        fileId = fileUploading(folderIdHrs, fileName, filePath, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(fileId))
        //                        {
        //                            SimpleUpload = "Success";
        //                            AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        SimpleUpload = "failure";
        //    }

        //    return SimpleUpload;
        //}

        //public static string uploadFiles(string filePath, string EmailID, string Date, string Hour, string cli_id, string cli_sec, string ref_token)
        //{
        //    string SimpleUpload = "failure";
        //    try
        //    {
        //        string fileId = string.Empty;
        //        string fileName = new FileInfo(filePath).Name;
        //        Google.Apis.Drive.v2.Data.ChildList childList = new Google.Apis.Drive.v2.Data.ChildList();
        //        List<Google.Apis.Drive.v3.Data.File> folderdetails = null;
        //        if (AllFiles == null)
        //        {
        //            AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //        }
        //        try
        //        {
        //            folderdetails = AllFiles.Where(x => x.Name.Equals("EmpMonitor")).ToList();//getFolderdetails("EmpMonitor", cli_id, cli_sec, ref_token);
        //        }
        //        catch (Exception ex) { }
        //        #region if Empmonitor not presents
        //        if (folderdetails == null || folderdetails.Count < 1)
        //        {
        //            string foldeId = folderCreation("EmpMonitor", "", cli_id, cli_sec, ref_token);
        //            if (!string.IsNullOrEmpty(foldeId))
        //            {
        //                string EmailfoldeId = folderCreation(EmailID, foldeId, cli_id, cli_sec, ref_token);
        //                if (!string.IsNullOrEmpty(EmailfoldeId))
        //                {
        //                    foldeId = folderCreation(DateTime.Now.Year.ToString(), EmailfoldeId, cli_id, cli_sec, ref_token);
        //                    if (!string.IsNullOrEmpty(foldeId))
        //                    {
        //                        foldeId = folderCreation(DateTime.Now.ToString("MMM"), EmailfoldeId, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(foldeId))
        //                        {
        //                            foldeId = folderCreation(Date, EmailfoldeId, cli_id, cli_sec, ref_token);
        //                            if (!string.IsNullOrEmpty(foldeId))
        //                            {
        //                                foldeId = folderCreation(Hour, foldeId, cli_id, cli_sec, ref_token);
        //                                fileId = fileUploading(foldeId, fileName, filePath, cli_id, cli_sec, ref_token);
        //                                if (!string.IsNullOrEmpty(fileId))
        //                                {
        //                                    SimpleUpload = "Success";
        //                                    //AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            List<Google.Apis.Drive.v3.Data.File> getFolderdetailssEmailID = AllFiles.Where(x => x.Name.Equals(EmailID) && x.Parents[0].Equals(folderdetails[0].Id)).ToList(); //getFolderdetails(EmailID, res, cli_id, cli_sec, ref_token);
        //            if (getFolderdetailssEmailID.Count > 0)
        //            {
        //                List<Google.Apis.Drive.v3.Data.File> getFolderdetailssDate = AllFiles.ToList().Where(x => x.Name.Equals(Date) && x.Parents[0].Equals(getFolderdetailssEmailID[0].Id)).ToList();//getFolderdetails(Date, res, cli_id, cli_sec, ref_token);
        //                if (getFolderdetailssDate.Count > 0)
        //                {
        //                    List<Google.Apis.Drive.v3.Data.File> getFolderdetailssHour = AllFiles.ToList().Where(x => x.Name.Equals(Hour) && x.Parents[0].Equals(getFolderdetailssDate[0].Id)).ToList();//getFolderdetails(Hour, res, cli_id, cli_sec, ref_token);
        //                    if (getFolderdetailssHour.Count > 0)
        //                    {
        //                        fileId = fileUploading(getFolderdetailssHour[0].Id, fileName, filePath, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(fileId))
        //                        {
        //                            SimpleUpload = "Success";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string folderId = folderCreation(Hour, getFolderdetailssDate[0].Id, cli_id, cli_sec, ref_token);
        //                        fileId = fileUploading(folderId, fileName, filePath, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(fileId))
        //                        {
        //                            SimpleUpload = "Success";
        //                            //AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    string folderId = folderCreation(Date, getFolderdetailssEmailID[0].Id, cli_id, cli_sec, ref_token);
        //                    string folderIdHrs = folderCreation(Hour, folderId, cli_id, cli_sec, ref_token);
        //                    fileId = fileUploading(folderIdHrs, fileName, filePath, cli_id, cli_sec, ref_token);
        //                    if (!string.IsNullOrEmpty(fileId))
        //                    {
        //                        SimpleUpload = "Success";
        //                        //AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                    }

        //                }
        //            }
        //            else
        //            {
        //                string folderId = folderCreation(EmailID, folderdetails[0].Id, cli_id, cli_sec, ref_token);
        //                if (!String.IsNullOrEmpty(folderId))
        //                {
        //                    string folderIdDate = folderCreation(Date, folderId, cli_id, cli_sec, ref_token);
        //                    if (!String.IsNullOrEmpty(folderId))
        //                    {
        //                        string folderIdHour = folderCreation(Hour, folderIdDate, cli_id, cli_sec, ref_token);
        //                        fileId = fileUploading(folderIdHour, fileName, filePath, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(fileId))
        //                        {
        //                            SimpleUpload = "Success";
        //                            //AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    string folderIdDate = folderCreation(Date, folderId, cli_id, cli_sec, ref_token);
        //                    if (!String.IsNullOrEmpty(folderIdDate))
        //                    {
        //                        string folderIdHrs = folderCreation(Hour, folderIdDate, cli_id, cli_sec, ref_token);
        //                        fileId = fileUploading(folderIdHrs, fileName, filePath, cli_id, cli_sec, ref_token);
        //                        if (!string.IsNullOrEmpty(fileId))
        //                        {
        //                            SimpleUpload = "Success";
        //                            //AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLogging.log(ex.ToString());
        //        SimpleUpload = "failure";
        //        throw;
        //    }
        //    return SimpleUpload;
        //}

        public static bool uploadFiles(string filePath, string EmailID, string Date, string Hour, string cli_id, string cli_sec, string ref_token, DateTime fileUploadDateTime)
        {
            bool SimpleUpload = false;
            string EmpMonitorfoldeId = null, EmailFolderId = null, YearFolderId = null;
            try
            {
                lock (lockemail)
                {
                    string fileId = string.Empty;
                    string fileName = new FileInfo(filePath).Name;
                    //List<Google.Apis.Drive.v3.Data.File> folderdetails = null;

                    #region EmpMonitor
                    EmpMonitorfoldeId = GetFolderId(cli_id, cli_sec, ref_token, "EmpMonitor");
                    #endregion

                    #region Email
                    EmailFolderId = GetFolderId(cli_id, cli_sec, ref_token, EmailID, EmpMonitorfoldeId);
                    #endregion
                    //string MainFolderId = GetFolderId(cli_id, cli_sec, ref_token, EmailID, EmpMonitorfoldeId);
                    #region Year                  
                    YearFolderId = GetFolderId(cli_id, cli_sec, ref_token, fileUploadDateTime.ToString("yyyy-MM-dd"), EmailFolderId);
                    #endregion

                    if (!string.IsNullOrEmpty(YearFolderId))
                    {
                        try
                        {
                            fileId = fileUploading(YearFolderId, Hour+"-"+fileName, filePath, cli_id, cli_sec, ref_token);
                        }
                        catch (Exception ex)
                        {

                            throw;
                        }
                        if (!string.IsNullOrEmpty(fileId))
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorLogging.log(ex.ToString());
                SimpleUpload = false;
                throw;
            }
            return SimpleUpload;
        }

        [Obsolete]
        public static bool uploadFilesold2(string filePath, string EmailID, string Date, string Hour, string cli_id, string cli_sec, string ref_token, DateTime fileUploadDateTime)
        {
            bool SimpleUpload = false;
            string EmpMonitorfoldeId = null, EmailFolderId = null, YearFolderId = null, MonthFolderId = null, DateFolderId = null, HourFolderId = null;
            try
            {
                lock (lockemail)
                {
                    string fileId = string.Empty;
                    string fileName = new FileInfo(filePath).Name;
                    List<Google.Apis.Drive.v3.Data.File> folderdetails = null;
                    if (AllFiles == null)
                    {
                        AllFiles = GetFolderdetails(cli_id, cli_sec, ref_token);
                    }
                    try
                    {
                        folderdetails = AllFiles.Where(x => x.Name.Equals("EmpMonitor")).ToList();
                    }
                    catch (Exception ex) { }

                    #region EmpMonitor
                    if (folderdetails == null || folderdetails.Count < 1)
                        EmpMonitorfoldeId = folderCreation("EmpMonitor", "", cli_id, cli_sec, ref_token);
                    else
                        EmpMonitorfoldeId = AllFiles.Single(x => x.Name.Equals("EmpMonitor")).Id;
                    #endregion
                    #region Email
                    if (!string.IsNullOrEmpty(EmpMonitorfoldeId) && AllFiles.Where(x => x.Name.Equals(EmailID) && x.Parents[0].Equals(EmpMonitorfoldeId)).Count() == 0)
                        EmailFolderId = folderCreation(EmailID, EmpMonitorfoldeId, cli_id, cli_sec, ref_token);
                    else
                        EmailFolderId = AllFiles.Single(x => x.Name.Equals(EmailID) && x.Parents[0].Equals(EmpMonitorfoldeId)).Id;
                    #endregion

                    #region Year
                    if (!string.IsNullOrEmpty(EmailFolderId) && AllFiles.Where(x => x.Name.Equals(fileUploadDateTime.Year.ToString()) && x.Parents[0].Equals(EmailFolderId)).Count() == 0)
                        YearFolderId = folderCreation(fileUploadDateTime.Year.ToString(), EmailFolderId, cli_id, cli_sec, ref_token);
                    else
                        YearFolderId = AllFiles.First(x => x.Name.Equals(DateTime.Now.Year.ToString()) && x.Parents[0].Equals(EmailFolderId)).Id;
                    #endregion
                    #region Month
                    if (!string.IsNullOrEmpty(YearFolderId) && AllFiles.Where(x => x.Name.Equals(fileUploadDateTime.ToString("MMM")) && x.Parents[0].Equals(YearFolderId)).Count() == 0)
                        MonthFolderId = folderCreation(fileUploadDateTime.ToString("MMM"), YearFolderId, cli_id, cli_sec, ref_token);
                    else
                        MonthFolderId = AllFiles.First(x => x.Name.Equals(fileUploadDateTime.ToString("MMM")) && x.Parents[0].Equals(YearFolderId)).Id;
                    #endregion
                    #region date
                    if (!string.IsNullOrEmpty(MonthFolderId) && AllFiles.Where(x => x.Name.Equals(Date) && x.Parents[0].Equals(MonthFolderId)).Count() == 0)
                        DateFolderId = folderCreation(Date, MonthFolderId, cli_id, cli_sec, ref_token);
                    else
                        DateFolderId = AllFiles.First(x => x.Name.Equals(Date) && x.Parents[0].Equals(MonthFolderId)).Id;
                    #endregion
                    if (!string.IsNullOrEmpty(DateFolderId) && AllFiles.Where(x => x.Name.Equals(Hour) && x.Parents[0].Equals(DateFolderId)).Count() == 0)
                        HourFolderId = folderCreation(Hour, DateFolderId, cli_id, cli_sec, ref_token);
                    else
                        HourFolderId = AllFiles.First(x => x.Name.Equals(Hour) && x.Parents[0].Equals(DateFolderId)).Id;

                    if (!string.IsNullOrEmpty(HourFolderId))
                    {
                        fileId = fileUploading(HourFolderId, fileName, filePath, cli_id, cli_sec, ref_token);
                        if (!string.IsNullOrEmpty(fileId))
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
                //ErrorLogging.log(ex.ToString());
                SimpleUpload = false;
                throw;
            }
            return SimpleUpload;
        }

        public static void deleteFolder(string[] dates, string cli_id, string cli_sec, string ref_token)
        {
            List<Google.Apis.Drive.v3.Data.File> folderdetails = GetFolderdetails("EmpMonitor", cli_id, cli_sec, ref_token);
            Google.Apis.Drive.v2.Data.ChildList childList = getChildren(folderdetails[0].Id, cli_id, cli_sec, ref_token);
            var res = childList.Items.Select(x => x.Id).ToArray();
            List<Google.Apis.Drive.v3.Data.File> folderdetailschild = GetFolderdetails(res, cli_id, cli_sec, ref_token);
            foreach (var item in folderdetailschild)
            {
                try
                {

                    childList = getChildren(item.Id, cli_id, cli_sec, ref_token);
                    res = childList.Items.Select(x => x.Id).ToArray();
                    List<Google.Apis.Drive.v3.Data.File> folderdetailschilddates = GetFolderdetails(res, dates, cli_id, cli_sec, ref_token);
                    foreach (var item_dates in folderdetailschilddates)
                    {
                        try
                        {
                            deleteFoler(item_dates.Id, cli_id, cli_sec, ref_token);
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        public static void deleteemailFolder(string EmailId, string cli_id, string cli_sec, string ref_token)
        {
            try
            {
                List<Google.Apis.Drive.v3.Data.File> folderdetails = GetFolderdetails(EmailId, cli_id, cli_sec, ref_token);
                deleteFoler(folderdetails[0].Id, cli_id, cli_sec, ref_token);
            }
            catch (Exception)
            {

            }
        }


        //public static string bulkDownload(string cli_id, string cli_sec, string ref_token, string path, string foldername, string userEmailid, string[] arrHrs)
        //{
        //    var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
        //    var service = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
        //    {
        //        HttpClientInitializer = Y_oAuthCredentials,
        //        ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
        //    });
        //    var services = new Google.Apis.Drive.v2.DriveService(new BaseClientService.Initializer()
        //    {
        //        HttpClientInitializer = Y_oAuthCredentials,
        //        ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
        //    });
        //    try
        //    {
        //        List<Google.Apis.Drive.v3.Data.File> allfiles = GetFolderdetails(cli_id, cli_sec, ref_token);
        //        List<Google.Apis.Drive.v3.Data.File> folderdetailsemp = allfiles.ToList().Where(x => x.Name.Equals("EmpMonitor") && x.Trashed == false).ToList();//getFolderdetails("EmpMonitor", cli_id, cli_sec, ref_token);
        //                                                                                                                                                         //Google.Apis.Drive.v2.Data.ChildList childListemp = getChildren(folderdetailsemp[0].Id, cli_id, cli_sec, ref_token);
        //                                                                                                                                                         //var resemp = childListemp.Items.Select(x => x.Id).ToArray();
        //        List<Google.Apis.Drive.v3.Data.File> folderdetails = allfiles.ToList().Where(x => x.Name.Equals(userEmailid) && x.Trashed == false && x.Parents[0].Equals(folderdetailsemp[0].Id)).ToList(); //getFolderdetails(userEmailid, resemp, cli_id, cli_sec, ref_token);
        //                                                                                                                                                                                                     //List<Google.Apis.Drive.v3.Data.File> folderdetails = getFolderdetails(emails, cli_id, cli_sec, ref_token);
        //                                                                                                                                                                                                     //Google.Apis.Drive.v2.Data.ChildList childList = getChildren(folderdetails[0].Id, cli_id, cli_sec, ref_token);
        //                                                                                                                                                                                                     //var res = childList.Items.Select(x => x.Id).ToArray();
        //        List<Google.Apis.Drive.v3.Data.File> folderdetailsdates = allfiles.ToList().Where(x => x.Name.Equals(foldername) && x.Trashed == false && x.Parents[0].Equals(folderdetails[0].Id)).ToList(); //getFolderdetails(foldername, res, cli_id, cli_sec, ref_token);
        //                                                                                                                                                                                                      //Google.Apis.Drive.v2.Data.ChildList childListhrs = getChildren(folderdetailsdates[0].Id, cli_id, cli_sec, ref_token);
        //                                                                                                                                                                                                      //var reshrs = childListhrs.Items.Select(x => x.Id).ToArray();
        //        var reshrs = allfiles.ToList().Where(x => x.Trashed == false && arrHrs.Contains(x.Name) && x.Parents[0].Equals(folderdetailsdates[0].Id)).ToList();
        //        var reshrss = reshrs.Select(x => x.Id).ToArray();
        //        Console.WriteLine("Processing...\n");
        //        List<Google.Apis.Drive.v3.Data.File> filess = new List<Google.Apis.Drive.v3.Data.File>();
        //        //Google.Apis.Drive.v3.FilesResource.ListRequest listRequest = service.Files.List();
        //        //listRequest.PageSize = 1000;
        //        //listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink,parents)";
        //        //var result = listRequest.Execute();
        //        //IList<Google.Apis.Drive.v3.Data.File> filesss = result.Files;
        //        //try
        //        //{
        //        //    filess.AddRange(filesss.ToList());
        //        //}
        //        //catch (Exception)
        //        //{

        //        //}
        //        //while (filesss != null && filesss.Count > 0)
        //        //{
        //        //    if (!string.IsNullOrWhiteSpace(result.NextPageToken))
        //        //    {
        //        //        listRequest = service.Files.List();
        //        //        listRequest.PageToken = result.NextPageToken;
        //        //        listRequest.PageSize = 1000;
        //        //        listRequest.Fields = "nextPageToken, files(webViewLink, name,id,trashed,webContentLink)";
        //        //        result = listRequest.Execute();
        //        //        filesss = result.Files;
        //        //        filess.AddRange(filesss.ToList());
        //        //    }
        //        //    else
        //        //    {
        //        //        break;
        //        //    }
        //        //}

        //        filess = allfiles.Where(x => reshrss.Contains(x.Parents[0])).ToList();
        //        foreach (var item in filess)
        //        {
        //            //var parentsId = item.Parents[0];
        //            //if (reshrs.Contains(parentsId))
        //            //{
        //            new Thread(delegate ()
        //            {
        //                DownloadGoogleFile(item.Id, service, item.Name, path);
        //            }).Start();
        //            Thread.Sleep(500);
        //            // }
        //        }

        //        //if (folderdetailsdates != null && folderdetailsdates.Count > 0)
        //        //{
        //        //    foreach (var fil in folderdetailsdates)
        //        //    {
        //        //        Google.Apis.Drive.v2.ChildrenResource.ListRequest requestss = services.Children.List(fil.Id);
        //        //        Google.Apis.Drive.v2.Data.ChildList children = requestss.Execute();
        //        //        foreach (Google.Apis.Drive.v2.Data.ChildReference child in children.Items)
        //        //        {
        //        //            try
        //        //            {
        //        //                GetDownloadChildren(path, service, services, child);
        //        //            }
        //        //            catch (Exception ex)
        //        //            {

        //        //            }

        //        //        }

        //        //        Console.WriteLine(fil.Name, fil.WebViewLink);
        //        //    }
        //        //    //Console.WriteLine(filesss.Count + " records fetched.");
        //        //}
        //        //else
        //        //{
        //        //    Console.WriteLine("No files found.");
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        return "no_data";
        //    }
        //    return "success";
        //}

        private static void GetDownloadChildren(string path, Google.Apis.Drive.v3.DriveService service, Google.Apis.Drive.v2.DriveService services, Google.Apis.Drive.v2.Data.ChildReference child)
        {
            Google.Apis.Drive.v2.ChildrenResource.ListRequest requestsfiles = services.Children.List(child.Id);
            Google.Apis.Drive.v2.Data.ChildList childrenfile = requestsfiles.Execute();
            foreach (Google.Apis.Drive.v2.Data.ChildReference child_file in childrenfile.Items)
            {
                downloadfiles(path, service, child_file);
            }
        }

        private static void downloadfiles(string path, Google.Apis.Drive.v3.DriveService service, Google.Apis.Drive.v2.Data.ChildReference child_file)
        {
            try
            {
                Google.Apis.Drive.v3.FilesResource.GetRequest getres = service.Files.Get(child_file.Id);
                getres.Fields = "webViewLink, name,id,trashed";
                Google.Apis.Drive.v3.Data.File getfile = getres.Execute();
                DownloadGoogleFile(child_file.Id, service, getfile.Name, path);
            }
            catch (Exception ex)
            {

            }
        }

        public static string DownloadGoogleFile(string fileId, Google.Apis.Drive.v3.DriveService service, string fileName, string path)
        {

            try
            {
                Google.Apis.Drive.v3.FilesResource.GetRequest request = service.Files.Get(fileId);
                string FileName = fileName;//request.Execute().Name;
                string FilePath = System.IO.Path.Combine(path, FileName);

                MemoryStream stream1 = new MemoryStream();

                request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
                {
                    switch (progress.Status)
                    {
                        case DownloadStatus.Downloading:
                            {
                                Console.WriteLine(progress.BytesDownloaded);
                                break;
                            }
                        case DownloadStatus.Completed:
                            {
                                //Console.WriteLine("Download complete.");
                                SaveStream(stream1, FilePath);
                                break;
                            }
                        case DownloadStatus.Failed:
                            {
                                //Console.WriteLine("Download failed.");
                                break;
                            }
                    }
                };
                request.Download(stream1);
                return FilePath;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static void SaveStream(MemoryStream stream, string FilePath)
        {
            try
            {
                if (!File.Exists(FilePath))
                {
                    using (System.IO.FileStream file = new FileStream(FilePath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        stream.WriteTo(file);
                    }
                }
            }
            catch (Exception)
            {

            }

        }

        private static string GetFolderId(string cli_id, string cli_sec, string ref_token, string folderName, string folderParentId = null)
        {
            try
            {
                #region already present in the list
                Google.Apis.Drive.v3.Data.File file = null;

                if (string.IsNullOrEmpty(folderParentId))
                    try
                    {
                        file = AllFiles.Where(files => files.Name == folderName).FirstOrDefault();
                    }
                    catch (Exception e)
                    {

                        throw;
                    }
                else
                    file = AllFiles.Where(files => files.Name == folderName && files.Parents.Contains(folderParentId)).FirstOrDefault();
                if (file != null)
                    return file.Id;
                #endregion

                #region search in gdrive
                IList<Google.Apis.Drive.v3.Data.File> filesss = GetFoldersInfo(cli_id, cli_sec, ref_token, folderName, folderParentId);
                if (filesss.Count >= 1)
                {
                    file = filesss.FirstOrDefault();
                    AllFiles.Add(file);//save in a list. so that you don't have to send request to gdrive api
                    return file.Id;
                }
                #endregion

                //create folder if not present
                return folderCreation(folderName, folderParentId, cli_id, cli_sec, ref_token);
            }
            catch (Exception ex)
            {
                Thread.Sleep(5000);
                //ErrorLogging.log(ex);
                throw;
            }
        }

        public static IList<Google.Apis.Drive.v3.Data.File> GetFoldersInfo(string cli_id, string cli_sec, string ref_token, string folderName, string folderParentId = null)
        {
            var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
            var driveService = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = Y_oAuthCredentials,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });
            Google.Apis.Drive.v3.FilesResource.ListRequest listRequest = driveService.Files.List();
            if (string.IsNullOrEmpty(folderParentId))
                listRequest.Q = $"name='{folderName}' and mimeType = 'application/vnd.google-apps.folder' and trashed = false";
            else
                listRequest.Q = $"name='{folderName}' and '{folderParentId}' in parents and mimeType='application/vnd.google-apps.folder' and trashed = false";

            listRequest.Fields = "files(name,id,parents)";
            try
            {
                var result = listRequest.Execute();
                return result.Files;
            }
            catch (Exception e)
            {
                var data = e.Message;
                return null;
            }

        }

        public static IList<Google.Apis.Drive.v3.Data.File> ExceptFolders(string cli_id, string cli_sec, string ref_token, string query, string folderParentId = null)
        {
            var Y_oAuthCredentials = getUserAuthentication(cli_id, cli_sec, ref_token);
            var driveService = new Google.Apis.Drive.v3.DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = Y_oAuthCredentials,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });
            Google.Apis.Drive.v3.FilesResource.ListRequest listRequest = driveService.Files.List();
            listRequest.Q = $"{query} and '{folderParentId}' in parents and mimeType='application/vnd.google-apps.folder' and trashed = false";
            listRequest.PageSize = 1000;
            listRequest.Fields = "files(name,id,parents)";
            var result = listRequest.Execute();
            return result.Files;

        }
    }

    [Obsolete]
    public class ImagesList
    {
        public DateTime date { get; set; }
        public string url { get; set; }
    }
}
