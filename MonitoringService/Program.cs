﻿using System;
using System.ServiceProcess;

namespace MonitoringService
{
    class Program
    {
        static void Main(string[] args)
        {
#if DEBUG
            var test = new MonitoringServices();
            test.OnDebug();
#elif RELEASE
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new MonitoringServices()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
